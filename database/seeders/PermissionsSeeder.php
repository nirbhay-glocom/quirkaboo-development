<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DbModel\User;
use App\Models\DbModel\UserInfo;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Hash;
use Carbon\Carbon;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $data = $this->data();

        foreach ($data as $value) {
            Permission::create([
                'name' => $value['name'],
            ]);
        }

        // create roles and assign existing permissions
        // gets all permissions via Gate::before rule; see AuthServiceProvider
        $role1 = Role::create(['name' => 'Super Admin']);
        $role2 = Role::create(['name' => 'Admin']);
        $role3 = Role::create(['name' => 'Business User']);
        $role3->givePermissionTo(['user-read','order-update', 'order-read', 'order-delete', 'account-create','account-read','account-update','account-delete']);

        $role4 = Role::create(['name' => 'Vendor']);
        $role4->givePermissionTo(['order-read','account-create','account-read','account-update','account-delete']);


        // create demo users
        $user1 = User::create([
            'first_name' => 'Super',
            'last_name' => 'User',
            'email' => 'superadmin@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '0935923523',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        $user1->assignRole($role1);

        $user2 = User::create([
            'first_name' => 'Admin',
            'last_name' => 'User',
            'email' => 'admin@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '9883453454',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        $user2->assignRole($role2);

        $user3 = User::create([
            'first_name' => 'Business',
            'last_name' => 'User',
            'email' => 'business.user@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '7856453432',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        $user3->assignRole($role3);

        $user4 = User::create([
            'first_name' => 'Vendor',
            'last_name' => 'User',
            'email' => 'vendor@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '8823242323',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        $user4->assignRole($role4);
    }

    public function data()
    {
        $data = [];
        // list of model permission
        $model = ['account', 'user', 'role', 'permission','order'];

        foreach ($model as $value) {
            foreach ($this->crudActions($value) as $action) {
                $data[] = ['name' => $action];
            }
        }
        return $data;
    }

    public function crudActions($name)
    {
        $actions = [];
        // list of permission actions
        $crud = ['create', 'read', 'update', 'delete'];

        foreach ($crud as $value) {
            $actions[] = $name.'-'.$value;
        }

        return $actions;
    }
}
