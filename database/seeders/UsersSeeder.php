<?php

namespace Database\Seeders;

use App\Models\DbModel\User;
use App\Models\DbModel\UserInfo;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $user1 = User::create([
            'first_name' => 'Yuvraj',
            'last_name' => 'Eswaran',
            'email' => 'superadmin@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '0935923523',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        //$user1->assignRole("Super Admin");

        $user2 = User::create([
            'first_name' => 'Suriya',
            'last_name' => 'Prakash',
            'email' => 'admin@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '9883453454',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        //$user2->assignRole("Admin");

        $user3 = User::create([
            'first_name' => 'Lokesh',
            'last_name' => 'Garg',
            'email' => 'company_user@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '7856453432',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        //$user3->assignRole("Company User");

        $user4 = User::create([
            'first_name' => 'Nirbhay',
            'last_name' => 'Sharma',
            'email' => 'vendor@email.com',
            'password' => Hash::make('Qvp@1234'),
            'phone' => '8823242323',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        //$user4->assignRole("Vendor");
    }
}
