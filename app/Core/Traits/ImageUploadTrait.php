<?php
 
namespace App\Core\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait ImageUploadTrait {

    public static function backup_storeFiles($file, $folder)
    {
        $destinationPath = '/uploads/'.$folder;
        $file_name = time().'-'.$file->getClientOriginalName();
        $file->move($_SERVER['DOCUMENT_ROOT'].$destinationPath, $file_name); 
        return $file_name;
    }

    public static function storeFiles($image_name,$dir_name=null)
    {
        $request = request();
        $image = $request->file($image_name);
        $random = Str::random(8);
        $name = time().$random.'_'.$image->getClientOriginalName();
        $destinationPath = is_null($dir_name) ? public_path('/uploads/'): public_path('/uploads/'.$dir_name.'/');
        $image->move($destinationPath, $name);
        // $imageName = is_null($dir_name) ?  asset('uploads').'/'.$name : asset('uploads').'/'.$dir_name.'/'.$name ;
        $imageName = is_null($dir_name) ?  'uploads'.'/'.$name : '/uploads'.'/'.$dir_name.'/'.$name ;
        return $imageName;
    }

    /**
     * Deleted Existing Image from folder
     *
     * @param  String $fieldName
     * @param  String $tableName
     * @param  int $rowId
     * @return Boolean
     */
    public static function deletedExistingImage($fieldName, $tableName, $rowId){
        $resultData = DB::table($tableName)->where('id', $rowId)->first();
        return File::delete(public_path(parse_url($resultData->$fieldName)['path']));
    }

    /**
     * Check image exist or not
     *
     * @param  String $fieldName
     * @param  String $tableName
     * @param  int $rowId
     * @return Boolean
     */
    public static function checkImageExitOrNot($fieldName, $tableName, $rowId){
        $resultData = DB::table($tableName)
            ->where('id', $rowId)
            ->where($fieldName, '<>', null)
            ->exists();

        if($resultData)
            return true;
        else
            return false;
    }
}