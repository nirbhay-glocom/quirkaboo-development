<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use App\Models\QVPDOModel\Order_DbQuery_Model;

class CustomerUrlController extends Controller
{
    public function index($public_key, $token)
    {
        if((isset($public_key) && !empty($public_key)) && (isset($token) && !empty($token))){
            $order = Order_DbQuery_Model::getCustomerPreviewData($public_key, $token);
            if(!is_null($order)){

                $currentTime = time();
                $access = false;

                if(!$order->Expiry){
                    $access = true;
                }else{
                    if($order->Expiry > $currentTime){
                        $access = true;
                    }else{
                        $access = false;
                    }
                }
                
                if($access == true){
                    return view('pages.customer-url',compact('order'));
                }else{
                    abort(404);
                }
            }else{
                abort(404);
            }
        }
    }

}
