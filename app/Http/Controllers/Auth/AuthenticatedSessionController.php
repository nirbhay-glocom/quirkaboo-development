<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;

use App\Models\DbModel\User;
use App\Models\QVPBusinessLogic\User_Authentication_Model;
use App\Models\QVPDOModel\User_Create_Model;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Author: Nirbhay Sharma
     * Date: 14-01-2022
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        $request->session()->regenerate();
        
        //My Code Start from Here...
        $user = Auth::guard('api')->check();
        $validator = User_Authentication_Model::formValidation($request, 'signin');
        if ($validator->fails()) { 
            return response()->json(['error' => $validator->errors()], 401);
        }else{
            $response = User_Authentication_Model::signInVerification($request);
            //Update Token into user table...
            $updToken = User_Authentication_Model::updateUser($response);
            if($updToken){
                
                //Set Token in Cookie for 1 day
                $cookie = cookie('api-token', $response['token'], 60 * 24);
                return response([
                    'message' => 'User logged in Successfully',
                    'token' => $response['token']
                    ],200)
                    ->withCookie($cookie);     

                
            }           
        }
        //return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Handle an incoming api authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function apiStore(LoginRequest $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect']
            ]);
        }

        $user = User::where('email', $request->email)->first();
      
        // create a new api token for current login session
        $user->api_token = hash('sha256', Str::random(30));
        $user->save();
        return response($user);
    }

    /**
     * Verifies user token.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function apiVerifyToken(Request $request)
    {
        $user = User::where('api_token', $request->token)->first();

        if(!$user){
            throw ValidationException::withMessages([
                'token' => ['Invalid token']
            ]);
        }
        return response($user);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
