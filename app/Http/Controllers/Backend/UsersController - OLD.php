<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\Http;
use App\Models\DbModel\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use App\Models\QVPBusinessLogic\User_Business_Model;
use App\Models\QVPDOModel\User_DbQuery_Model;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:user-read|user-create|user-edit|user-delete', ['only' => ['index','show']]);
        //  $this->middleware('permission:user-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User_DbQuery_Model::getAllUsers();
        return view('pages.users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = User_DbQuery_Model::getAllRoles();
        return view('pages.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = User_Business_Model::checkUserValidations($request);
        if ($validator) {
         
            $input = $request->all();
            $input = User_Business_Model::setPasswordBycrypt($input);
            $user = User_DbQuery_Model::createUser($input);
            
            //Assign Role to User...
            User_Business_Model::assignRoleToUser($user, $request);
            return redirect()->route('users.index')
                            ->with('success','User created successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User_DbQuery_Model::getUserDetailById($id);
        return view('pages.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User_DbQuery_Model::getUserDetailById($id);
        $roles = User_DbQuery_Model::getAllRoles();
        $userRole = User_DbQuery_Model::getUserRole($user, $roles);
        return view('pages.users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = User_Business_Model::checkUserValidations($request, $id);
        $input = $request->all();
        $input = User_Business_Model::setPasswordBycrypt($input);
        $user = User_DbQuery_Model::getUserDetailById($id);
        User_DbQuery_Model::updateUser($user, $input);

        //Delete User Model Has Role 
        User_DbQuery_Model::deleteModelHasRole($id);
        //Assign Role to User...
        User_Business_Model::assignRoleToUser($user, $request);
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteUser = User_DbQuery_Model::deleteUserById($id);
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}
