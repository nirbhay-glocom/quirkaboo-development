<?php
    
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use App\Models\QVPBusinessLogic\Role_Business_Model;
use App\Models\QVPDOModel\Role_DbQuery_Model;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
                foreach($this->user->roleHasPagePermissions() as $permission){
                    if($permission['Name']=='Manage Roles' && $permission['page']=='Hide'){
                        abort(404);
                    }
                }
            return $next($request);
        });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $allRoles = Role_DbQuery_Model::getAllRoles();
        $updatedRoles = Role_DbQuery_Model::addedExtraField($allRoles);
        $pages = Role_DbQuery_Model::getAllPagePermissions();
        if(isset($updatedRoles) && !empty($updatedRoles)){
            foreach($updatedRoles as $key => $role){
                $roles_permissions = Role_DbQuery_Model::getRolePermission($role->id);

                //set Page level...
                $pagesArr = Role_Business_Model::setArray($roles_permissions, $pages, $role->id);
                $updatedRoles[$key]['pagesArr'] = $pagesArr;
               
                //set Container level...
                $permissionsArr = Role_DbQuery_Model::getAllContainerPermissions();
                $chk = Role_DbQuery_Model::checkPermissions('Manage Orders', $role->id);
                if($chk){
                    $containerLevelArr = Role_Business_Model::setArray($roles_permissions, $permissionsArr, $role->id);
                }else{
                    $containerLevelArr = Role_Business_Model::setArray([], $permissionsArr, $role->id);
                }
                $container_permissions = Role_Business_Model::buildTree($containerLevelArr);
                $updatedRoles[$key]['container_permissions'] = $container_permissions;
            }
        }
        $roles = $updatedRoles;
        return view('pages.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_groups = Role_DbQuery_Model::getUserGroup();
        return view('pages.roles.create',compact('user_groups'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Role_Business_Model::checkRoleValidations($request);
        if (!$validator->fails()) {
            $slug = Role_Business_Model::createSlug($request->name);
            $role = Role_DbQuery_Model::roleCreate($request, $slug);

            //Save Role into Role-Permissions table...
            $saveRoleIntoRolePermission = Role_DbQuery_Model::saveRoleIntoRolePermissionTbl($role->id);
            return redirect()->route('roles.index')
                            ->with('success','Role created successfully');
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role_DbQuery_Model::getRoleById($id);
        $user_groups = Role_DbQuery_Model::getUserGroup();
        return view('pages.roles.edit',compact('role','user_groups'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Role_Business_Model::checkRoleUpdtValidations($request);
        if (!$validator->fails()) {
            $slug = Role_Business_Model::createSlug($request->name);
            $role = Role_DbQuery_Model::roleUpdate($request, $slug,  $id);

            return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 09-03-2022
    * Add Roles and Permissions Syn
    * Request $request
    */
    public function addRolePermissions(Request $request){
        //Save Page Permission...
        $isExists = Role_DbQuery_Model::checkPageRolePermissions($request->Role_Id);
        if(count($isExists) > 0){
            $updatePagePermissions = Role_DbQuery_Model::updatePagePermissions($request);
            
        }else{
            $setPageArr = Role_Business_Model::setPagePermissionArr($request);
            $savePagePermissions = Role_DbQuery_Model::savePagePermissions($request, $setPageArr);
        }
        
        //Save Container Permission...
        if($request->OrderStatus==1){

            //Save default Arr inserted...
            $permissionsArr = Role_DbQuery_Model::getAllContainerPermissions();
            $conPermission = $permissionsArr->toArray();
            $container_permissions = Role_Business_Model::buildTree($conPermission);

            $defaultArr = Role_DbQuery_Model::getPermissionsUiTree($container_permissions, $request->Role_Id);
            Role_DbQuery_Model::saveRolePermissions($defaultArr);

            $saveContainerPermissions = Role_DbQuery_Model::saveContainerPermissions($request);

            return redirect()->route('roles.index')
                ->with('success','Permissions updated successfully.');
        }
       
        return redirect()->route('roles.index')
           ->with('success','Permissions updated successfully');
                        
    }
    
    /*
    * Author: Nirbhay Sharma
    * Date: 09-03-2022
    * Update Roles and Permissions Syn
    * Request $request
    */
    public function updateRolePermissions(Request $request){
        
        //Update Page Permissions...
        $updatePagePermissions = Role_DbQuery_Model::updatePagePermissions($request);
       
       //Update Container Permissions...
       if($request->OrderStatus==1){
            $updateContainerPermissions = Role_DbQuery_Model::updateContainerPermissions($request);
            return redirect()->route('roles.index')
                            ->with('success','Permissions updated successfully');
        }
        return redirect()->route('roles.index')
                            ->with('success','Permissions updated successfully');
     }
    
}