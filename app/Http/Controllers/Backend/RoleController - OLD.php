<?php
    
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\QVPBusinessLogic\Role_Business_Model;
use App\Models\QVPDOModel\Role_DbQuery_Model;
    
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        //  $this->middleware('permission:role-read|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        //  $this->middleware('permission:role-create', ['only' => ['create','store']]);
        //  $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        //  $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $roles = Role_DbQuery_Model::getAllRoles();
        return view('pages.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Role_DbQuery_Model::getAllPermissions();
        $permissionRoleUsers = Role_Business_Model::getRoleUsers($permission);
        return view('pages.roles.create',compact('permission','permissionRoleUsers'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Role_Business_Model::checkRoleValidations($request);
        $role = Role_DbQuery_Model::createRole($request); 
        Role_Business_Model::syncPermissions($request, $role); 
        return redirect()->route('roles.index')
                        ->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role_DbQuery_Model::getRoleById($id);
        $rolePermissions = Role_DbQuery_Model::rolePermissionsJoin($id);
        return view('pages.roles.show',compact('role','rolePermissions'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role_DbQuery_Model::getRoleById($id);
        $permission = Role_DbQuery_Model::getAllPermissions();
        $rolePermissions = Role_DbQuery_Model::getAllRolePermissions($id);
        $permissionRoleUsers = Role_Business_Model::getRoleUsers($permission);
        return view('pages.roles.edit',compact('role','permission','rolePermissions','permissionRoleUsers'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Role_Business_Model::checkRoleValidations($request);
        $role = Role_DbQuery_Model::getRoleById($id);
        Role_DbQuery_Model::updateRole($request, $role);
        Role_Business_Model::syncPermissions($request, $role);
        return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role_DbQuery_Model::deleteRoleById($id);
        return redirect()->route('roles.index')
                        ->with('success','Role deleted successfully');
    }
}