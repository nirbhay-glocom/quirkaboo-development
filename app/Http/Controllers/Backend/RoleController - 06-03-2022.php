<?php
    
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
// use App\Models\QVPBusinessLogic\Role_Business_Model;
// use App\Models\QVPDOModel\Role_DbQuery_Model;

use Validator;
use App\Models\DbModel\User;
use App\Models\DbModel\Role;
use App\Models\DbModel\MasterUiElement;
use App\Models\DbModel\UserGroup;
use App\Models\DbModel\RolesPermission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
                foreach($this->user->roleHasPagePermissions() as $permission){
                    if($permission['Name']=='Manage Roles' && $permission['page']=='Hide'){
                        abort(404);
                    }
                }
            return $next($request);
        });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $roles = Role::with('user_group')->orderBy('id','DESC')->paginate(5);
        foreach($roles as $key => $role){
            if($role->id){
               $roles_permissionsExist = RolesPermission::with('permission')->where('Role_Id',$role->id)->get();
               if(count($roles_permissionsExist) > 0){
                    if($roles_permissionsExist[1]['Visibility'] > 0){
                        $roles[$key]['pageRoute'] = 'Edit'; 
                    }else{
                        $roles[$key]['pageRoute'] = 'Add';
                    }
                }else{
                    $roles[$key]['pageRoute'] = 'Add';
                }

                //Role-Permissions Related...
                $role_id = $role->id;
                $role = Role::with('user_group')->where('id', $role_id)->first();

                //Page level...
                $pages = MasterUiElement::where('Type', 'Page')->get();
                $roles_permissions = RolesPermission::where('Role_Id', $role_id)->get();        
                $pagesArr = $this->setArray($roles_permissions, $pages, $role_id);
                $roles[$key]['pagesArr'] = $pagesArr; 

                //Container level...
                $permissionsArr = MasterUiElement::where("Type",'<>', 'Page')->get();
                $chk = $this->checkPermissions('Manage Orders', $role_id);
                if($chk){
                    $containerLevelArr = $this->setArray($roles_permissions, $permissionsArr, $role_id);
                }else{
                    $containerLevelArr = $this->setArray([], $permissionsArr, $role_id);
                }
                $container_permissions = $this->buildTree($containerLevelArr);
                $roles[$key]['container_permissions'] = $container_permissions; 
              
            }
        }

        return view('pages.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_groups = UserGroup::get();
        return view('pages.roles.create',compact('user_groups'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|unique:roles,name',
                'user_group_id' => 'required',
            ]
        );

         $params = $request->all();
         $params['slug'] = $this->createSlug($request->name);
         $role = Role::create($params);
         
        //Save Role into Role-Permissions table...
        $permissions = MasterUiElement::where('Type', 'Page')->get();
        $paramsPageArray = [];
        foreach($permissions as $value){ 
                $paramsPageArray[] = [
                    'Role_Id' => $role->id,
                    'Permission_Id' => $value->ID,
                    'Visibility' => 0,
                    'View' => 0,
                    'View_And_Edit' => 0
                ];
        }
        RolesPermission::insert($paramsPageArray);

        return redirect()->route('roles.index')
                        ->with('success','Role created successfully');
    }

    public static function createSlug($str, $delimiter = '_'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    } 


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::with('user_group')->find($id);
        return view('roles.show',compact('role'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $user_groups = UserGroup::get();
        return view('pages.roles.edit',compact('role','user_groups'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'name' => 'required',
                'user_group_id' => 'required',
            ]
        );

         $params = $request->all();
         $params['slug'] = $this->createSlug($request->name);
         $role = Role::find($id);
         $role->update($params);
       
        //Role_Business_Model::syncPermissions($request, $role);
        return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
    }

    public function setArray($roles_permissions, $allPermissions, $role_id){
        $setPermissionArray = [];
        foreach($allPermissions as $key2 => $permArr2){

           if (count($roles_permissions) > 0){
                foreach($roles_permissions as $key1 => $permArr1){           

                    if($permArr1->Permission_Id==$permArr2->ID){

                       $setPermissionArray[] = [
                            'ID' => $permArr2->ID,
                            'Role_Permission_RowId' => $permArr1->ID,
                            'Role_Id' => $permArr1->Role_Id,
                            'Permission_Id' => $permArr1->Permission_Id,
                            'Name' => $permArr2->Name,
                            'Type' => $permArr2->Type,
                            'Parent_Id' => $permArr2->Parent_Id,
                            'Visibility' => $permArr1->Visibility,
                            'View' => $permArr1->View,
                            'View_And_Edit' => $permArr1->View_And_Edit
                        ];

                    }
                }
            }else{
                    $setPermissionArray[] = [
                        'ID' => $permArr2->ID,
                        'Role_Permission_RowId' => 0,
                        'Role_Id' => $role_id,
                        'Permission_Id' => 0,
                        'Name' => $permArr2->Name,
                        'Type' => $permArr2->Type,
                        'Parent_Id' => $permArr2->Parent_Id,
                        'Visibility' => 0,
                        'View' => 0,
                        'View_And_Edit' => 0
                    ];    
            }
        }
      
        return $setPermissionArray;
    }

    public function addPermissions($role_id){
        
        $role = Role::with('user_group')->where('id', $role_id)->first();

        //Page level...
        $pages = MasterUiElement::where('Type', 'Page')->get();
        $roles_permissions = RolesPermission::where('Role_Id', $role_id)->get();        
        $pagesArr = $this->setArray($roles_permissions, $pages, $role_id);

        //Container level...
        $permissionsArr = MasterUiElement::where("Type",'<>', 'Page')->get();
        $chk = $this->checkPermissions('Manage Orders', $role_id);
        if($chk){
            $containerLevelArr = $this->setArray($roles_permissions, $permissionsArr, $role_id);
        }else{
            $containerLevelArr = $this->setArray([], $permissionsArr, $role_id);
        }
        
        $container_permissions = $this->buildTree($containerLevelArr);
        return view('pages.roles.add_permissions', compact('role','pagesArr','container_permissions','pages'));
    }

    public function updatePermissions($role_id){
        $role = Role::with('user_group')->where('id', $role_id)->first();

        //Page level...
        $pages = MasterUiElement::where('Type', 'Page')->get();
        $roles_permissions = RolesPermission::where('Role_Id', $role_id)->get();        
        $pagesArr = $this->setArray($roles_permissions, $pages, $role_id);

        //Container level...
        $permissionsArr = MasterUiElement::where("Type",'<>', 'Page')->get();
        $chk = $this->checkPermissions('Manage Orders', $role_id);
        if($chk){
            $containerLevelArr = $this->setArray($roles_permissions, $permissionsArr, $role_id);
        }else{
            $containerLevelArr = $this->setArray([], $permissionsArr, $role_id);
        }
        
        $container_permissions = $this->buildTree($containerLevelArr);
        return view('pages.roles.update_permissions', compact('role','pagesArr','container_permissions','pages'));
    }

    public function checkPermissions($pageName, $role_id){

        $permArr = MasterUiElement::where("Type", 'Page')->where("Name", $pageName)->first();
        if(!is_null($permArr) && $permArr->ID){
            $getParentOneRow = MasterUiElement::where("Type",'<>','Page')->where("Parent_Id", $permArr->ID)->first();
            return RolesPermission::where('Role_Id', $role_id)->where('Permission_Id', $getParentOneRow->ID)->first();
        }
        return false;
    }

    public function addRolePermissions(Request $request){
       //dd($request->all());
        //Save Page...
        if(isset($request->PageVisibility) && !empty($request->PageVisibility) && !empty($request->Role_Id)){
            $paramsPageArray = [];
            foreach($request->PageVisibility as $permission_Id => $value){ 
                if($request->pageExists==0){
                    
                    $paramsPageArray[] = [
                        'Role_Id' => $request->Role_Id,
                        'Permission_Id' => $permission_Id,
                        'Visibility' => $value,
                        'View' => 0,
                        'View_And_Edit' => 0
                    ];
                }

                if($request->pageExists > 0){
                     RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $permission_Id)
                     ->update([                        
                           'Visibility' => $value
                        ]);
                }
            }
            RolesPermission::insert($paramsPageArray);

        if($request->OrderStatus==1){

                //Save default Arr inserted...
                $defaultArr = $this->getPermissionsUiTree($request->Role_Id);
                RolesPermission::insert($defaultArr);

                //Save Container fields...
                if(isset($request->Visibility) && !empty($request->Visibility) && !empty($request->Role_Id)){

                    $paramsArray = [];
                    foreach($request->Visibility as $permission_Id => $value){
                        if($value){
                            $visiblity = $value; 
                        }else{
                            $visiblity = 0;
                        }
                        if($permission_Id > 0){
                            
                            RolesPermission::where("Role_Id", $request->Role_Id)->where("Permission_Id", $permission_Id)
                            ->update([                        
                                    
                                    'Visibility' => $visiblity,
                                    'View' => 0,
                                    'View_And_Edit' => 0   
                            ]);
                        }                         
                    }
                }

                //Update View....
                if(isset($request->View) && !empty($request->View) && !empty($request->Role_Id)){
                    $paramsArray = [];
                    foreach($request->View as $permission_Id => $value){
                        RolesPermission::where("Role_Id", $request->Role_Id)->where("Permission_Id", $permission_Id)
                        ->update([                        
                                 'View' => $value   
                        ]);
                    }
                }

                 //Update View_And_Edit....
                if(isset($request->View_And_Edit) && !empty($request->View_And_Edit) && !empty($request->Role_Id)){
                    $paramsArray = [];
                    foreach($request->View_And_Edit as $permission_Id => $value){
                        RolesPermission::where("Role_Id", $request->Role_Id)->where("Permission_Id", $permission_Id)
                        ->update([                        
                                 'View_And_Edit' => $value
                                   
                        ]);
                    }
                }

                //return redirect()->route('roles.update_permissions', $request->Role_Id)
                return redirect()->route('roles.index')
                                ->with('success','Permissions added successfully');
            }
        }
        //return redirect()->route('roles.add_permissions', $request->Role_Id)
        return redirect()->route('roles.index')
                        ->with('success','Page Permission added successfully');
                        
    }
    
    public function getPermissionsUiTree($role_id){
        $permissionsArr = MasterUiElement::where("Type",'<>', 'Page')->get()->toArray();
        $container_permissions = $this->buildTree($permissionsArr);
        $defaultArr = [];
        if(isset($container_permissions) && !empty($container_permissions) && !empty($role_id)){
          
            foreach($container_permissions as $key1 => $tab){
                
                $defaultArr[] = [
                            'Role_Id' => $role_id,
                            'Permission_Id' => $tab['ID'],
                            'Visibility' => 0,
                            'View' => 0,
                            'View_And_Edit' => 0
                    ]; 
                    if(isset($tab['childs']) && !empty($tab['childs'])){
                        foreach($tab['childs'] as $key2 => $section){
                            

                            $defaultArr[] = [
                                    'Role_Id' => $role_id,
                                    'Permission_Id' => $section['ID'],
                                    'Visibility' => 0,
                                    'View' => 0,
                                    'View_And_Edit' => 0
                            ]; 

                                    if(isset($section['childs']) && !empty($section['childs'])){
                                          foreach($section['childs'] as $key3 => $field){
                                            
                                            $defaultArr[] = [
                                                    'Role_Id' => $role_id,
                                                    'Permission_Id' => $field['ID'],
                                                    'Visibility' => 0,
                                                    'View' => 0,
                                                    'View_And_Edit' => 0
                                            ]; 
                                            
                                          }
                                   }

                        }  
                    }
            }
        
            return $defaultArr;
        }
    }

    public function updateRolePermissions(Request $request){
        //dd($request->all());
        //Update Page...
        if(isset($request->PageVisibility) && !empty($request->PageVisibility) && !empty($request->Role_Id)){
        
        $pageIds = [];
        foreach($request->PageVisibility as $permission_Id => $value){

            if($request->pageExists > 0){
                 RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $permission_Id)
                 ->update([                        
                       'Visibility' => $value
                    ]);
            }
        }

       if($request->OrderStatus==1){       
            //Save Container fields...
            if(isset($request->Visibility) && !empty($request->Visibility) && !empty($request->Role_Id)){

                $paramsArray = [];
                foreach($request->Visibility as $rowID => $value){
                    if($value){
                        $visiblity = $value; 
                    }else{
                        $visiblity = 0;
                    }
                    if($rowID > 0){
                        
                        RolesPermission::where("ID", $rowID)
                        ->update([                        
                                
                                'Visibility' => $visiblity,
                                'View' => 0,
                                'View_And_Edit' => 0 
                        ]);
                    }
                     
                }
            }

            //Update View....
             if(isset($request->View) && !empty($request->View) && !empty($request->Role_Id)){
                $paramsArray = [];
                foreach($request->View as $rowID => $value){
                    RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $rowID)
                    ->update([                        
                             'View' => $value,
                             'Visibility' => 0,
                             'View_And_Edit' => 0

                    ]);
                }
             }

             // //Update View_And_Edit....
             if(isset($request->View_And_Edit) && !empty($request->View_And_Edit) && !empty($request->Role_Id)){
                $paramsArray = [];
                foreach($request->View_And_Edit as $rowID => $value){
                    RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $rowID)
                    ->update([                        
                             'View_And_Edit' => $value,
                             'Visibility' => 0,
                             'View' => 0


                    ]);
                }
             }

            //return redirect()->route('roles.update_permissions', $request->Role_Id)
            return redirect()->route('roles.index')
                            ->with('success','Permissions updated successfully');
        }
       // return redirect()->route('roles.update_permissions', $request->Role_Id)
        return redirect()->route('roles.index')
                            ->with('success','Page Permission updated successfully');
     }
     
    }

    //Recursive get permissions function
    function buildTree($items) {
       if($items){
            $childs = array();
            foreach($items as &$item){
                $childs[$item['Parent_Id']][] = &$item;
                unset($item);
            }
            foreach($items as &$item){
                if (isset($childs[$item['ID']])){
                    $item['childs'] = $childs[$item['ID']];
                }
            }

            return $childs[2];
       }        
    } 
}