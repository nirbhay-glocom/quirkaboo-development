<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\Http;
use App\Models\DbModel\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use App\Models\QVPBusinessLogic\User_Business_Model;
use App\Models\QVPDOModel\User_DbQuery_Model;
use App\Models\QVPDOModel\Role_DbQuery_Model;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
                foreach($this->user->roleHasPagePermissions() as $permission){
                    if($permission['Name']=='Manage Users' && $permission['page']=='Hide'){
                        abort(404);
                    }
                }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User_DbQuery_Model::getAllUsers();
        return view('pages.users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = User_DbQuery_Model::getAllRoles();
        return view('pages.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = User_Business_Model::checkUserValidations($request);
        if ($validator) {
            $input = $request->all();
            $input = User_Business_Model::setPasswordBycrypt($input);
            $user = User_DbQuery_Model::createUser($input);
            
            //Assign Role to User...
            $roleArr = Role_DbQuery_Model::getRoleByName($request->input('roles'));
            User_Business_Model::assignRoleToUser($user, $roleArr);
            return redirect()->route('users.index')
                            ->with('success','User created successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User_DbQuery_Model::getUserDetailById($id);
        return view('pages.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User_DbQuery_Model::getUserDetailById($id);
        $roles = User_DbQuery_Model::getAllRoles();
        $userRole = User_DbQuery_Model::getUserRole($user, $roles);
        return view('pages.users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = User_Business_Model::checkUserValidations($request, $id);
        $input = $request->all();
        $input = User_Business_Model::setPasswordBycrypt($input);
        $user = User_DbQuery_Model::getUserDetailById($id);
        User_DbQuery_Model::updateUser($user, $input);

        //Delete User Model Has Role 
        User_DbQuery_Model::deleteModelHasRole($id);
        //Assign Role to User...
        $roleArr = Role_DbQuery_Model::getRoleByName($request->input('roles'));
        User_Business_Model::assignRoleToUser($user, $roleArr);
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteUser = User_DbQuery_Model::deleteUserById($id);
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}
