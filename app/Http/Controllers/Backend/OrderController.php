<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use App\Models\QVPDOModel\Order_DbQuery_Model;
use App\Models\DbModel\QVPOrderCustomizationDetail;
use App\DataTables\OrdersDataTable;
use Datatables;
use App\Models\DbModel\User;
use App\Models\DbModel\Role;
use App\Models\DbModel\MasterUiElement;
use App\Models\DbModel\UserGroup;
use App\Models\DbModel\RolesPermission;
use App\Models\DbModel\QVPOrderDetail;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Author: Nirbhay Sharma
     * Date: 22-01-2022
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
                foreach($this->user->roleHasPagePermissions() as $permission){
                    if($permission['Name']=='Manage Orders' && $permission['page']=='Hide'){
                        abort(404);
                    }
                }
            return $next($request);
        });
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Getting Orders from orders table with all its relational tables.
    * @return $orders
    */
    public function index(){
        $orders = Order_DbQuery_Model::getAllOrdersInfo();
        $userId = auth()->user()->id;
        $role = auth()->user()->roles->pluck('name')['0'];
        return view('pages.orders.orders-listing.index',compact('orders','userId','role'));
    }
}
