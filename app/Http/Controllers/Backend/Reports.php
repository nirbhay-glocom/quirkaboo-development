<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use DB;
//use App\Http\Constants\MyApp;
//use App\Http\Traits\ResponseTrait;
use App\Models\QVPBusinessLogic\Reports_Business_Logic_Model;
use App\Models\QVPBusinessLogic\Common_Model_Business_Logic;
use App\Models\QVPDOModel\Reports_Do_Model;
use Illuminate\Support\Facades\Storage;

class Reports extends Controller
{
    //use ResponseTrait;
	
    public function __construct() {
        
		$this->Reports_Business_Logic_Model= new Reports_Business_Logic_Model();
		$this->Common_Model_Business_Logic = new Common_Model_Business_Logic();
	}

    public function SendQvpMail(Request $request){
   
        $Report_Type = $request->ReportType;
        $Current_URL = $request->currentUrl;

        if(empty($Report_Type)) {
            echo "Report Type Is Empty"; exit;
        }
        
        // if(!$this->Common_Model_Business_Logic->Check_Cron_Status($Current_URL)) {
        //     // Inactive
        //     echo "Cron Is Disabled"; exit;
        // }

        switch ($Report_Type) {
            case "Customer Preview":
              $result = $this->Reports_Business_Logic_Model->sendTestMail();
              if($result){
                return Response()->json(["data" => "Success"], 200);
              }
               break;
            default :
            echo "Report Type not found";
        }
       
    }

    public function Send_Report() {

        $Report_Type = $_GET['ReportType'];

        $Report_Date = isset($_GET['Date'])?$_GET['Date']:"";

        if(empty($Report_Type)) {
            echo "Report Type Is Empty"; exit;
        }

        if(!$this->Common_Model_Business_Logic->Check_Cron_Status($this->Common_Model_Business_Logic->Get_Current_URL($_SERVER))) {
            // Inactive
            echo "Cron Is Disabled"; exit;
        }

        switch ($Report_Type) {
            case "Stock-Accuracy":
                $this->Reports_Business_Logic_Model->getStockAccuracy();
                break;
            case "Missing-PriceStock-Variants":
                $this->Reports_Business_Logic_Model->getRetailMissingVariants();
                break;
            case "Missing-Channel-Configuration":
                $this->Reports_Business_Logic_Model->getMissingChannelConfiguration();
                break;
            case "Missing-Catalogue-Products":
                $this->Reports_Business_Logic_Model->getMissingCatalogueProducts();
                break;
            case "Wish-OOS-Override":
                $this->Reports_Business_Logic_Model->getWishOOSOverride();
                break;
            case "Listing_Errors_Report":
                $this->Reports_Business_Logic_Model->getListingErrorsReport();
                break;

            case "Channel_Catalogue_Report":
                $this->Reports_Business_Logic_Model->getChannelCatalogueReport();
                break;

            case "Listing_Processing_Error_Report":
                $this->Reports_Business_Logic_Model->getListingProcessingErrorsReport();
                break;

            case "Pending_Listing_Report":
                $this->Reports_Business_Logic_Model->getPendingListingReport();
                break;
            
            case "Missed_Daily_Report":
                $this->Reports_Business_Logic_Model->getMissedDailyReport($Report_Date);
                break;

            case "Incomplete_Listing_Library":
                $this->Reports_Business_Logic_Model->getIncompleteListingLibrary();
                break;

            case "Vendor_Price_Change_Alert":
                $this->Reports_Business_Logic_Model->getVendorPriceChangeAlert();
                break;

            case "Retail_Reset_Configuration":
                $this->Reports_Business_Logic_Model->GetResetCongifAlert();
                break;
            
            default :
                echo "Report Type not found";
        }

    }
}