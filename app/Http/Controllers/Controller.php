<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\DbModel\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request){
        $this->apiToken = $request->cookie('api-token');
    }

    public function AuthUser(){
        return User::where('api_token', $this->apiToken)->first();
    }

    public function AuthCheckUser(){
        if(isset($this->apiToken) && !empty($this->apiToken)){
            $user = $this->AuthUser();
            if(isset($user) && !empty($user)){
                return true;
            }else{
                return false;
            }            
        }else{
            return false;
        }
    }
}
