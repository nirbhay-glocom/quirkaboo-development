<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use App\Models\QVPDOModel\Order_DbQuery_Model;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Http\Response;
use App\Http\Requests;

class OrderController extends Controller
{
    public $successStatus = 200;
    public $unprocessingStatus = 422;

    /*
    * Author: Nirbhay Sharma
    * Date: 04-01-2022
    * Order list Fetch.
    * @param Request $request
    * @return JSON Response
    */
    public function index(Request $request){
        $orders = Order_DbQuery_Model::getAllOrdersInfo();
        if(sizeof($orders) > 0){
            return response()->json(['orders' => $orders], $this->successStatus);
        }else{
            return response()->json(["error" => "Not found any data"], $this->unprocessingStatus);
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 04-01-2022
    * Order Details Fetch.
    * @param Request $request
    * @return JSON Response
    */
    public function orderDetail(Request $request){
       if(!empty($request->orderId)){
            $orderDetail = Order_DbQuery_Model::getOrderDetail($request->orderId); 
            if(sizeof($orderDetail) > 0){
                return response()->json(['orderDetail' => $orderDetail], $this->successStatus);
            }else{
               return response()->json(["error" => "Not found any data"], $this->unprocessingStatus); 
            }
       }   
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 04-01-2022
    * update Order Info with Order Product Detail.
    * @param Request $request
    * @return JSON Response $orderData
    */
    public function updateOrderDetail(Request $request){ 
       //dd($request->all()); 
        $orderDetail = Order_DbQuery_Model::updateOrder($request);
        $orderProductDetail = Order_DbQuery_Model::updateOrderProduct($request);
        $singleOrderDetail = Order_DbQuery_Model::getOrderDetail($request->ID);
        $orderData = Order_Information_Model::getOrderArr($singleOrderDetail);
        if(sizeof($orderData) > 0){
           return response()->json(['data' => $orderData], $this->successStatus); 
        }else{
            return response()->json(['error' => 'Order not updated'], $this->unprocessingStatus);
        }
    }

    public function updateOrderText(Request $request){
        $orderDetail = Order_DbQuery_Model::updateOrderTextNotes($request);
        if($orderDetail){
            return response()->json(['data' => "Success"], $this->successStatus); 
         }else{
            return response()->json(['error' => 'Order detail not updated'], $this->unprocessingStatus);
         }
    }

    public function updateOrderVendorNotes(Request $request){
        $orderDetail = Order_DbQuery_Model::updateOrderVendorNotes($request);
        if($orderDetail){
            return response()->json(['data' => "Success"], $this->successStatus); 
         }else{
            return response()->json(['error' => 'Order detail not updated'], $this->unprocessingStatus);
         }
    }

    public function updateOrderCustomerNotes(Request $request){
        $orderDetail = Order_DbQuery_Model::updateOrderCustomerNotes($request);
        if($orderDetail){
            return response()->json(['data' => "Success"], $this->successStatus); 
         }else{
            return response()->json(['error' => 'Order detail not updated'], $this->unprocessingStatus);
         }
    }

    public function updateOrderGiftMessage(Request $request){
        $orderDetail = Order_DbQuery_Model::updateOrderGiftMessage($request);
        if($orderDetail){
            return response()->json(['data' => "Success"], $this->successStatus); 
         }else{
            return response()->json(['error' => 'Order detail not updated'], $this->unprocessingStatus);
         }
    }



    
}
