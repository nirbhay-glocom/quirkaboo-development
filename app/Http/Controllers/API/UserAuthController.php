<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DbModel\User;
use App\Models\QVPBusinessLogic\User_Authentication_Model;
use App\Models\QVPDOModel\User_Create_Model;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Http\Response;
use App\Http\Requests;

class UserAuthController extends Controller
{
    public $successStatus = 200;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 02-01-2022
    * SignUp Auth Verification.
    * @param Request $request
    * @return JSON Response
    */
    public function signup(Request $request){
        $validator = User_Authentication_Model::formValidation($request, 'signup');
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }else{
            $success = User_Create_Model::user_signup($request);
            return response()->json(['success' => $success], $this->successStatus);
        }      
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 02-01-2022
    * SignIn Auth Verification.
    * @param Request $request
    * @return JSON Response
    */
    public function signin(Request $request){
        $user = Auth::guard('api')->check();
        $validator = User_Authentication_Model::formValidation($request, 'signin');
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }else{
           $response = User_Authentication_Model::signInVerification($request);

           //Update Token into user table...
           $updToken = User_Authentication_Model::updateUser($response);
           if($updToken){
                //Set Token in Cookie for 1 day
                $cookie = cookie('api-token', $response['token'], 60 * 24);
                return response([
                    'message' => 'User logged in Successfully',
                    'token' => $response['token']
                    ],200)->withCookie($cookie);
            }           
        }
    }
}
