<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use App\Models\QVPDOModel\Order_DbQuery_Model;
use App\Models\QVPDOModel\Order_Customization_Detail_Model;
use App\Models\QVPDOModel\User_DbQuery_Model;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ImageCustomizedController extends Controller
{
    public $successStatus = 200;
    public $unprocessingStatus = 422;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Save Order Customized Image...
    * @param Request $request
    * @return Response Image URL
    */
    public function save(Request $request)
    {
        $validator = Order_Information_Model::checkValidImage($request,'Image_URL');
        if($validator){
            $updateCustomeText = Order_Customization_Detail_Model::updateCustomizationText($request);
            $parmsData = Order_Information_Model::checkCustomizedImage($request);
            $resultID = Order_Customization_Detail_Model::uploadCustomizedImage($parmsData); 
            if($resultID){
                $user = User_DbQuery_Model::getUserDetailById($request->UserId);
                $commentContent = Order_Information_Model::manageCommentContent('uploadImg', $user);
                $commentJsonArr = Order_Information_Model::setCommentDataArr($resultID, $request, $user, $commentContent);
                $result = Order_Customization_Detail_Model::updateImageComments($commentJsonArr, $resultID);
                $getData = Order_DbQuery_Model::getOrderCustomizeData($resultID);
             
                return Response()->json(["data" => $getData], Response::HTTP_OK);
            }else{
                return Response()->json(["error" => "Image is not uploaded"], $this->unprocessingStatus);
            }
        }else{
            return Response()->json(["error" => "Invalid image format"], $this->unprocessingStatus);
        }
    }
    
    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Delete Order Customized Image...
    * @param Request $request
    * @return Json Response
    */
    public function deleteImage(Request $request){
        $customizedRowData = Order_DbQuery_Model::getOrderCustomizeData($request->ID);
        if(!empty($customizedRowData) && $customizedRowData->count() > 0){
            $isImageUnset = Order_Information_Model::unsetImage($customizedRowData);
            if($isImageUnset){
                $result = Order_Customization_Detail_Model::deleteCustomizeImage($customizedRowData); 
                if($result){
                    return response()->json(['status' => 'Success'], $this->successStatus);
                 }else{
                    return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
                }
            }else{
                return response()->json(['status' => 'Something went wrong. Image is not unset.'], $this->unprocessingStatus);
            }           
        }else{
            return response()->json(['status' => 'No Record Found'], $this->unprocessingStatus);
        }  
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Update Order Customized Custome Text & Notes...
    * @param Request $request
    * @return Json Response
    */
    public function updateCustomizedImage(Request $request){
        $resultID = Order_Customization_Detail_Model::updateCustomizeImageContent($request);
        if($resultID){
            $user = User_DbQuery_Model::getUserDetailById($request->UserId);
            $commentContent = Order_Information_Model::manageCommentContent('UpdateImgContent', $user);
            $commentJsonArr = Order_Information_Model::setCommentDataArr($request->ID, $request, $user, $commentContent);
            $result = Order_Customization_Detail_Model::updateImageComments($commentJsonArr, $request->ID);
            return response()->json(['status' => 'Updated Successfully'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Not Updated'], $this->unprocessingStatus);
        }
    }

    // public function getDataAfterDeleteImageRow(Request $request){
    //     $response = $this->delete($request);
    //     $json = json_decode($response, true);
    //      dd($json);
    // }

    /*
    * Author: Nirbhay Sharma
    * Date: 25-01-2022
    * Reject Order Customized Image Status...
    * @param Request $request
    * @return Json Response
    */
    public function rejectImage(Request $request){
        $customizedRowData = Order_DbQuery_Model::getOrderCustomizeData($request->ID);
        if(!empty($customizedRowData) && $customizedRowData->count() > 0){
            $result = Order_Customization_Detail_Model::rejectCustomizeImage($customizedRowData);
            if($result){
                return response()->json(['status' => 'Success'], $this->successStatus);
            }else{
                return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
            }          
        }else{
            return response()->json(['status' => 'No Record Found'], $this->unprocessingStatus);
        }  
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 25-01-2022
    * Approve Order Customized Image Status...
    * @param Request $request
    * @return Json Response
    */
    public function approveImage(Request $request){
        $customizedRowData = Order_DbQuery_Model::getOrderCustomizeData($request->ID);
        if(!empty($customizedRowData) && $customizedRowData->count() > 0){
            $result = Order_Customization_Detail_Model::approveCustomizeImage($customizedRowData);
            if($result){
                $user = User_DbQuery_Model::getUserDetailById($request->UserId);
                $commentContent = Order_Information_Model::manageCommentContent('Approved', $user);
                $commentJsonArr = Order_Information_Model::setCommentDataArr($request->ID, $request, $user, $commentContent);
                $result = Order_Customization_Detail_Model::updateImageComments($commentJsonArr, $request->ID);
                return response()->json(['status' => 'Success'], $this->successStatus);
            }else{
                return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
            }          
        }else{
            return response()->json(['status' => 'No Record Found'], $this->unprocessingStatus);
        }  
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 25-01-2022
    * Update Comments Order Customized Image...
    * @param Request $request
    * @return Json Response
    */
    public function updateComments(Request $request){
        $jsonArr = Order_Information_Model::setArrInJSON($request);
        $result = Order_Customization_Detail_Model::updateImageComments($jsonArr, $request->ID);
        if($result){
            return response()->json(['status' => 'Success'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 28-01-2022
    * Update rejection reason Customized Image...
    * @param Request $request
    * @return Json Response
    */
    public function updateRejectionReason(Request $request){
        $jsonArr = Order_Information_Model::setArrInJSON($request);
        $result = Order_Customization_Detail_Model::updateRejectReason($jsonArr, $request->ID, 'Rejected');
        if($result){
            return response()->json(['status' => 'Success'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 02-02-2022
    * Update Notes Customized Image...
    * @param Request $request
    * @return Json Response
    */
    public function updateNotes(Request $request){
        
        $result = Order_Customization_Detail_Model::updateCustomeNotes($request);
        if($result){
            return response()->json(['status' => 'Success'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    }  

    /*
    * Author: Nirbhay Sharma
    * Date: 04-02-2022
    * Update Notes Customized Image...
    * @param Request $request
    * @return Json Response
    */
    public function ajxUpdateNotes(Request $request){
        $result = Order_Customization_Detail_Model::updateCustomeNotes($request);
        if($result){
            $customImg =  Order_DbQuery_Model::getAllOrdersImg($request);
            return response()->json(["status" => $customImg], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    } 
}
