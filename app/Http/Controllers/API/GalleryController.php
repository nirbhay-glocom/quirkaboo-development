<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use App\Models\QVPDOModel\Order_DbQuery_Model;
use App\Models\QVPDOModel\Order_Customization_Detail_Model;
use App\Models\QVPDOModel\User_DbQuery_Model;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Models\DbModel\QVPOrderDetail;

class GalleryController extends Controller
{
    public $successStatus = 200;
    public $unprocessingStatus = 422;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Save Order Customized Image...
    * @param Request $request
    * @return Response Image URL
    */
    public function save(Request $request)
    {
        $validator = Order_Information_Model::checkValidImage($request,'URL');
        if($validator){
            $parmsData = Order_Information_Model::checkGalleryImage($request);
            $result = Order_Customization_Detail_Model::uploadGalleryImage($request->Order_Id, $parmsData); 
            if($result){
                $getData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
                $setData =  Order_Information_Model::setGalleryHtml($request->Order_Id, json_decode($getData->Gallery, TRUE), $request->visible);
                return Response()->json(["data" => $setData], Response::HTTP_OK);
            }else{
                return Response()->json(["error" => "Preview data is not uploaded."], $this->unprocessingStatus);
            }
        }else{
            return Response()->json(["error" => "Invalid Image/video format"], $this->unprocessingStatus);
        }
    }
   
    /*
    * Author: Nirbhay Sharma
    * Date: 25-01-2022
    * Approve Order Customized Image Status...
    * @param Request $request
    * @return Json Response
    */
    public function approveSelectedImages(Request $request){
       //dd($request->all());
        $setGalleryData = Order_Information_Model::setGalleryData($request);
        $result = Order_DbQuery_Model::updateGallerywMediaStatus($request->Order_Id, $setGalleryData);
        if($result){
            $getData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
            $setData =  Order_Information_Model::setGalleryHtml($request->Order_Id, json_decode($getData->Gallery, TRUE), $request->visible);
            return Response()->json(["data" => $setData], Response::HTTP_OK);
 
        }else{
            return Response()->json(["error" => "Gallery data is not uploaded."], $this->unprocessingStatus);
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 11-03-2022
    * Update Gallery Section Notes...
    * @param Request $request
    * @return Json Response
    */
    public function updateGalleryNotes(Request $request){
        $setGalleryMedia = Order_Information_Model::setGalleryNotesArr($request);
        $result = Order_DbQuery_Model::updateGalleryMediaNotes($request->Order_Id, $setGalleryMedia);
        if($result){
            return response()->json(['status' => 'Success'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    } 


    public function selectCustomerPreviewImages(Request $request){
        //dd($request->all());
        $setOnlyPreviewData = Order_Information_Model::setPreviewData($request);
        $result = Order_DbQuery_Model::updatePreviewMediaStatus($request->Order_Id, $setOnlyPreviewData);
        //dd($result1);
        $setCustomerPreviewData = Order_Information_Model::setCustomerPreviewData($request);
        $result = Order_DbQuery_Model::updatePreviewMediaStatus($request->Order_Id, $setCustomerPreviewData);

        //dd($result1);
        // if($result){
            $getData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
            $setData =  Order_Information_Model::setCustomerPreviewHtml($request->Order_Id, json_decode($getData->Preview_Media, TRUE), $request->visible);
            //dd($setData);
            return Response()->json(["data" => $setData], Response::HTTP_OK);
 
        //}
        // else{
        //     return Response()->json(["error" => "Customer Preview data is not uploaded."], $this->unprocessingStatus);
        // }
    }

    public function updateCustomerPreviewNotes(Request $request){
       
        $setCustomerPreviewMedia = Order_Information_Model::setPreviewNotesArr($request);
        $result = Order_DbQuery_Model::updatePreviewMediaNotes($request->Order_Id, $setCustomerPreviewMedia);
        if($result){
            return response()->json(['status' => 'Success'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    }

    public function setContent(Request $request){
        if($request->ID){
            $getData = Order_DbQuery_Model::getAllOrdersInfo();
            $setData =  Order_Information_Model::setGridContent($request->ID, $getData);
            return Response()->json(["data" => $setData], Response::HTTP_OK);

        }else{
            return Response()->json(["error" => "Order Grid data is not set."], $this->unprocessingStatus);
        }
    }
    
    
}
