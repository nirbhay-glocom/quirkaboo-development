<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use App\Models\QVPDOModel\Order_DbQuery_Model;
use App\Models\QVPDOModel\Order_Customization_Detail_Model;
use App\Models\QVPDOModel\User_DbQuery_Model;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\Models\DbModel\QVPOrderDetail;

class PreviewController extends Controller
{
    public $successStatus = 200;
    public $unprocessingStatus = 422;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Save Order Customized Image...
    * @param Request $request
    * @return Response Image URL
    */
    public function save(Request $request)
    {
        $validator = Order_Information_Model::checkValidImage($request,'URL');
        if($validator){
            $parmsData = Order_Information_Model::checkPreviewImage($request);
            $result = Order_Customization_Detail_Model::uploadPreviewImage($request->Order_Id, $parmsData); 
            if($result){
                $getData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
                $setData =  Order_Information_Model::setPreviewHtml($request->Order_Id, json_decode($getData->Preview_Media, TRUE), $request->visible);
                return Response()->json(["data" => $setData], Response::HTTP_OK);
            }else{
                return Response()->json(["error" => "Preview data is not uploaded."], $this->unprocessingStatus);
            }
        }else{
            return Response()->json(["error" => "Invalid Image/video format"], $this->unprocessingStatus);
        }
    }
   
    /*
    * Author: Nirbhay Sharma
    * Date: 25-01-2022
    * Approve Order Customized Image Status...
    * @param Request $request
    * @return Json Response
    */
    public function approveSelectedImage(Request $request){
       
        $setPreviewData = Order_Information_Model::setPreviewData($request);
        $result = Order_DbQuery_Model::updatePreviewMediaStatus($request->Order_Id, $setPreviewData);
        if($result){
            $getData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
            $setData =  Order_Information_Model::setPreviewHtml($request->Order_Id, json_decode($getData->Preview_Media, TRUE), $request->visible);
            return Response()->json(["data" => $setData], Response::HTTP_OK);
 
        }else{
            return Response()->json(["error" => "Preview data is not uploaded."], $this->unprocessingStatus);
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 10-03-2022
    * Update Preview Section Notes...
    * @param Request $request
    * @return Json Response
    */
    public function updatePreviewNotes(Request $request){
        
        $setPreviewMedia = Order_Information_Model::setPreviewNotesArr($request);
        $result = Order_DbQuery_Model::updatePreviewMediaNotes($request->Order_Id, $setPreviewMedia);
        if($result){
            return response()->json(['status' => 'Success'], $this->successStatus);
        }else{
            return response()->json(['status' => 'Fail'], $this->unprocessingStatus);
        }
    } 
    
    public function getOrderPreviewMedia(Request $request){
        // dd($request->viewType);
        $getData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);

        if(!empty($request->viewType) && $request->viewType=='GalleryView'){
            $setData =  Order_Information_Model::setCustomerPreviewHtml($request->Order_Id, json_decode($getData->Preview_Media, TRUE), $request->visible);
        }

        if(!empty($request->viewType) && $request->viewType=='PreviewView'){
            $setData =  Order_Information_Model::setPreviewHtml($request->Order_Id, json_decode($getData->Preview_Media, TRUE), $request->visible);
        }
        
        return Response()->json(["data" => $setData], Response::HTTP_OK);
    }
    
}
