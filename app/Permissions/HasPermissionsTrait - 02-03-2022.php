<?php

namespace App\Permissions;

use App\Models\DbModel\MasterUiElement;
use App\Models\DbModel\Role;
use App\Models\DbModel\RolesPermission;

trait HasPermissionsTrait {

  public function withdrawPermissionsTo( ... $permissions ) {

    $permissions = $this->getAllPermissions($permissions);
    $this->permissions()->detach($permissions);
    return $this;

  }

  public function refreshPermissions( ... $permissions ) {

    $this->permissions()->detach();
    return $this->givePermissionsTo($permissions);
  }

  public function hasPermissionTo($permission) {

    return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
  }

  public function hasPermissionThroughRole($permission) {

    foreach ($permission->roles as $role){
      // if($this->roles->contains($role)) {
      //   return true;
      // }

      if (Role::where('slug', $role)->count() > 0) {
        return true;
      }
    }
    return false;
  }

  public function hasRole( ... $roles ) {

    foreach ($roles as $role) {
      $roleArr = Role::where('slug', $role)->first();
      $currentRole = auth()->user()->roles->pluck('slug')['0'];
      if ($roleArr->slug==$currentRole) {
        return true;
      }
    }
    return false;
  }

  public function roleHasPagePermissions() {
    $currentRole = auth()->user()->roles->pluck('id')['0'];
        $permissions = RolesPermission::with('permission')->where('Role_Id', $currentRole)->get();
        $pagePermissionArr = [];
        if(count($permissions) > 0){
            foreach($permissions as $key => $val){
              if($val->permission->Type=='Page'){
                  $page = 'Hide';
                  if($val->Visibility > 0){
                      $page = 'Show';
                  }

                  $pagePermissionArr[] = [
                      'Role_Id' => $val->Role_Id,
                      'Permission_Id' => $val->Permission_Id,
                      'Name' => $val->permission->Name,
                      'Type' => $val->permission->Type,
                      'Parent_Id' => $val->permission->Parent_Id,
                      'page' => $page
                  ];
              }            
          }
        }
       return $pagePermissionArr;
  }

  public function roleHasContainerPermissions() {
    $currentRole = auth()->user()->roles->pluck('id')['0'];
    $permissions = RolesPermission::with('permission')->where('Role_Id', $currentRole)->get();
    $contentPermissionArr = [];
    if(count($permissions) > 0){
        foreach($permissions as $key => $val){
          if($val->permission->Type != 'Page'){
           
              
              $View = 'No';
              $View_And_Edit = 'No';
              $section = 'None';

              if($val->permission->Type == 'Tab'){
                $Visibility = 'Hide';
                if($val->Visibility > 0){
                    $Visibility = 'Show';
                    $section = 'Show';
                }
              }

              //Section Page....
              if($val->permission->Type == 'Section'){
                $Visibility = 'Show';
                $section = 'Show';
                if($val->Visibility > 0){
                    $Visibility = 'Hide';
                    $section = 'None';
                }
              }

              if($val->View > 0){
                  $View = 'Yes';
                  $section = 'View';
              }else if($val->View_And_Edit > 0){
                  $View_And_Edit = 'Yes';
                  $section = 'Edit';
              }

              $contentPermissionArr[] = [
                  'Role_Id' => $val->Role_Id,
                  'Permission_Id' => $val->Permission_Id,
                  'Name' => $val->permission->Name,
                  'Type' => $val->permission->Type,
                  'Parent_Id' => $val->permission->Parent_Id,
                  'Visibility' => $Visibility,
                  'View' => $View,
                  'View_And_Edit' => $View_And_Edit,
                  'section' => $section
              ];
          }            
      }
    }
       return $contentPermissionArr;
  }

  public function roles() {

    return $this->belongsToMany(Role::class,'users_roles');

  }
  public function permissions() {

    return $this->belongsToMany(MasterUiElement::class,'users_permissions');

  }
  protected function hasPermission($permission) {

    return (bool) $this->permissions->where('Name', $permission->Name)->count();
  }

  protected function getAllPermissions(array $permissions) {

    return MasterUiElement::whereIn('Name',$permissions)->get();
    
  }

}