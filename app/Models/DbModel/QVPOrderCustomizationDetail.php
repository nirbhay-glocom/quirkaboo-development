<?php

namespace App\Models\DbModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QVPOrderCustomizationDetail extends Model
{
    use HasFactory;
    protected $table = 'qvp_order_customization_details';
    protected $guarded =[];
    public $timestamps = false;

    public function order()
    {
        $this->belongsTo('App\Models\DbModel\QVPOrderDetail');
    }

    public function product()
    {
        $this->belongsTo('App\Models\DbModel\QVPOrderProductsDetail');
    }
}
