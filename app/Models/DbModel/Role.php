<?php

namespace App\Models\DbModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $guarded = [];
    
    public function permissions() {
       return $this->belongsToMany(MasterUiElement::class,'roles_permissions');           
    }

    public function users() {
       return $this->belongsToMany(User::class,'users_roles');           
    }

    public function user_group() {
       return $this->hasOne(UserGroup::class,'ID', 'user_group_id');           
    }
}
