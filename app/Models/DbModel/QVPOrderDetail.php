<?php

namespace App\Models\DbModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QVPOrderDetail extends Model
{
    use HasFactory;
    protected $table = 'qvp_order_details';
    protected $guarded =[];
    public $timestamps = false;
    
    public function users()
    {
        return $this->belongsTo('App\Models\DbModel\User','User_Id');
    }

    // public function productsDetail()
    // {
    //     return $this->belongsTo('App\Models\DbModel\QVPOrderProductsDetail', 'ID', 'Order_Id');
    // }

    public function products()
    {
        return $this->hasMany('App\Models\DbModel\QVPOrderProductsDetail', 'Order_Id', 'ID');
    }

    public function customizations()
    {
        return $this->hasMany('App\Models\DbModel\QVPOrderCustomizationDetail', 'Order_Id', 'ID')->orderBy('ID', 'DESC');
    }
   
}
