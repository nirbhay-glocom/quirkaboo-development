<?php

namespace App\Models\DbModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolesPermission extends Model
{
    use HasFactory;
    protected $table = "roles_permissions";
    protected $guards = [];
    public $timestamps = false; 

    public function permission() {
      return $this->belongsTo(MasterUiElement::class,'Permission_Id','ID');         
    }

    public function roles_permission() {
       return $this->belongsTo(RolesPermission::class,'ID','Permission_Id');           
    }
}
