<?php

namespace App\Models\DbModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QVPOrderProductsDetail extends Model
{
    use HasFactory;
    protected $table = 'qvp_order_products_details';
    protected $guarded =[];
    public $timestamps = false;

    public function users()
    {
        return $this->belongsTo('App\Models\DbModel\User','User_Id');
    }

    // public function ordersDetail(){
    //     return $this->belongsToMany('App\Models\DbModel\QVPOrderDetail','Order_Id','ID');
    // }

    //recently added...
    public function order()
    {
        return $this->belongsTo('App\Models\DbModel\QVPOrderDetail');
    }

    public function customizations() 
    {
        return $this->hasMany('App\Models\DbModel\QVPOrderCustomizationDetail');
    }
}
