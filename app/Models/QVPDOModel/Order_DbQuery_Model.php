<?php

namespace App\Models\QVPDOModel;
use Illuminate\Http\Request;
use App\Models\DbModel\User;
use App\Models\DbModel\QVPOrderDetail;
use App\Models\DbModel\QVPOrderProductsDetail;
use App\Models\DbModel\QVPOrderCustomizationDetail;
use DB;

class Order_DbQuery_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Get All Orders Data Fetch.
    * @param Request $request
    * @return JSON Response
    */
    public static function getAllOrdersInfo(){
        $orders = QVPOrderDetail::with(['users','products','customizations'])->orderBy('ID', 'desc')->get();
        return $orders;       
    }

    public static function getCustomerPreviewData($public_key, $token){
        $orderDetail = QVPOrderDetail::where('Public_Key', $public_key)->where('Token', $token)->first();
        return $orderDetail; 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Order Detail Fetch.
    * @param Request $request
    * @return JSON Response
    */
    public static function getOrderDetail($orderId){
        $orderDetail = QVPOrderDetail::with(['users','products','customizations'])->where('id', $orderId)->first();
        return $orderDetail;        
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Get single row from order customized table data.
    * @param Request $rowId
    * @return Response $query Array
    */
    public static function getOrderCustomizeData($rowId){
        return QVPOrderCustomizationDetail::findOrFail($rowId);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Update Order Information using API.
    * @param Request $request
    * @return Response $orderDetail
    */
    
    public static function updateOrder($request){
        $orderDetail = QVPOrderDetail::where("ID", $request->ID)
            ->update([
                     "Glocom_SKU" => $request->Glocom_SKU,
                     "Vendor_SKU" => $request->Vendor_SKU,
                     "Preview_Needed" => $request->Preview_Needed,
                     "Preview_Due" => $request->Preview_Due,
                     "Preview_Received" => $request->Preview_Received,
                     "Revision_Needed" => $request->Revision_Needed,
                     "Revision_Due" => $request->Revision_Due,
                     "Revision_Received" => $request->Revision_Received,
                     "Warehouse_Delivery_Due" => $request->Warehouse_Delivery_Due,
                     "Warehouse_Delivery_Received" => $request->Warehouse_Delivery_Received,
                     "Customer_Notes" => $request->Customer_Notes,
                     "Vendor_Notes" => $request->Vendor_Notes,
                     "Glocom_Notes" => $request->Glocom_Notes
            ]);
        return $orderDetail;
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Update Order Product Information using API.
    * @param Request $request
    * @return Response $orderProductDetail
    */
    public static function updateOrderProduct($request){
        $orderProductDetail = DB::update('update qvp_order_products_details set Product_Name = ?,Product_Type=?, Variation_1=?, Variation_2=? where ID = ?',[$request->Product_Name,$request->Product_Type, $request->Variation_1, $request->Variation_2, $request->ProdID]);
       
        return $orderProductDetail;
    }

    public static function getAllOrdersImg($request){
       
        $firstImg = QVPOrderCustomizationDetail::where("ID", $request->ID)->first();
        $orderImags = QVPOrderCustomizationDetail::where("Order_Id", $firstImg->Order_Id)->orderBy("ID", "desc")->get();
        return $orderImags;
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 28-02-2022
    * Update preview media status
    * @param Request $params
    * @return Response $order_id
    */
    public static function updatePreviewMediaStatus($order_id, $params){
        $result =  QVPOrderDetail::where("ID", $order_id)
        ->update([
                 "Preview_Media" => $params
        ]);

        if($result){
            return true;
        }else{
            return false;
        }
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 10-03-2022
    * Update preview media notes
    * @param Request $params
    * @return Response $order_id
    */
    public static function updatePreviewMediaNotes($order_id, $params){
        return QVPOrderDetail::where("ID", $order_id)
        ->update([
                 "Preview_Media" => $params
        ]);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 11-03-2022
    * Update gallery media notes
    * @param Request $params
    * @return Response $order_id
    */
    public static function updateGalleryMediaNotes($order_id, $params){
        return QVPOrderDetail::where("ID", $order_id)
        ->update([
                 "Gallery" => $params
        ]);
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 28-02-2022
    * Update gallery media status.
    * @param Request $params
    * @return Response $order_id
    */
    public static function updateGallerywMediaStatus($order_id, $params){
        return QVPOrderDetail::where("ID", $order_id)
        ->update([
                 "Gallery" => $params
        ]);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 19-03-2022
    * Update order custome notes and custom text
    * @param Request $request
    * @return Response true/false
    */
    public static function updateOrderTextNotes($request){
        return QVPOrderDetail::where("ID", $request->Order_Id)
        ->update([
                 "Notes" => $request->Notes,
                 "Custom_Text" => $request->Custom_Text
        ]);
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 24-03-2022
    * Update order vendor notes
    * @param Request $request
    * @return Response true/false
    */
    public static function updateOrderVendorNotes($request){
        return QVPOrderDetail::where("ID", $request->Order_Id)
        ->update([
                 "Vendor_Notes" => $request->Vendor_Notes
        ]);
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 24-03-2022
    * Update order customer notes
    * @param Request $request
    * @return Response true/false
    */
    public static function updateOrderCustomerNotes($request){
        return QVPOrderDetail::where("ID", $request->Order_Id)
        ->update([
                 "Customer_Notes" => $request->Customer_Notes
        ]);
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 24-03-2022
    * Update order gift message
    * @param Request $request
    * @return Response true/false
    */
    public static function updateOrderGiftMessage($request){
        return QVPOrderDetail::where("ID", $request->Order_Id)
        ->update([
                 "Gift_Message" => $request->Gift_Message
        ]);
    }


    

    
}
