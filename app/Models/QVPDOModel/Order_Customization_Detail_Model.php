<?php

namespace App\Models\QVPDOModel;
use Illuminate\Http\Request;
use App\Models\DbModel\User;
use App\Models\DbModel\QVPOrderDetail;
use App\Models\DbModel\QVPOrderProductsDetail;
use App\Models\DbModel\QVPOrderCustomizationDetail;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use DB;
use Validator;
use Carbon\Carbon;
use App\Core\Traits\ImageUploadTrait;

class Order_Customization_Detail_Model
{
    use ImageUploadTrait;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Update Order Information using API.
    * @param Request $request
    * @return Response $orderDetail
    */
    public static function uploadCustomizedImage($params){
        $result = QVPOrderCustomizationDetail::create($params); 
        return $result->id;
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 26-02-2022
    * Update Order Preview image using API.
    * @param Request $params
    * @return Response $OrderId
    */
    public static function uploadPreviewImage($OrderId, $params){
        return QVPOrderDetail::where("ID", $OrderId)
        ->update([
            'Preview_Media' => $params
        ]);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 28-02-2022
    * Update Order Gallery Image using API.
    * @param Request $params
    * @return Response $orderId
    */
    public static function uploadGalleryImage($OrderId, $params){
        return QVPOrderDetail::where("ID", $OrderId)
        ->update([
            'Gallery' => $params
        ]);
    }
    
    /*
    * Author: Nirbhay Sharma
    * Date: 25-02-2022
    * Update Order cutome text using API.
    * @param Request $request
    * @return Response $orderDetail
    */
    public static function updateCustomizationText($request){
        if(!is_null($request->Customize_Text) && !empty($request->Order_Id)){
           return QVPOrderDetail::where("ID", $request->Order_Id)
            ->update([
                    "Custom_Text" => $request->Customize_Text
            ]);
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Delete Customize Image using API.
    * @param Request $customizedRowData
    * @return Response $result
    */
    public static function deleteCustomizeImage($customizedRowData){
        return DB::table('qvp_order_customization_details')
        ->where('ID', $customizedRowData->ID)
        ->delete();
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Update Customize Image Content (Text & Notes) using API.
    * @param Request $request
    * @return Response $result
    */
    public static function updateCustomizeImageContent($request){
        return DB::update(
            'update qvp_order_customization_details set Customize_Text = ?,Notes=? where ID = ?',[$request->Customize_Text,$request->Notes, $request->ID]
        );
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Reject Customize Image using API.
    * @param Request $customizedRowData
    * @return Response $result
    */
    public static function rejectCustomizeImage($customizedRowData){
        return DB::update(
            'update qvp_order_customization_details set Status = ? where ID = ?',['Rejected', $customizedRowData->ID]
        );
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Approve Customize Image using API.
    * @param Request $customizedRowData
    * @return Response $result
    */
    public static function approveCustomizeImage($customizedRowData){
        return DB::update(
            'update qvp_order_customization_details set Status = ? where ID = ?',['Approved', $customizedRowData->ID]
        );
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Update Image Comments using API.
    * @param Request $commentsJsonArr, rowID
    * @return Response $result
    */
    public static function updateImageComments($commentsJsonArr, $rowID){
        return DB::update(
            'update qvp_order_customization_details set Comments = ? where ID = ?',[$commentsJsonArr, $rowID]
        );
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Update Reject Reason.
    * @param Request $commentsJsonArr, rowID, Status
    * @return Response $result
    */
    public static function updateRejectReason($commentsJsonArr, $rowID, $Status){
       return QVPOrderCustomizationDetail::where("ID", $rowID)
        ->update([
                 "Comments" => $commentsJsonArr,
                 "Status" => $Status
        ]);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 02-02-2022
    * Update Notes.
    * @param Request $request
    * @return true or false
    */
    public static function updateCustomeNotes($request){
        return QVPOrderCustomizationDetail::where("ID", $request->ID)
         ->update([
                  "Notes" => $request->Notes
         ]);
     }  
}
