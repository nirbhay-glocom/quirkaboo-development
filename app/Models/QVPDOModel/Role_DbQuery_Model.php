<?php

namespace App\Models\QVPDOModel;
use Illuminate\Http\Request;
use App\Models\DbModel\User;
use App\Models\DbModel\Role;
use App\Models\DbModel\MasterUiElement;
use App\Models\DbModel\UserGroup;
use App\Models\DbModel\RolesPermission;
use DB;

class Role_DbQuery_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get All Role Data Fetch.
    */
    public static function getAllRoles(){
        return Role::with('user_group')->orderBy('id','DESC')->paginate(5);       
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 08-03-2022
    * check page role permission exists
    */
    public static function checkPageRolePermissions($roleId){
        return RolesPermission::with('permission')->where('Role_Id',$roleId)->get();       
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * Get roles and permission setup.
    */
    public static function addedExtraField($roles){
        
        if(isset($roles) && count($roles) > 0){
            foreach($roles as $key => $role){
                   $roles_permissionsExist = RolesPermission::where('Role_Id',$role->id)->where('Permission_Id', 2)->get();
                   if(isset($roles_permissionsExist) && count($roles_permissionsExist) > 0){
                        foreach($roles_permissionsExist as $key2 => $role){
                                    if($role['Visibility'] > 0){
                                        $roles[$key]['pageRoute'] = 'Edit'; 
                                    }else{
                                        $roles[$key]['pageRoute'] = 'Add'; 
                                    }
                            }
                    }else{
                        $roles[$key]['pageRoute'] = 'Add';
                    }
            }
            return $roles;
        }             
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * Get roles and permission setup.
    */
    public static function rolePermissionSetup($roles){
        if(isset($roles) && !empty($roles)){
            $role = [];
            foreach($roles as $key => $role){
                if($role->id){
                   //Role-Permissions Related...
                   $role[] = Role::with('user_group')->where('id', $role->id)->first();
                }
                
            }
            return $role;
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * check permissions
    */
    public static function checkPermissions($pageName, $role_id){

        $permArr = MasterUiElement::where("Type", 'Page')->where("Name", $pageName)->first();
        if(!is_null($permArr) && $permArr->ID){
            $getParentOneRow = MasterUiElement::where("Type",'<>','Page')->where("Parent_Id", $permArr->ID)->first();
            return RolesPermission::where('Role_Id', $role_id)->where('Permission_Id', $getParentOneRow->ID)->first();
        }
        return false;
    }
    

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * Get All Page Permission Data Fetch.
    */
    public static function getAllPagePermissions(){
        return MasterUiElement::where('Type', 'Page')->get();     
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * Get All Container Permission Data Fetch.
    */
    public static function getAllContainerPermissions(){
        return MasterUiElement::where("Type",'<>', 'Page')->get();     
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * Get Role Permission Data Fetch.
    */
    public static function getRolePermission($roleId){
        return RolesPermission::where('Role_Id', $roleId)->get();  
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get All Permission Data Fetch.
    */
    public static function getAllPermissions(){
        return MasterUiElement::get();       
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Get Role by id
    * @param Request $id
    * @return Response Role Array
    */
    public static function getRoleById($role_id){
        return Role::with('user_group')->where('id', $role_id)->first();     
    }

    public static function getUserGroup(){
        return UserGroup::get();
    }

    public static function roleCreate($request, $slug){
        $params = $request->all();
        $params['slug'] = $slug;
        return Role::create($params);
    }

    public static function roleUpdate($request, $slug, $id){
        $params = $request->all();
        $params['slug'] = $slug;
        $role = Role::with('user_group')->where('id', $id)->first();
        return $role->update($params);
    }

    public static function saveRoleIntoRolePermissionTbl($roleId){
        if(!empty($roleId)){
            //Save Role into Role-Permissions table...
            $permissions = MasterUiElement::where('Type', 'Page')->get();
            $paramsPageArray = [];
            foreach($permissions as $value){ 
                    $paramsPageArray[] = [
                        'Role_Id' => $roleId,
                        'Permission_Id' => $value->ID,
                        'Visibility' => 0,
                        'View' => 0,
                        'View_And_Edit' => 0
                    ];
            }
           return RolesPermission::insert($paramsPageArray);
        } 
    }

    public static function savePagePermissions($request, $setPageArr){
        if(isset($setPageArr) && !empty($setPageArr)){
            if($request->pageExists==0){  
                return RolesPermission::insert($setPageArr);
            }
        }
    }

    public static function updatePagePermissions($request){
        if(isset($request->PageVisibility) && !empty($request->PageVisibility) && !empty($request->Role_Id)){
            foreach($request->PageVisibility as $permission_Id => $value){ 
                if($request->pageExists > 0){
                    RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $permission_Id)
                    ->update([                        
                        'Visibility' => $value
                    ]);
                }
            }
            return true;
        }
    }

    public static function saveRolePermissions($defaultArr){
        if(isset($defaultArr) && !empty($defaultArr)){
            return RolesPermission::insert($defaultArr);
        }
    }

    public static function saveContainerPermissions($request){
        if(isset($request->PageVisibility) && !empty($request->PageVisibility) && !empty($request->Role_Id)){
            if($request->OrderStatus==1){

                //Save Container fields...
                if(isset($request->Visibility) && !empty($request->Visibility) && !empty($request->Role_Id)){

                    $paramsArray = [];
                    foreach($request->Visibility as $permission_Id => $value){
                        if($value){
                            $visiblity = $value; 
                        }else{
                            $visiblity = 0;
                        }
                        if($permission_Id > 0){
                            
                            RolesPermission::where("Role_Id", $request->Role_Id)->where("Permission_Id", $permission_Id)
                            ->update([                        
                                    
                                    'Visibility' => $visiblity,
                                    'View' => 0,
                                    'View_And_Edit' => 0   
                            ]);
                        }                         
                    }
                }

                //Update View....
                if(isset($request->View) && !empty($request->View) && !empty($request->Role_Id)){
                    $paramsArray = [];
                    foreach($request->View as $permission_Id => $value){
                        RolesPermission::where("Role_Id", $request->Role_Id)->where("Permission_Id", $permission_Id)
                        ->update([                        
                                'View' => $value   
                        ]);
                    }
                }

                //Update View_And_Edit....
                if(isset($request->View_And_Edit) && !empty($request->View_And_Edit) && !empty($request->Role_Id)){
                    $paramsArray = [];
                    foreach($request->View_And_Edit as $permission_Id => $value){
                        RolesPermission::where("Role_Id", $request->Role_Id)->where("Permission_Id", $permission_Id)
                        ->update([                        
                                'View_And_Edit' => $value
                                
                        ]);
                    }
                }
                return true;
            }            
        }
    }

    public static function updateContainerPermissions($request){
        if(isset($request->PageVisibility) && !empty($request->PageVisibility) && !empty($request->Role_Id)){
            if($request->OrderStatus==1){

                if(isset($request->Visibility) && !empty($request->Visibility) && !empty($request->Role_Id)){

                    $paramsArray = [];
                    foreach($request->Visibility as $rowID => $value){
                        if($value){
                            $visiblity = $value; 
                        }else{
                            $visiblity = 0;
                        }
                        if($rowID > 0){
                            
                            RolesPermission::where("ID", $rowID)
                            ->update([                        
                                    
                                    'Visibility' => $visiblity,
                                    'View' => 0,
                                    'View_And_Edit' => 0 
                            ]);
                        }
                         
                    }
                }
    
                //Update View....
                 if(isset($request->View) && !empty($request->View) && !empty($request->Role_Id)){
                    $paramsArray = [];
                    foreach($request->View as $rowID => $value){
                        RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $rowID)
                        ->update([                        
                                 'View' => $value,
                                 'Visibility' => 0,
                                 'View_And_Edit' => 0
    
                        ]);
                    }
                 }
    
                //Update View_And_Edit....
                 if(isset($request->View_And_Edit) && !empty($request->View_And_Edit) && !empty($request->Role_Id)){
                    $paramsArray = [];
                    foreach($request->View_And_Edit as $rowID => $value){
                        RolesPermission::where("Role_Id", $request->Role_Id)->where("ID", $rowID)
                        ->update([                        
                                 'View_And_Edit' => $value,
                                 'Visibility' => 0,
                                 'View' => 0
    
    
                        ]);
                    }
                 }
                return true;
            }            
        }
    }

    public static function getPermissionsUiTree($container_permissions, $role_id){
        //$permissionsArr = MasterUiElement::where("Type",'<>', 'Page')->get()->toArray();
        //$container_permissions = Role_Business_Model::buildTree($permissionsArr);
        $defaultArr = [];
        if(isset($container_permissions) && !empty($container_permissions) && !empty($role_id)){
          
            foreach($container_permissions as $key1 => $tab){
                
                $defaultArr[] = [
                            'Role_Id' => $role_id,
                            'Permission_Id' => $tab['ID'],
                            'Visibility' => 0,
                            'View' => 0,
                            'View_And_Edit' => 0
                    ]; 
                    if(isset($tab['childs']) && !empty($tab['childs'])){
                        foreach($tab['childs'] as $key2 => $section){                           

                            $defaultArr[] = [
                                    'Role_Id' => $role_id,
                                    'Permission_Id' => $section['ID'],
                                    'Visibility' => 0,
                                    'View' => 0,
                                    'View_And_Edit' => 0
                            ]; 

                                    if(isset($section['childs']) && !empty($section['childs'])){
                                          foreach($section['childs'] as $key3 => $field){
                                            
                                            $defaultArr[] = [
                                                    'Role_Id' => $role_id,
                                                    'Permission_Id' => $field['ID'],
                                                    'Visibility' => 0,
                                                    'View' => 0,
                                                    'View_And_Edit' => 0
                                            ]; 
                                            
                                          }
                                   }

                        }  
                    }
            }
        
            return $defaultArr;
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 22-02-2022
    * Get Role by name
    * @param Request $id
    * @return Response Role Array
    */
    public static function getRoleByName($name){
        return Role::where('name', $name)->first();    
    }


    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * create Role
    * @param Request $request
    * @return true or false
    */
    public static function createRole($request){
        return Role::create(['name' => $request->input('name')]);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Join permissions with role
    * @param Request $id
    * @return arr
    */
    // public static function rolePermissionsJoin($id){
    //     return Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
    //     ->where("role_has_permissions.role_id",$id)
    //     ->get();    
    // }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Get All Role has Permissions array
    * @param Request $id
    * @return arr
    */
    // public static function getAllRolePermissions($id){
    //     return DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
    //     ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
    //     ->all();    
    // }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Update Role
    * @param Request $request, $role
    * @return true or false
    */
    public static function updateRole($request, $role){
        $role->name = $request->input('name');
        return $role->save(); 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Delete Role
    * @param Request $id
    * @return true or false
    */
    public static function deleteRoleById($id){
        return DB::table("roles")->where('id',$id)->delete();    
    }
}
