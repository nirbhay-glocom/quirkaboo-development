<?php

namespace App\Models\QVPDOModel;
use Illuminate\Http\Request;
use App\Models\DbModel\User;
use App\Models\DbModel\UserInfo;
use App\Models\DbModel\Role;
use DB;

class User_DbQuery_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get All Users Data Fetch.
    * @param Request $request
    * @return Users Array
    */
    public static function getAllUsers(){
        return User::with('roles')->orderBy('id','DESC')->paginate(2);        
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get User Data Fetch By ID.
    * @param Request $id
    * @return User Array
    */
    public static function getUserDetailById($id){
        return User::with('roles')->find($id);        
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get All Roles Data Fetch.
    * @param Request $request
    * @return Response Roles Array
    */
    public static function getAllRoles(){
        return Role::pluck('name','name')->all();        
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Get User Role
    * @param Request $user and $roles
    * @return Response Role Array
    */
    public static function getUserRole($user, $roles){
        return $user->roles->pluck('name','name')->all();      
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * create User
    * @param Request $request
    * @return 
    */
    public static function createUser($input){
        if(isset($input['Vendors']) && !empty($input['Vendors'])){
            $input['Vendors'] = implode(',', $input['Vendors']);
        }else{
            $input['Vendors'] = null;
        }
        return User::create($input);     
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Update User
    * @param Request $request
    * @return 
    */
    public static function updateUser($user, $input){
        if(isset($input['Vendors']) && !empty($input['Vendors'])){
            $input['Vendors'] = implode(',', $input['Vendors']);
        }else{
            $input['Vendors'] = null;
        }      
        return $user->update($input);     
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Delete User
    * @param Request $user and $roles
    * @return true or false
    */
    public static function deleteUserById($id){
        return User::find($id)->delete();     
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Delete Model Has Role
    * @param Request $user and $roles
    * @return true or false
    */
    public static function deleteModelHasRole($id){
        return DB::table('users_roles')->where('user_id',$id)->delete();
    }
}
