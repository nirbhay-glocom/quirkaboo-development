<?php

namespace App\Models\QVPDOModel;
use Illuminate\Http\Request;
use App\Models\DbModel\User;

class User_Create_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 03-01-2022
    * User SignUp Form Submit using API.
    * @param Request $request
    * @return Response $success
    */
    public static function user_signup($request){

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('authToken')->accessToken;
        $success['user'] = $user;
        
        return $success;
    }
}
