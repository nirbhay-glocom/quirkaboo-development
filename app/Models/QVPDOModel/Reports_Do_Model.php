<?php
namespace App\Models\QVPDOModel;

use App\Http\Controllers\Controller;
use DB;

/**
 * class implement 
 */
class Reports_Do_Model {

	public function Get_Missing_Variants_Price_Stock() {

		$tableName = config('constants.RETAIL_VARIANTS');
		$tableName2 = config('constants.RETAIL_PRICE_STOCK');

		$sql = "SELECT rv.Variant_SKU, rv.Created_At AS 'Creation_Date' FROM `$tableName` as rv LEFT JOIN `$tableName2` as rps ON rps.Variant_SKU = rv.Variant_SKU WHERE rps.Variant_SKU IS NULL ORDER BY rv.ID";
		$missingVariantsData =  DB::select($sql);

		/*applied object to array format */
		$missingVariantsDataArray = collect($missingVariantsData)->map(function($x){ return (array) $x; })->toArray(); 
		
		return $missingVariantsDataArray;
	} // Get_Missing_Variants_Price_Stock

	public function Initiate_Email_Queue($subject,$from,$to,$reply_to,$cc,$content,$priority,$status,$send_at, $attachment_url,$notification=false)
	{
		if($priority=="Immediate")
		{

			$insertvalues=['Subject'=>$subject,'Content'=>addslashes($content),'From_Email'=>$from,'To_Email'=>$to,'Reply_To'=>$reply_to,'Attachment_Url'=>$attachment_url,'CC'=>$cc,'Priority'=>$priority,'Status'=>$status,'Send_At'=>now(3),'Created_on'=>date("Y-m-d H:i:s")];


		}
		else
		{

			$insertvalues=['Subject'=>$subject,'Content'=>$content,'From_Email'=>$from,'To_Email'=>$to,'Reply_To'=>$reply_to,'Attachment_Url'=>$attachment_url,'CC'=>$cc,'Priority'=>$priority,'Status'=>$status,'Send_At'=>$send_at,'Created_on'=>date("Y-m-d H:i:s")];

		}
		$lastInsertedID = DB::table('Email_Queue')->insertGetId($insertvalues);

		if($lastInsertedID != '')
		{	
			if( $notification ) {
				$notificationTo = explode(",", $to);
				if(!empty($cc)) {
					$notificationCC = explode(",", $cc);
					$totalToAddress = array_merge($notificationTo, $notificationCC);
				} else {
					$totalToAddress = $notificationTo;
				}

				$notificationFinalArray = array();

				foreach($totalToAddress as $key => $value) {
					$notificationFinalArray[$key]['Email_Queue_ID'] = $lastInsertedID;
					$notificationFinalArray[$key]['To_Email'] = $value;
					$notificationFinalArray[$key]['Notification_Status'] = $status;
				}
				if(count($notificationFinalArray) > 0) {
					DB::table('Notification_Queue')->insert($notificationFinalArray);
				}
			}

			return "Success";
		}
		else
		{
			return $lastInsertedID->num_rows();
		}
	}

	public function Get_Email_Campaign_Details($campaign_id)
	{
		$sql="select email_template.Template_ID, email_campaign.Subject,email_campaign.Type, email_campaign.From_Email, email_campaign.To_Email,email_campaign.Additional_Recepient, email_campaign.CC,email_campaign.Reply_To, email_campaign.Attachment_Url, email_tags.Tags from Email_Campaign email_campaign left join Email_Template email_template on email_template.Template_ID=email_campaign.Template_ID left join Email_Module_Tags email_tags on email_template.Module_ID=email_tags.ID where email_campaign.Email_Campaign_ID='".$campaign_id."'";
		//echo $sql;
		$result=DB::select($sql);
		$response=array();
		if($result)
		{
			if (count($result)==1)
			{
				$row = array_map(function ($value) {
					return (array)$value;
				}, $result);
				$content_to_replace="";

				 if(is_array($row[0]))
				{
					$response["Subject"]=$row[0]["Subject"];
					$response["From_Email"]=$row[0]["From_Email"];

					if($row[0]["Additional_Recepient"]!=""&&$row[0]["Additional_Recepient"]!=NULL)
					{ $response["To_Email"]=$row[0]["To_Email"].$row[0]["Additional_Recepient"]; }
					else
					{$response["To_Email"]=$row[0]["To_Email"];}

					$response["CC"]=$row[0]["CC"];
					$response["Reply_To"]=$row[0]["Reply_To"]; 
					// $response["BCC"]=$row[0]["BCC"];
					$response["Attachment_Url"]=$row[0]["Attachment_Url"];
					$response["Template_ID"]=$row[0]["Template_ID"];
					$response["Tags"]=$row[0]["Tags"];
					$response["Type"]=$row[0]["Type"];
					$response["Additional_Recepient"]=$row[0]["Additional_Recepient"];
				}
				else
				{
					$response["Subject"]=$row["Subject"];
					$response["From_Email"]=$row["From_Email"];
					if($row[0]["Additional_Recepient"]!=""&&$row["Additional_Recepient"]!=NULL)
					{ $response["To_Email"]=$row["To_Email"].$row["Additional_Recepient"]; }
					else
					{$response["To_Email"]=$row["To_Email"];}
					$response["CC"]=$row["CC"];
					// $response["BCC"]=$row["BCC"];
					$response["Reply_To"]=$row["Reply_To"]; 
					$response["Attachment_Url"]=$row["Attachment_Url"];
					$response["Template_ID"]=$row["Template_ID"];
					$response["Tags"]=$row["Tags"];
					$response["Type"]=$row["Type"];
					$response["Additional_Recepient"]=$row["Additional_Recepient"];
				}


				$response["err"]="Success";
			}
		    else
		    {
			  $response["err"]="Records fetched more than expected";
		    }

		}
		else
		{
			$response["err"]="Mysql Error : ".$this->db->_error_message();
		}
		return $response;
	} //Get_Email_Campaign_Details

	public function Get_Subject($campaign_id,$subject_tags)
	{
		$sql="select Subject from Email_Campaign where Email_Campaign_ID='".$campaign_id."'";

		$result = DB::select($sql);
		if($result)
		{
			if(count($result)==1)
			{
				$row = array_map(function ($value) {
					return (array)$value;
				}, $result);
				$content_to_replace="";
				if(is_array($row[0]))
				{

					$content_to_replace=$row[0]["Subject"];
				}
				else
				{
					$content_to_replace=$row["Subject"];
				}

				foreach($subject_tags as $key=>$value)
				{
						$content_to_replace=str_replace('#'.$key,  $value,  $content_to_replace);

				}

				$content=$content_to_replace;
				$response["err"]="Success";
				// $response["subject"]=strip_tags(html_entity_decode(utf8_decode($content)));
				$response["subject"]=strip_tags(html_entity_decode($content));
		   }
		   else
		   {
			  $response["err"]="Records fetched more than expected";
		   }

		 }
		 else
		 {
			$response["err"]="Mysql Error : ".$this->db->_error_message();
		 }
		 return $response;
	} //Get_Subject

	public function Get_Email_Content_From_Template($template_id,$template_data) //@template_id pk of master_email_template integer,@template_data array['tag_name1'=>tag_value1,'tag_name2'=>tag_value2]
	{
		$sql="select Template_Content from Email_Template where Template_ID='".$template_id."'";

		$result = DB::select($sql);
		if($result)
		{
			if (count($result)==1)
			{
				$row = array_map(function ($value) {
					return (array)$value;
				}, $result);
				$content_to_replace="";
				if(is_array($row[0]))
				{

					$content_to_replace=$row[0]["Template_Content"];
				}
				else
				{
					$content_to_replace=$row["Template_Content"];
				}

				foreach($template_data as $key=>$value)
				{
						$content_to_replace=str_replace('#'.$key,  $value,  $content_to_replace);

				}

				$content=$content_to_replace;
				$response["err"]="Success";
				$response["content"]=html_entity_decode($content);
		   }
		   else
		   {
			  $reponse["err"]="Records fetched more than expected";
		   }

		 }
		 else
		 {
			$reponse["err"]="DB Error : ".$this->db->_error_message();
		 }
		 return $response;
	} //Get_Email_Content_From_Template


	public function Get_Total_Stock_Unmatch_Data($sumOfStock, $sumOfReturnedStock) {
        $sql = "SELECT Variant_SKU
                    FROM Retail_Price_Stock 
                    WHERE Received_Quantity != (Total_Sale_Quantity + ($sumOfStock)) - ($sumOfReturnedStock) ";

		$stockUnmatchDataOne =  DB::select($sql);

		/*applied object to array format */
		$stockUnmatchDataOneArray = collect($stockUnmatchDataOne)->map(function($x){ return (array) $x; })->toArray(); 

		return $stockUnmatchDataOneArray;
    } //Get_Total_Stock_Unmatch_Data

	public function Get_Total_Stock_Unmatch_Data_Issue_Two() {
        $sql = "SELECT Variant_SKU
					FROM Retail_Price_Stock
					WHERE (Total_Sale_Quantity + Awaiting_Dispatch_UK + Awaiting_Dispatch_IN + Awaiting_Dispatch_US + Awaiting_Dispatch_CD + Awaiting_Dispatch_Asia + Awaiting_Dispatch_FBA_IN) != ((Warehouse_Stock_CD + Awaiting_Dispatch_CD) + (Warehouse_Stock_IN + Awaiting_Dispatch_IN) + (Warehouse_Stock_UK + Awaiting_Dispatch_UK) + (Warehouse_Stock_US + Awaiting_Dispatch_US) + (Warehouse_Stock_Asia + Awaiting_Dispatch_Asia) + (Warehouse_Stock_FBA_IN + Awaiting_Dispatch_FBA_IN))";
        $stockUnmatchDataTwo =  DB::select($sql);

		/*applied object to array format */
		$stockUnmatchDataTwoArray = collect($stockUnmatchDataTwo)->map(function($x){ return (array) $x; })->toArray(); 

		return $stockUnmatchDataTwoArray;
    } //Get_Total_Stock_Unmatch_Data_Issue_Two

	public function Get_Total_Price_Stock_Issue_Three() {
        $sql = "SELECT Variant_SKU, Warehouse_Stock_IN, Warehouse_Stock_CD, Warehouse_Stock_UK, Warehouse_Stock_US, Warehouse_Stock_Asia, Warehouse_Stock_FBA_IN
                    FROM Retail_Price_Stock ";
        $stockUnmatchDataThree =  DB::select($sql);

		/*applied object to array format */
		$stockUnmatchDataThreeArray = collect($stockUnmatchDataThree)->map(function($x){ return (array) $x; })->toArray(); 

		return $stockUnmatchDataThreeArray;
    } //Get_Total_Price_Stock_Issue_Three

	public function Get_Total_Storage_Issue_Three() {
        $sql = "SELECT Variant_SKU,
                    COUNT(case when Item_Available = 'Yes' AND Warehouse='IN' then 1 else null end) AS Warehouse_Stock_IN,
                    COUNT(case when Item_Available = 'Yes' AND Warehouse='CD' then 1 else null end) AS Warehouse_Stock_CD,
                    COUNT(case when Item_Available = 'Yes' AND Warehouse='UK' then 1 else null end) AS Warehouse_Stock_UK,
                    COUNT(case when Item_Available = 'Yes' AND Warehouse='US' then 1 else null end) AS Warehouse_Stock_US,
                    COUNT(case when Item_Available = 'Yes' AND Warehouse='Asia' then 1 else null end) AS Warehouse_Stock_Asia,
                    COUNT(case when Item_Available = 'Yes' AND Warehouse='FBA_IN' then 1 else null end) AS Warehouse_Stock_FBA_IN
                    FROM Retail_Warehouse_Storage GROUP BY Variant_SKU ";
         $storageDataThree =  DB::select($sql);

		 /*applied object to array format */
		 $storageDataThreeArray = collect($storageDataThree)->map(function($x){ return (array) $x; })->toArray(); 
 
		 return $storageDataThreeArray;
    } //Get_Total_Storage_Issue_Three

	public function Get_Retail_Products() {
		
		$tableName = config('constants.RETAIL_PRODUCTS');

		$sql = "SELECT DISTINCT Department, Vendor_Code from `$tableName`";

		$getProductData = DB::select($sql);
		return $getProductData;
	} //Get_Retail_Products

	public function Get_Master_Accounts_Retail() {
		$tableName = config('constants.MASTER_ACCOUNTS_RETAIL');

		$sql = "SELECT Sales_Channel, Store_Name from `$tableName` where Store_Sales_Status = 'Active'";

		$getMasterAccountRetail = DB::select($sql);
		return $getMasterAccountRetail;
	} //Get_Master_Accounts_Retail

	public function Get_Missing_Channel_Configuration($department, $vendorCode, $salesChannel, $storeName) {

		$tableName = config('constants.RETAIL_CHANNEL_CONFIGURATION');

		$sql="SELECT Department, Vendor_Code, Sales_Channel, Store_Name FROM `$tableName` where Department='".$department."' AND Vendor_Code='".$vendorCode."' AND Sales_Channel='".$salesChannel."' AND Store_Name='".$storeName."'"; 
		
		$missingChannelConfigData =  DB::select($sql);
 
		/*applied object to array format */
		$missingChannelConfigDataArray = collect($missingChannelConfigData)->map(function($x){ return (array) $x; })->toArray();   
		return $missingChannelConfigDataArray;
	} //Get_Missing_Channel_Configuration

	public function Get_Missing_Vendor_Retail_Products() {
		$tableName = config('constants.VENDOR_RETAIL_PRODUCTS');

		$sql="SELECT `Parent_SKU`, `Product_Status`, `Label`, `Created_At`, `Modified_At` FROM `$tableName` WHERE Product_Status != 'Catalogue Started' AND DATEDIFF(NOW(),Modified_At) > 7"; 
		
		$getVendorRetailProductsData =  DB::select($sql);
 
		/*applied object to array format */
		$getVendorRetailProductsDataArray = collect($getVendorRetailProductsData)->map(function($x){ return (array) $x; })->toArray();   
		return $getVendorRetailProductsDataArray;
	} //Get_Missing_Vendor_Retail_Products

	public function Get_Missing_Retail_Products() {
		$tableName = config('constants.RETAIL_PRODUCTS');

		$sql="SELECT `Parent_SKU`,`Catalogue_Status`,`Label`,`Created_At`,`Modified_At` FROM `$tableName` WHERE `Catalogue_Status` != 'Completed' AND DATEDIFF(NOW(),Modified_At) > 7"; 
		
		$getRetailProductsData =  DB::select($sql);
 
		/*applied object to array format */
		$getRetailProductsDataArray = collect($getRetailProductsData)->map(function($x){ return (array) $x; })->toArray();   
		return $getRetailProductsDataArray;
	} //Get_Missing_Retail_Products

	public function Get_Missing_Retail_Listing_Library() {
		$tableName = config('constants.RETAIL_LISTING_LIBRARY');

		$sql="SELECT `Parent_SKU`,`Sales_Channel`,`Store_Name`,`Label`,`Channel_Catalogue_Status`,`Modified_At` as `Channel_Catalogue_Last_Modified` FROM `$tableName` WHERE `Channel_Catalogue_Status` != 'Completed' AND DATEDIFF(NOW(),Modified_At) > 7"; 
		
		$getRetailListingLibraryData =  DB::select($sql);
 
		/*applied object to array format */
		$getRetailListingLibraryDataArray = collect($getRetailListingLibraryData)->map(function($x){ return (array) $x; })->toArray();   
		return $getRetailListingLibraryDataArray;
	} //Get_Missing_Retail_Listing_Library

	public function Get_Missing_Retail_Variants() {
		$tableName = config('constants.RETAIL_PRICE_STOCK');

		$tableName2 = config('constants.RETAIL_VARIANTS');

		$sql="SELECT rps.Variant_SKU FROM `$tableName` as rps LEFT OUTER JOIN `$tableName2` as rv on rps.Variant_SKU = rv.Variant_SKU WHERE rv.Variant_SKU IS NULL";
		
		$getRetailVariantPriceData =  DB::select($sql);
 
		/*applied object to array format */
		$getRetailVariantPriceDataArray = collect($getRetailVariantPriceData)->map(function($x){ return (array) $x; })->toArray();   
		return $getRetailVariantPriceDataArray;
	} //Get_Missing_Retail_Variants

	public function Get_Missing_Retail_Price_Stock() {
		$tableName = config('constants.RETAIL_VARIANTS');

		$tableName2 = config('constants.RETAIL_PRICE_STOCK');

		$sql="SELECT rv.Variant_SKU FROM `$tableName` as rv LEFT OUTER JOIN `$tableName2` as rps on rv.Variant_SKU = rps.Variant_SKU WHERE rps.Variant_SKU IS NULL";
		
		$getRetailPriceData =  DB::select($sql);
 
		/*applied object to array format */
		$getRetailPriceDataArray = collect($getRetailPriceData)->map(function($x){ return (array) $x; })->toArray();   
		return $getRetailPriceDataArray;
	} //Get_Missing_Retail_Price_Stock

	public function Insert_Missing_Channel_Config($department, $vendorCode, $salesChannel, $storeName, $defaultSelection, $status) {

		$tableName = config('constants.RETAIL_CHANNEL_CONFIGURATION');

		$sql = "INSERT INTO `$tableName`(`ID`,`Department`,`Sales_Channel`,`Store_Name`,`Vendor_Code`,`Default_Selection`,`Status`) VALUES ('','".$department."','".$salesChannel."','".$storeName."','".$vendorCode."','".$defaultSelection."','".$status."')"; 

        $insertQuery =  DB::insert($sql);

		return $insertQuery;
	} //Insert_Missing_Channel_Config

	public function Get_Wish_Override_Data() {
		$tableName = config('constants.RETAIL_PRICE_STOCK');
		$tableName2 = config('constants.RETAIL_LISTING_WISH_GLOCOM-TRADE');

		$sql = "SELECT rw.Parent_SKU, rw.Variant_SKU, rw.Seller_Parent_SKU, rw.Seller_Variant_SKU, rps.Warehouse_Stock_UK, rw.Channel_Sale_Price_Override, rw.Quantity_Override, '' AS Label FROM `$tableName` rps
					INNER JOIN `$tableName2` rw ON rps.Variant_SKU = rw.Variant_SKU
					WHERE rps.Warehouse_Stock_UK = '0' AND rw.Quantity_Override != '0' AND rw.Quantity_Override != '' AND rw.Quantity_Override IS NOT NULL"; 


        $selectQuery =  DB::select($sql);

		/*applied object to array format */
		$SelectQueryArray = collect($selectQuery)->map(function($x){ return (array) $x; })->toArray();   
		return $SelectQueryArray;
	}

	public function Get_Listing_Error_Status_Data() {
		$retailListingLibraryTable = config('constants.RETAIL_LISTING_LIBRARY');
		$retailListingLibraryColumnNames = config('constants.RETAIL_LISTING_LIBRARY_COLUMN_EXPLICIT');

		$GetListingErrorSql = "SELECT $retailListingLibraryColumnNames FROM `$retailListingLibraryTable` WHERE Listing_Integration_Status = 'Listing Error'";
		$ListingErrorQuery =  DB::select($GetListingErrorSql);

		/*applied object to array format */
		$ListingErrorQueryArray = collect($ListingErrorQuery)->map(function($x){ return (array) $x; })->toArray(); 
		
		return $ListingErrorQueryArray;

	} //Get_Listing_Error_Status_Data

	public function Get_Update_Catalogue_Status_Data() {
		$retailListingLibraryTable = config('constants.RETAIL_LISTING_LIBRARY');
		$retailListingLibraryColumnNames = config('constants.RETAIL_LISTING_LIBRARY_COLUMN_EXPLICIT');

		$GetUpdateCatalogueSql = "SELECT $retailListingLibraryColumnNames FROM `$retailListingLibraryTable` WHERE `Channel_Catalogue_Status` = 'Update Catalogue' AND Modified_At <= CURDATE()- INTERVAL 1 DAY";
		$UpdateCatalogueQuery =  DB::select($GetUpdateCatalogueSql);

		/*applied object to array format */
		$UpdateCatalogueQueryArray = collect($UpdateCatalogueQuery)->map(function($x){ return (array) $x; })->toArray(); 
		
		return $UpdateCatalogueQueryArray;

	} //Get_Update_Catalogue_Status_Data

	public function Get_Processing_Status_Data() {
		$retailListingLibraryTable = config('constants.RETAIL_LISTING_LIBRARY');
		$retailListingLibraryColumnNames = config('constants.RETAIL_LISTING_LIBRARY_COLUMN_EXPLICIT');

		$GetProcessingStatusSql = "SELECT $retailListingLibraryColumnNames FROM `$retailListingLibraryTable` WHERE `Listing_Integration_Status` = 'Processing'";
		$ProcessingDataQuery =  DB::select($GetProcessingStatusSql);

		/*applied object to array format */
		$ProcessingDataQueryArray = collect($ProcessingDataQuery)->map(function($x){ return (array) $x; })->toArray(); 
		
		return $ProcessingDataQueryArray;

	} //Get_Processing_Status_Data

	public function Get_Listing_Integration_Status_Data() {
		$retailListingLibraryTable = config('constants.RETAIL_LISTING_LIBRARY');
		$retailListingLibraryColumnNames = config('constants.RETAIL_LISTING_LIBRARY_COLUMN_EXPLICIT');

		$ListingIntegrationStatusSql = "SELECT $retailListingLibraryColumnNames FROM `$retailListingLibraryTable` WHERE (`Listing_Integration_Status` = 'New Product' OR `Listing_Integration_Status` = 'New Variant' OR `Listing_Integration_Status` = 'Listing Update') AND (Modified_At <= CURDATE()- INTERVAL 2 DAY)";
		$ListingIntegrationStatusQuery =  DB::select($ListingIntegrationStatusSql);

		/*applied object to array format */
		$ListingIntegrationStatusQueryArray = collect($ListingIntegrationStatusQuery)->map(function($x){ return (array) $x; })->toArray(); 
		
		return $ListingIntegrationStatusQueryArray;

	} //Get_Listing_Integration_Status_Data
	
	public function Get_Missed_Daily_Report($Report_Date="", $Exclude_Users=""){
		$tableName1 			= config('constants.MASTER_USERS');
		$tableName2 			= config('constants.USER_TASK_REPORT');

		$ExcludeUsers 			= "'". implode ( "','", $Exclude_Users ) ."'";

		$holidaydateSQL 		= "SELECT `Date` from Holiday_Calendar WHERE `Date`= '$Report_Date'"; // Check if holiday or not
		$holidaydateSQLResult 	=  DB::select($holidaydateSQL);

		$holidayDate 			= false;
		if( isset($holidaydateSQLResult[0]) && $holidaydateSQLResult[0]->Date != '' ){
			$holidayDate 		= true;
		}

		if( $holidayDate  == false ) {
			$GetMissedDailyReportSql = "SELECT MU.Name as 'User Name', MU.Email_ID FROM `$tableName1` as MU
										LEFT OUTER JOIN `$tableName2` as UTR on MU.Email_ID = UTR.User_Name 
										AND UTR.Report_Date='$Report_Date' 
										AND UTR.Daily_Report != ''
										WHERE MU.EMAIL_ID NOT IN ($ExcludeUsers)
										AND UTR.User_Name IS NULL";
			
			$GetMissedDailyReportQuery =  DB::select($GetMissedDailyReportSql);

			/*applied object to array format */
			$GetMissedDailyReportQueryArray = collect($GetMissedDailyReportQuery)->map(function($x){ return (array) $x; })->toArray(); 

			return $GetMissedDailyReportQueryArray;
		}
	} //Get_Missed_Daily_Report

	public function Get_Report_Analytics($reportLevel="", $rowID="") {
		$tableName1 			 = config('constants.REPORT_ANALYTICS');

		if($reportLevel != "" && $rowID != "") {
			$GetReportAnalyticsSql   = "SELECT * FROM `$tableName1` WHERE `ID`=$rowID";
		}
		else{
			$GetReportAnalyticsSql   = "SELECT * FROM `$tableName1`";
		}
		$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);

		/*applied object to array format */
		$GetReportAnalyticsQueryArray = collect($GetReportAnalyticsQuery)->map(function($x){ return (array) $x; })->toArray(); 

		return $GetReportAnalyticsQueryArray;
	} //Get_Report_Analytics

	public function Get_Tab_Menus($level, $menus) {
		$tableName1 			  = config('constants.REPORT_ANALYTICS');

		// $GetReportAnalyticsSql   = "SELECT Level1,Level2,Level3,Level4,Level5 FROM `$tableName1` WHERE `$level` Like '$menus'";
		
		if($level == "level2" && $menus != "") {
			$GetReportAnalyticsSql   = "SELECT GROUP_CONCAT(DISTINCT Level2 ORDER BY ID ASC SEPARATOR ', ') as sub_menu FROM `$tableName1` WHERE `ID` IN ($menus) AND (`Level2` <> '') GROUP BY `Level2`";
			$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);
		}
		elseif($level == "level3" && $menus != "") {
			$GetReportAnalyticsSql   = "SELECT GROUP_CONCAT(DISTINCT Level3 ORDER BY ID ASC SEPARATOR ', ') as sub_menu FROM `$tableName1` WHERE `ID` IN ($menus) AND (`Level3` <> '') GROUP BY `Level3`";
			$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);
		}
		elseif($level == "level4" && $menus != "") {
			$GetReportAnalyticsSql   = "SELECT GROUP_CONCAT(DISTINCT Level4 ORDER BY ID ASC SEPARATOR ', ') as sub_menu FROM `$tableName1` WHERE `ID` IN ($menus) AND (`Level4` <> '') GROUP BY `Level4`";
			$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);
		}
		elseif($level == "level5" && $menus != "") {
			$GetReportAnalyticsSql   = "SELECT GROUP_CONCAT(DISTINCT Level5 ORDER BY ID ASC SEPARATOR ', ') as sub_menu FROM `$tableName1` WHERE `ID` IN ($menus) AND (`Level5` <> '') GROUP BY `Level5`";
			$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);
		}
		else{
			$GetReportAnalyticsSql   = "SELECT `Level1`, GROUP_CONCAT(ID ORDER BY ID ASC SEPARATOR ', ') as Menus_IDs FROM `$tableName1` GROUP BY `Level1`";
			$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);
		}

		/*applied object to array format */
		$GetReportAnalyticsQueryArray = collect($GetReportAnalyticsQuery)->map(function($x){ return (array) $x; })->toArray(); 

		return $GetReportAnalyticsQueryArray;
	} //Get_Report_Analytics

	public function Get_Report_Analytics_Multi_Level($reportLevel="", $rowID="") {
		$tableName1 	= config('constants.REPORT_ANALYTICS');
		if($reportLevel < 5) {
			$next 			= $reportLevel+1;
		}
		else{
			$next 			= $reportLevel;
		}

		if($reportLevel != "" && $rowID != "") {
			$GetReportAnalyticsSql   = "SELECT `Filter$reportLevel` as filter, `Filter$next` as nextFilter, `Field_Value1` as NewProduct1, `Field_Previous_Value1` as NewProduct2, `Field_Value2` as PendingCatalogue1, `Field_Value3` as Exception1, `Created_At` as Reported_Date FROM `$tableName1` WHERE `ID`=$rowID";
			
			$GetReportAnalyticsQuery =  DB::select($GetReportAnalyticsSql);

			/*applied object to array format */
			$GetReportAnalyticsQueryArray = collect($GetReportAnalyticsQuery)->map(function($x){ return (array) $x; })->toArray(); 
	
			return $GetReportAnalyticsQueryArray;
		}
	}
} // ReportsDoModel