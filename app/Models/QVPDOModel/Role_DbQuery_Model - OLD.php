<?php

namespace App\Models\QVPDOModel;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;

class Role_DbQuery_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get All Role Data Fetch.
    */
    public static function getAllRoles(){
        return Role::with('permissions')->orderBy('id','DESC')->paginate(5);       
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Get All Permission Data Fetch.
    */
    public static function getAllPermissions(){
        return Permission::get();       
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Get Role by id
    * @param Request $id
    * @return Response Role Array
    */
    public static function getRoleById($id){
        return Role::find($id);      
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * create Role
    * @param Request $request
    * @return true or false
    */
    public static function createRole($request){
        return Role::create(['name' => $request->input('name')]);
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Join permissions with role
    * @param Request $id
    * @return arr
    */
    public static function rolePermissionsJoin($id){
        return Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        ->where("role_has_permissions.role_id",$id)
        ->get();    
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Get All Role has Permissions array
    * @param Request $id
    * @return arr
    */
    public static function getAllRolePermissions($id){
        return DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
        ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
        ->all();    
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Update Role
    * @param Request $request, $role
    * @return true or false
    */
    public static function updateRole($request, $role){
        $role->name = $request->input('name');
        return $role->save(); 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-05-01-2022
    * Delete Role
    * @param Request $id
    * @return true or false
    */
    public static function deleteRoleById($id){
        return DB::table("roles")->where('id',$id)->delete();    
    }
}
