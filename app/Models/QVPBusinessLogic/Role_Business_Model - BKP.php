<?php

namespace App\Models\QVPBusinessLogic;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Role_Business_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Check form validation.
    * @param Request $request
    * @return $validator
    */
    public static function checkRoleValidations($request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]); 

        return $validator; 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Syn permissions
    * @param Request $request, $role
    * @return true or false
    */
    public static function syncPermissions($request, $role){
        return $role->syncPermissions($request->input('permission'));  
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 01-02-2022
    * set Role Permissions
    * @param Request $permissions
    * @return $arr
    */
    public static function getRoleUsers($permissions){
        if($permissions){
            $arr = [];
            foreach($permissions as $key => $permission){
                $arr[] = explode("-", $permission->name);
            }
            $arr2 = [];
            foreach($arr as $key2 => $a){
                $arr2[] = $a[0];
            }
            $pages = array_values(array_unique($arr2));

            $arrY = [];
            foreach($permissions as $keyy => $permission){
                $pageName = explode("-", $permission->name);
                $arrY[$pageName[0]][$permission->id]= $permission->name;
            }
            return $arrY;
        }
    }
}
