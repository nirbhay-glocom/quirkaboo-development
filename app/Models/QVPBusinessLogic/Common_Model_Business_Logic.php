<?php

namespace App\Models\QVPBusinessLogic;
use App\Http\Controllers\Controller;
use DB;

/**
 * class to implement the bussiness logic for updating the alert data
 */
class Common_Model_Business_Logic extends Controller
{
	public function __construct() {
	
	}

	function Check_Cron_Status($Job_URL){

		// if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'wget') === false) {
		// 	return true;
		// }
		$Job_URL=str_replace("index.php/","",$Job_URL);
		
		// $sql="Select Status From ". MASTER_CRON_JOBS ." where Job_URL='".$Job_URL."'";
		$sql="select Status From Master_Cron_Jobs where Job_URL='".$Job_URL."'";
		
		// $query=mysql_query($sql);
		//echo "<pre>";print_r($query->result_array());
		$query =  DB::select($sql);
		 /*applied object to array format */
		 $queryArray = collect($query)->map(function($x){ return (array) $x; })->toArray();
		if(count($queryArray) > 0){
			
			// $results = mysql_fetch_array($query);
			
			$status = $queryArray[0]['Status'];
			
			switch($status){
				
				case 'Active':
					return true;
					break;
					
				case 'Inactive':
					return false;
					break;
			}
		}
		return false;
	}
	
	
	function Get_Current_URL($SERVER){
		
		$URL ="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		return $URL;
		
	}	

}
