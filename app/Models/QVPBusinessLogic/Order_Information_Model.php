<?php

namespace App\Models\QVPBusinessLogic;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Support\Facades\Cookie;
use App\Models\DbModel\User;
use App\Models\DbModel\QVPOrderDetail;
use App\Models\DbModel\QVPOrderProductsDetail;
use App\Models\DbModel\QVPOrderCustomizationDetail;
use App\Models\QVPDOModel\Order_DbQuery_Model;
use App\Core\Traits\ImageUploadTrait;
use Carbon\Carbon;

class Order_Information_Model
{
    use ImageUploadTrait;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * Check image type and validation.
    * @param Request $request
    * @return Response true or false
    */
    public static function checkValidImage($request,$name){
        $validator = Validator::make($request->all(), [
            $name => 'required|mimes:jpeg,jpg,png,mp4,mov,ogg,qt|max:50000',
        ]);

        if ($validator->fails()) {
            return false;
        }else{
            return true; 
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 20-01-2022
    * check and set customized image data.
    * @param Request $request
    * @return Response Array $params
    */
    public static function checkCustomizedImage($request){
        $params = $request->all();
        if ($files = $request->file('Image_URL')) {
            $param['Image_URL'] = ImageUploadTrait::storeFiles('Image_URL','customized-image');
            $param['Order_Id'] = $request->Order_Id;
            $param['Order_Product_Id'] = $request->Order_Product_Id;
            //$param['Customize_Text'] = $request->Customize_Text;
            $param['Notes'] = $request->Notes;
            return $param;
        }
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 11-03-2022
    * check and set customer preview image data.
    * @param Request $request
    * @return Response Array $params
    */
    public static function setCustomerPreviewData($request){
        if(!empty($request->Order_Id)){
            $rowData = QVPOrderDetail::find($request->Order_Id);
            if(!empty($rowData->Preview_Media)){
                $existPreviewMedia = json_decode($rowData->Preview_Media, TRUE);   
                if(!is_null($request->check)){         
                    foreach($existPreviewMedia as $key => $val){

                        //For Checkboxes...
                        if(in_array($val['ID'], $request->check)){
                            $params[] = [
                                'ID' => $val['ID'],
                                'Media_Type' => $val['Media_Type'],
                                'Date' => $val['Date'],
                                'Status' => $val['Status'],
                                'URL' => $val['URL'],
                                'Notes' => $val['Notes'],
                                'Customer_Preview' => 'Yes',
                            ];  
                        }else{
                            $params[] = [
                                'ID' => $val['ID'],
                                'Media_Type' => $val['Media_Type'],
                                'Date' => $val['Date'],
                                'Status' => $val['Status'],
                                'URL' => $val['URL'],
                                'Notes' => $val['Notes'],
                                'Customer_Preview' => 'No',
                            ];  
                        }
                    }
                    $existPreviewMedia = $params;
                }
            }else{
                $existPreviewMedia = [];
            }
            return json_encode(array_merge($existPreviewMedia));
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 28-02-2022
    * check and set preview image data.
    * @param Request $request
    * @return Response Array $params
    */
    public static function setPreviewData($request){
        if(!empty($request->Order_Id)){
            $rowData = QVPOrderDetail::find($request->Order_Id);
            if(!empty($rowData->Preview_Media)){
                $existPreviewMedia = json_decode($rowData->Preview_Media, TRUE);
               
               // if($request->radio != null){              
                    foreach($existPreviewMedia as $key => $val){
                        if($request->radio== $val['ID']){
                            $params[] = [
                                'ID' => $val['ID'],
                                'Media_Type' => $val['Media_Type'],
                                'Date' => $val['Date'],
                                'Status' => 'Approved',
                                'URL' => $val['URL'],
                                'Notes' => $val['Notes'],
                                'Customer_Preview' => $val['Customer_Preview'],
                            ];  
                        }else{
                            $params[] = [
                                'ID' => $val['ID'],
                                'Media_Type' => $val['Media_Type'],
                                'Date' => $val['Date'],
                                'Status' => 'Pending',
                                'URL' => $val['URL'],
                                'Notes' => $val['Notes'],
                                'Customer_Preview' => $val['Customer_Preview'],
                            ];  
                        }
                    }
                    $existPreviewMedia = $params;
               // }
                
            }else{
                $existPreviewMedia = [];
            }
            return json_encode(array_merge($existPreviewMedia));
        }
    }

    public static function setGalleryData($request){
        if(!empty($request->Order_Id)){
            $rowData = QVPOrderDetail::find($request->Order_Id);
            if(!empty($rowData->Gallery)){
                $existPreviewMedia = json_decode($rowData->Gallery, TRUE);
                foreach($existPreviewMedia as $key => $val){

                        if(isset($request->check) && !empty($request->check)){
                            $check = $request->check;
                        }else{
                            $check = explode(",", $request->checkedBoxes);
                        }
                        
                        if(in_array($key, $check)){
                            $params[] = [
                                'ID' => $val['ID'],
                                'Media_Type' => $val['Media_Type'],
                                'Date' => $val['Date'],
                                'Status' => 'Approved',
                                'URL' => $val['URL'],
                                'Notes' => $val['Notes'],
                            ];  
                        }else{
                            $params[] = [
                                'ID' => $val['ID'],
                                'Media_Type' => $val['Media_Type'],
                                'Date' => $val['Date'],
                                'Status' => 'Pending',
                                'URL' => $val['URL'],
                                'Notes' => $val['Notes'],
                            ];  
                        }
                  
                }
                $existPreviewMedia = $params;
            }else{
                $existPreviewMedia = [];
            }
            return json_encode(array_merge($existPreviewMedia));
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 28-02-2022
    * check and set preview image data.
    * @param Request $request
    * @return Response Array $params
    */
    public static function checkPreviewImage($request){        
        
            $rowData = QVPOrderDetail::find($request->Order_Id);
            if(!empty($rowData->Preview_Media)){
                $existPreviewMedia = json_decode($rowData->Preview_Media, TRUE);
            }else{
                $existPreviewMedia = [];
            }

            $file = $request->file('URL');
            $mime = $file->getMimeType($request->file('URL'));
            if(strstr($mime, "video/")){
                $Media_Type = "Video";
            }else if(strstr($mime, "image/")){
                $Media_Type = "Image";
            }
            
            //$date = Carbon::now()->toDateString();
            $dt = Carbon::now();
            $dateGMT = $dt->format("jS M Y h:i:s \G\M\T");

            if(!empty($existPreviewMedia) && sizeof($existPreviewMedia) > 0){
                $rowID = sizeof($existPreviewMedia);
            }else{
                $rowID = 0;
            }

            $params[] = [
                'ID' => $rowID+1,
                'Media_Type' => $Media_Type,
                'Date' => $dateGMT,
                'Status' => 'Pending',
                'URL' => ImageUploadTrait::storeFiles('URL','preview-store'),
                'Notes' => null,
                'Customer_Preview' => 'No',
            ];

            if(!is_null($existPreviewMedia)){
                return json_encode(array_merge($params, $existPreviewMedia));
            }else{	                
                return json_encode(array_merge($params));
            }
        } 

     /*
    * Author: Nirbhay Sharma
    * Date: 28-02-2022
    * check and set gallery image data.
    * @param Request $request
    * @return Response Array $params
    */
    public static function checkGalleryImage($request){ 
       
        if($request->Order_Id){
                $rowData = QVPOrderDetail::find($request->Order_Id);
                if(!empty($rowData->Gallery)){
                    $existGalleryMedia = json_decode($rowData->Gallery, TRUE);
                }else{
                    $existGalleryMedia = [];
                }

                $file = $request->file('URL');
                $mime = $file->getMimeType($request->file('URL'));
                if(strstr($mime, "video/")){
                    $Media_Type = "Video";
                }else if(strstr($mime, "image/")){
                    $Media_Type = "Image";
                }
                
                //$date = Carbon::now()->toDateString();
                $dt = Carbon::now();
                $dateGMT = $dt->format("jS M Y h:i:s \G\M\T");

                if(!empty($existGalleryMedia) && sizeof($existGalleryMedia) > 0){
                    $rowID = sizeof($existGalleryMedia);
                }else{
                    $rowID = 0;
                }

                $params[] = [
                    'ID' => $rowID+1,
                    'Media_Type' => $Media_Type,
                    'Date' => $dateGMT,
                    'Status' => 'Pending',
                    'URL' => ImageUploadTrait::storeFiles('URL','gallery-store'),
                    'Notes' => null
                ];

                if(!is_null($existGalleryMedia)){
                    return json_encode(array_merge($params, $existGalleryMedia));
                }else{	                
                    return json_encode(array_merge($params));
                }
            }
        } 

    public static function setPreviewHtml($Order_Id, $previewArr, $visible){

        if(isset($previewArr) && !empty($previewArr)){
            $output = '';
            foreach($previewArr as $key => $preview){

                if($preview['Status']=='Approved'){
                    $selectBox = "inner-grid img-wraps selectBox previewBox_".$Order_Id.$key;
                    $checked = "checked";
                    $chemrk = "checkmark checko";
                }else{
                    $selectBox = "inner-grid img-wraps previewBox_".$Order_Id.$key;
                    $checked = "";
                    $chemrk = "checkmark";
                }

                if($preview['Notes']==""){
                    $display = 'display:none';
                }else{
                    $display = 'display:block;';
                }

                $download = 'download';

                if($preview['Media_Type']=='Image'){
                    $output  .= '<div class="'.$selectBox.'" id="previewBox_'.$Order_Id.$key.'">';
                    $output  .= '<h6 style="padding:10px 0px 0px 0px;text-align:center;">'.$preview['Date'].'</h6>';
                    //$output  .= '<label class="conta">';
                    //$output  .= '<input type="radio" name="radio" id="'.$key.'" '.$checked.' onchange="checkedPreview(\''.$Order_Id.'\', \''.$key.'\');" value="'.$key.'">';
                    //$output  .= '<span class="'.$chemrk.'"></span>';
                    //$output  .= '</label>';
                    $output  .= '<a class="example-image-link" href="'.$preview['URL'].'" data-lightbox="example-1" style="width: 100% !important;display: block !important;">';
                    $output  .= '<div class="divpic">';
                    $output  .= '<img src="'.$preview['URL'].'"/>';
                    $output  .= '</div>';
                    $output  .= '</a>';
                    $output  .= '<a href="'.$preview['URL'].'" '.$download.' class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">';
                    $output  .= '<i class="bi bi-download"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="form-group" style="'.$visible.'">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openPreNotes" onclick="displayPreviewNote('.$Order_Id.$preview['ID'].')" id="pnote_'.$Order_Id.$preview['ID'].'" data-backdrop="static" data-keyboard="false" style="float: right;">';
                    $output  .= '<i class="bi bi-pencil-fill fs-7"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="col-sm-10" id="divnote_'.$Order_Id.$preview['ID'].'" style="margin-bottom: 10px;margin-top: -7px; '.$display.'" >';
                    $output  .= '<span class="form-control form-control-sm readable" id="pnoteUp_'.$Order_Id.$preview['ID'].'" style="width: 122%;">'.$preview['Notes'].'</span>';
                    $output  .= '</div>';
                    $output  .= '</div><br><br>';
                    $output  .= '</div>';

                    $output  .= '<div class="modal" id="pnoteModel_'.$Order_Id.$preview['ID'].'" class="pnotmod">';
                    $output  .= '<div class="modal-dialog">';
                    $output  .= '<div class="modal-content">';
                    $output  .= '<div class="modal-header">';
                    $output  .= '<h4 class="modal-title" style="color: #fff;">Update Preview Notes:</h4>';
                    $output  .= '<button type="button" class="btn-close-white" onclick="pnotModel('.$Order_Id.$preview['ID'].')">&times;</button>';
                    $output  .= '</div>';
                    $output  .= '<div class="modal-body">';
                    $output  .= '<div class="form-group">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<div class="col-sm-10">';
                    $output  .= '<textarea class="form-control form-control-sm" id="pNotes_'.$Order_Id.$preview['ID'].'" name="Notes" style="width: 117%;"></textarea>';
                   // $output  .= '<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>';
                    $output  .= '</div></div></div>';
                    $output  .= '<div class="modal-footer">';
                    $output  .= '<button type="button" class="btn-sm btn-info" onclick="updatePNotes('.$Order_Id.', '.$preview['ID'].')">Update Notes</button>';
                    $output  .= '</div></div></div></div>';
                   


                }else if($preview['Media_Type']=='Video'){

                    $output  .= '<div class="'.$selectBox.'" id="previewBox_'.$Order_Id.$key.'">';
                    $output  .= '<h6 style="padding: 10px 0px 0px 0px;text-align:center;">'.$preview['Date'].'</h6>';
                   // $output  .= '<label class="conta">';
                    //$output  .= '<input type="radio" name="radio" id="'.$key.'" '.$checked.' onchange="checkedPreview(\''.$Order_Id.'\', \''.$key.'\');" value="'.$key.'">';
                    //$output  .= '<span class="'.$chemrk.'"></span>';
                    //$output  .= '</label>';
                    $output  .= '<a class="example-image-link" href="javascript:void(0);" data-lightbox="example-1" style="width: 100% !important;display: block !important;">';
                    $output  .= '<div class="divpic">';
                    $output  .= '<video width="100%" height="" controls class="preview-vid">';
                    $output  .= '<source class="example-image" src="'.$preview['URL'].'" type="video/mp4">';
                    $output  .= '</video>';
                    $output  .= '</div>';
                    $output  .= '</a>';
                    $output  .= '<div class="form-group" style="'.$visible.'">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openPreNotes" onclick="displayPreviewNote('.$Order_Id.$preview['ID'].')" id="pnote_'.$Order_Id.$preview['ID'].'" data-backdrop="static" data-keyboard="false" style="float: right;">';
                    $output  .= '<i class="bi bi-pencil-fill fs-7"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="col-sm-10" id="divnote_'.$Order_Id.$preview['ID'].'" style="margin-bottom: 10px;margin-top: -7px; '.$display.'" >';
                    $output  .= '<span class="form-control form-control-sm readable" id="pnoteUp_'.$Order_Id.$preview['ID'].'" style="width: 122%;">'.$preview['Notes'].'</span>';
                    $output  .= '</div>';
                    $output  .= '</div><br><br>';
                    $output  .= '</div>';

                    $output  .= '<div class="modal" id="pnoteModel_'.$Order_Id.$preview['ID'].'" class="pnotmod">';
                    $output  .= '<div class="modal-dialog">';
                    $output  .= '<div class="modal-content">';
                    $output  .= '<div class="modal-header">';
                    $output  .= '<h4 class="modal-title" style="color: #fff;">Update Preview Notes:</h4>';
                    $output  .= '<button type="button" class="btn-close-white" onclick="pnotModel('.$Order_Id.$preview['ID'].')">&times;</button>';
                    $output  .= '</div>';
                    $output  .= '<div class="modal-body">';
                    $output  .= '<div class="form-group">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<div class="col-sm-10">';
                    $output  .= '<textarea class="form-control form-control-sm" id="pNotes_'.$Order_Id.$preview['ID'].'" name="Notes" style="width: 117%;"></textarea>';
                   // $output  .= '<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>';
                    $output  .= '</div></div></div>';
                    $output  .= '<div class="modal-footer">';
                    $output  .= '<button type="button" class="btn-sm btn-info" onclick="updatePNotes('.$Order_Id.', '.$preview['ID'].')">Update Notes</button>';
                    $output  .= '</div></div></div></div>';
                }
            }
            
            return $output;
        }
    }

    public static function setGalleryHtml($Order_Id, $galleryArr, $visible){

        if(isset($galleryArr) && !empty($galleryArr)){
            $output = '';
            foreach($galleryArr as $key => $gallery){

                if($gallery['Status']=='Approved'){
                    $selectBox = "inner-grid img-wraps selectCheckBox galleryBox_".$Order_Id.$key;
                    $checked = "checked";                    
                    $chemrk = "checkmarkk checko";
                    $styleOpacity = "opacity: 1";
                }else{
                    $selectBox = "inner-grid img-wraps galleryBox_".$Order_Id.$key;
                    $checked = "";
                    $chemrk = "checkmarkk";
                    $styleOpacity = "opacity: 0.5";
                }

                if($gallery['Notes']==""){
                    $display = 'display:none';
                }else{
                    $display = 'display:block;';
                }
                $download = "download";

                if($gallery['Media_Type']=='Image'){
                    $output  .= '<div class="'.$selectBox.'" id="galleryBox_'.$Order_Id.$key.'">';
                   
                    $output  .= '<h6 style="padding: 10px 0px 0px 0px;text-align:center;">'.$gallery['Date'].'</h6>';
                    $output  .= '<input type="checkbox" class="myinput large" name="check[]" id="grid_'.$Order_Id.$key.'" '.$checked.' onchange="checkedGallery(\''.$Order_Id.'\', \''.$key.'\');" value="'.$key.'" style="'.$visible.'">';
                    $output  .= '<a class="example-image-link" href="'.$gallery['URL'].'" data-lightbox="example-1" style="width: 100% !important;display: block !important;">';
                    
                    $output  .= '<div class="divpic">';
                    $output  .= '<img id="imgGalPicId_'.$Order_Id.$key.'" src="'.$gallery['URL'].'" style="'.$styleOpacity.'" />';
                    $output  .= '</div>';
                    $output  .= '</a>';
                    $output  .= '<a href="'.$gallery['URL'].'" '.$download.' class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">';
                    $output  .= '<i class="bi bi-download"></i>';
                    $output  .= '</a>';
                    
                    $output  .= '<div class="form-group" style="'.$visible.'">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalNotes" onclick="displayGalleryNote('.$Order_Id.$gallery['ID'].')" id="gnote_'.$Order_Id.$gallery['ID'].'" data-backdrop="static" data-keyboard="false" style="float: right;">';
                    $output  .= '<i class="bi bi-pencil-fill fs-7"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="col-sm-10" id="gdivnote_'.$Order_Id.$gallery['ID'].'" style="margin-bottom: 10px;margin-top: -7px; '.$display.'">';
                    $output  .= '<span class="form-control form-control-sm readable" id="gnoteUp_'.$Order_Id.$gallery['ID'].'" style="width: 122%;">'.$gallery['Notes'].'</span>';
                    $output  .= '</div>';
                    $output  .= '</div><br><br>';
                    $output  .= '</div>';

                    $output  .= '<div class="modal" id="gnoteModel_'.$Order_Id.$gallery['ID'].'" class="gnotmod">';
                    $output  .= '<div class="modal-dialog">';
                    $output  .= '<div class="modal-content">';
                    $output  .= '<div class="modal-header">';
                    $output  .= '<h4 class="modal-title" style="color: #fff;">Update Gallery Notes:</h4>';
                    $output  .= '<button type="button" class="btn-close-white" onclick="gnotModel('.$Order_Id.$gallery['ID'].')">&times;</button>';
                    $output  .= '</div>';
                    $output  .= '<div class="modal-body">';
                    $output  .= '<div class="form-group">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<div class="col-sm-10">';
                    $output  .= '<textarea class="form-control form-control-sm" id="gNotes_'.$Order_Id.$gallery['ID'].'" name="Notes" style="width: 117%;"></textarea>';
                   // $output  .= '<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>';
                    $output  .= '</div></div></div>';
                    $output  .= '<div class="modal-footer">';
                    $output  .= '<button type="button" class="btn-sm btn-info" onclick="updateGNotes('.$Order_Id.', '.$gallery['ID'].')">Update Notes</button>';
                    $output  .= '</div></div></div></div>';

                } 
                
                if($gallery['Media_Type']=='Video'){

                    $output  .= '<div class="'.$selectBox.'" id="galleryBox_'.$Order_Id.$key.'">';
                    
                    $output  .= '<h6 style="padding: 10px 0px 0px 0px;text-align:center;">'.$gallery['Date'].'</h6>';
                    $output  .= '<input type="checkbox" class="myinput large" name="check[]" id="grid_'.$Order_Id.$key.'" '.$checked.' onchange="checkedGallery(\''.$Order_Id.'\', \''.$key.'\');" value="'.$key.'" style="'.$visible.'">';
                    $output  .= '<a class="example-image-link" href="javascript:void(0);" data-lightbox="example-1" style="width: 100% !important;display: block !important;">';
                    $output  .= '<div class="divpic" style="padding-bottom: 10px;">';
                    $output  .= '<video id="imgGalPicId_'.$Order_Id.$key.'" width="100%" height="" controls class="gallery-vid" style="'.$styleOpacity.'">';
                    $output  .= '<source class="example-image" src="'.$gallery['URL'].'" type="video/mp4">';
                    $output  .= '</video>';
                    $output  .= '</div>';
                    $output  .= '</a>';
                    $output  .= '<div class="form-group" style="'.$visible.'">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalNotes" onclick="displayGalleryNote('.$Order_Id.$gallery['ID'].')" id="gnote_'.$Order_Id.$gallery['ID'].'" data-backdrop="static" data-keyboard="false" style="float: right;">';
                    $output  .= '<i class="bi bi-pencil-fill fs-7"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="col-sm-10" id="gdivnote_'.$Order_Id.$gallery['ID'].'" style="margin-bottom: 10px;margin-top: -7px; '.$display.'">';
                    $output  .= '<span class="form-control form-control-sm readable" id="gnoteUp_'.$Order_Id.$gallery['ID'].'" style="width: 122%;">'.$gallery['Notes'].'</span>';
                    $output  .= '</div>';
                    $output  .= '</div><br><br>';
                    $output  .= '</div>';
                    

                    $output  .= '<div class="modal" id="gnoteModel_'.$Order_Id.$gallery['ID'].'" class="gnotmod">';
                    $output  .= '<div class="modal-dialog">';
                    $output  .= '<div class="modal-content">';
                    $output  .= '<div class="modal-header">';
                    $output  .= '<h4 class="modal-title" style="color: #fff;">Update Gallery Notes:</h4>';
                    $output  .= '<button type="button" class="btn-close-white" onclick="gnotModel('.$Order_Id.$gallery['ID'].')">&times;</button>';
                    $output  .= '</div>';
                    $output  .= '<div class="modal-body">';
                    $output  .= '<div class="form-group">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<div class="col-sm-10">';
                    $output  .= '<textarea class="form-control form-control-sm" id="gNotes_'.$Order_Id.$gallery['ID'].'" name="Notes" style="width: 117%;"></textarea>';
                   // $output  .= '<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>';
                    $output  .= '</div></div></div>';
                    $output  .= '<div class="modal-footer">';
                    $output  .= '<button type="button" class="btn-sm btn-info" onclick="updateGNotes('.$Order_Id.', '.$gallery['ID'].')">Update Notes</button>';
                    $output  .= '</div></div></div></div>';
                }
            }
            
            return $output;
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 12-01-2022
    * Customized image unset from folder while delting image row.
    * @param Request $request
    * @return Response true or false
    */
    public static function unsetImage($customizedRowData){
        $result = 0;
        if(ImageUploadTrait::checkImageExitOrNot('Image_URL', 'qvp_order_customization_details', $customizedRowData->ID)){
           $result = ImageUploadTrait::deletedExistingImage('Image_URL', 'qvp_order_customization_details', $customizedRowData->ID);
        }
        return $result; 
    }


    /*
    * Author: Nirbhay Sharma
    * Date: 13-01-2022
    * Get Order Data and set and array format.
    * @param Request $orderData
    * @return Response $data as Array
    */
    public static function getOrderArr($orderData){
        if($orderData){
            $data['Vendor'] = $orderData->Vendor;
            $data['Order_Date'] = $orderData->Order_Date;
            $data['Vendor_Estimate'] = $orderData->Vendor_Estimate;
            $data['Customer_Notes'] = $orderData->Customer_Notes;
            $data['Vendor_Notes'] = $orderData->Vendor_Notes;
            $data['Glocom_Notes'] = $orderData->Glocom_Notes;
            $data['Product_Type'] = $orderData->products[0]->Product_Type;
            $data['Product_Name'] = $orderData->products[0]->Product_Name;
            $data['Variation_1'] = $orderData->products[0]->Variation_1;
            $data['Variation_2'] = $orderData->products[0]->Variation_2;

            return $data;
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 25-01-2022
    * set comments data into json array.
    * @param Request $orderData
    * @return Response $data as Array
    */
    public static function setArrInJSON($request){
        if($request){
            $rowData = QVPOrderCustomizationDetail::findOrFail($request->ID);
            if(!empty($rowData->Comments)){
                $existComments = json_decode($rowData->Comments, TRUE);
            }else{
                $existComments = [];
            }
           
            $dt = Carbon::now();
            $dateGMT = $dt->format("jS M Y h:i:s \G\M\T");

            $params[] = [
                'Date' => $dateGMT,
                'Commented_By' => $request->Commented_By,
                'User_Id' => $request->User_Id,
                'Comments' => $request->Comments,
                
            ];
            return json_encode(array_merge($params,$existComments));
        }
    }

     /*
    * Author: Nirbhay Sharma
    * Date: 28-01-2022
    * set comments data array.
    * @param Request $request, $role
    * @return Response $data as Array
    */
    public static function setCommentDataArr($rowID, $request, $user, $comment){
        $role = $user->roles->pluck('name')['0'];
        $rowData = QVPOrderCustomizationDetail::findOrFail($rowID);
            if(!empty($rowData->Comments)){
                $existComments = json_decode($rowData->Comments, TRUE);
            }else{
                $existComments = [];
            }
           
            $dt = Carbon::now();
            $dateGMT = $dt->format("jS M Y h:i:s \G\M\T");

            $params[] = [
                'Date' => $dateGMT,
                'Commented_By' => $role,
                'User_Id' => $request->UserId,
                'Comments' => $role.$comment,
            ];
            return json_encode(array_merge($params,$existComments));
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 11-03-2022
    * set preview notes data array.
    * @param Request $request
    * @return Response $data as Array
    */
    public static function setPreviewNotesArr($request){
        if(!empty($request->Order_Id) && !empty($request->ID)){
            $orderDetilData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
            if(!empty($orderDetilData->Preview_Media)){
                $existPreviewArr = json_decode($orderDetilData->Preview_Media, TRUE);
                if(isset($existPreviewArr) && count($existPreviewArr) > 0){
                    foreach($existPreviewArr as $key => $preview){
                        if($preview['ID'] == $request->ID){
                            $existPreviewArr[$key]['Notes'] = $request->Notes;
                        }
                    }
                }

            }else{
                $existPreviewArr = [];
            }

            return $existPreviewArr;

        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 11-03-2022
    * set gallery data array.
    * @param Request $request
    * @return Response $data as Array
    */
    public static function setGalleryNotesArr($request){
        if(!empty($request->Order_Id) && !empty($request->ID)){
            $orderDetilData = Order_DbQuery_Model::getOrderDetail($request->Order_Id);
            if(!empty($orderDetilData->Gallery)){
                $existGalleryArr = json_decode($orderDetilData->Gallery, TRUE);
                if(isset($existGalleryArr) && count($existGalleryArr) > 0){
                    foreach($existGalleryArr as $key => $gallery){
                        if($gallery['ID'] == $request->ID){
                            $existGalleryArr[$key]['Notes'] = $request->Notes;
                        }
                    }
                }

            }else{
                $existGalleryArr = [];
            }

            return $existGalleryArr;

        }
    }
    

     /*
    * Author: Nirbhay Sharma
    * Date: 28-01-2022
    * manage comment text.
    * @param $user, $method
    * @return $comment
    */
    public static function manageCommentContent($method, $user){
        $comment = ''; 
        if($method=='uploadImg'){
            $comment = ' ('.$user->first_name.' '.$user->last_name.") Uploade the image for customization.";
        }else if($method=='UpdateImgContent'){
            $comment = ' ('.$user->first_name.' '.$user->last_name.") Update the customized image text and notes.";
        }else if($method=='Approved'){
            $comment = ' ('.$user->first_name.' '.$user->last_name.") Approved the Image.";
        }
        return $comment;
    }

    public static function setCustomerPreviewHtml($Order_Id, $previewArr, $visible){
        if(isset($previewArr) && !empty($previewArr)){
            $output = '';
            foreach($previewArr as $key => $preview){

                if($preview['Customer_Preview']=='Yes' && $preview['Status']=='Approved'){
                    $selectBox = "inner-grid img-wraps selectCheckBox selectBox gPreviewBox_".$Order_Id.$key;
                    $checked = "checked";
                    $radioChecked = "checked"; 
                    $styleOpacity = "opacity: 1";
                    //$chemrk = "<span class='checkmark checko'></span>";                   
                }else if($preview['Customer_Preview']=='Yes' && $preview['Status'] !='Approved'){
                    $selectBox = "inner-grid img-wraps selectCheckBox gPreviewBox_".$Order_Id.$key;
                    $checked = "checked";
                    $radioChecked = ""; 
                    $styleOpacity = "opacity: 1";
                    //$chemrk = "<span class='checkmark' style='background-color:#ccc;'></span>";
                }else if($preview['Status']=='Approved' && $preview['Customer_Preview']!='Yes'){
                    $selectBox = "inner-grid img-wraps selectBox gPreviewBox_".$Order_Id.$key;
                    $checked = "";
                    $radioChecked = "checked";
                    $styleOpacity = "opacity: 1"; 
                    //$chemrk = "<span class='checkmark checko'></span>";
                }else{
                    $selectBox = "inner-grid img-wraps gPreviewBox_".$Order_Id.$key;
                    $checked = ""; 
                    $radioChecked = ""; 
                    $styleOpacity = "opacity: 0.5;";
                    //$chemrk = "<span class='checkmark' style='background-color:#ccc;'></span>";               
                }

                $download = 'download';
                $checkbox = 'Checkbox';
                $radiobutton = 'Radio';

                if($preview['Notes']==""){
                    $display = 'display:none';
                }else{
                    $display = 'display:block';
                }

                $dd = explode(":", $visible);
                if($dd[1]=="none"){
                    $setCss = 'margin: 10px 0px 0px 0px;text-align:center';
                    $setVideoCss = 'margin: 10px 0px 0px 0px;text-align:center';
                    $setRado = 'margin: -4px 5px 0px -72px !important';
                }else{
                    $setCss = 'margin: -10px 0px 0px 0px;text-align:center';
                    $setRado = 'margin: -4px 5px 0px -260px !important';
                    $setVideoCss = 'margin: 10px 0px 0px 0px;text-align:center';
                }

                if($preview['Media_Type']=='Image'){ 
                    $output  .= '<div class="'.$selectBox.'" id="gPreviewBox_'.$Order_Id.$key.'">';

                    $output  .= '<input type="checkbox" class="galPreview" '.$checked.' name="check[]" id="galPreGrid_'.$Order_Id.$key.'" onchange="checkedGalPreview(\''.$checkbox.'\', \''.$Order_Id.'\', \''.$key.'\', \''.$preview['ID'].'\');" value="'.$preview['ID'].'" title="Customer Preview" style="'.$visible.'">';
                    
                    $output  .= '<label class="btn btn-default" style="'.$visible.'">';
                    $output  .= '<input class="custome-radio" type="radio" name="radio" id="galPreRadio_'.$Order_Id.$key.'" '.$radioChecked.' onclick="checkedGalPreview(\''.$radiobutton.'\', \''.$Order_Id.'\', \''.$key.'\', \''.$preview['ID'].'\');" value="'.$preview['ID'].'" title="Approve">';
                    //$output  .= $chemrk;
                    $output  .= '</label>';

                    $output  .= '<h6 style="'.$setCss.'">'.$preview['Date'].'</h6>';

                    $output  .= '<a class="example-image-link" href="'.$preview['URL'].'" data-lightbox="example-1" style="width: 100% !important;display: block !important;">';
                    $output  .= '<div class="divpic">';
                    $output  .= '<img id="imgPicId_'.$Order_Id.$key.'" src="'.$preview['URL'].'" style="'.$styleOpacity.'"/>';
                    $output  .= '</div>';
                    $output  .= '</a>';

                    $output  .= '<a href="'.$preview['URL'].'" '.$download.' class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">';
                    $output  .= '<i class="bi bi-download"></i>';
                    $output  .= '</a>';

                    $output  .= '<div class="form-group" style="'.$visible.'">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalPreNotes" onclick="displayGalPreviewNote('.$Order_Id.$preview['ID'].')" id="gpnote_'.$Order_Id.$preview['ID'].'" data-backdrop="static" data-keyboard="false" style="float: right;">';
                    $output  .= '<i class="bi bi-pencil-fill fs-7"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="col-sm-10" id="gpdivnote_'.$Order_Id.$preview['ID'].'" style="margin-bottom: 10px;margin-top: -7px; '.$display.'">';
                    $output  .= '<span class="form-control form-control-sm readable" id="gpnoteUp_'.$Order_Id.$preview['ID'].'" style="width: 122%;">'.$preview['Notes'].'</span>';
                    $output  .= '</div>';
                    $output  .= '</div><br><br>';
                    $output  .= '</div>';

                    $output  .= '<div class="modal" id="gpnoteModel_'.$Order_Id.$preview['ID'].'" class="gpnotmod">';
                    $output  .= '<div class="modal-dialog">';
                    $output  .= '<div class="modal-content">';
                    $output  .= '<div class="modal-header">';
                    $output  .= '<h4 class="modal-title" style="color: #fff;">Update Customer Preview Notes:</h4>';
                    $output  .= '<button type="button" class="btn-close-white" onclick="gpnotModel('.$Order_Id.$preview['ID'].')">&times;</button>';
                    $output  .= '</div>';
                    $output  .= '<div class="modal-body">';
                    $output  .= '<div class="form-group">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<div class="col-sm-10">';
                    $output  .= '<textarea class="form-control form-control-sm" id="gpNotes_'.$Order_Id.$preview['ID'].'" name="Notes" style="width: 117%;"></textarea>';
                   // $output  .= '<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>';
                    $output  .= '</div></div></div>';
                    $output  .= '<div class="modal-footer">';
                    $output  .= '<button type="button" class="btn-sm btn-info" onclick="updateGPNotes('.$Order_Id.', '.$preview['ID'].')">Update Notes</button>';
                    $output  .= '</div></div></div></div>';
                   


                }else if($preview['Media_Type']=='Video'){

                    $output  .= '<div class="'.$selectBox.'" id="gPreviewBox_'.$Order_Id.$key.'">';

                    $output  .= '<input type="checkbox" class="galPreview" '.$checked.' name="check[]" id="galPreGrid_'.$Order_Id.$key.'" onchange="checkedGalPreview(\''.$checkbox.'\', \''.$Order_Id.'\', \''.$key.'\', \''.$preview['ID'].'\');" value="'.$preview['ID'].'" title="Customer Preview" style="'.$visible.'">';

                    $output  .= '<h6 style="'.$setVideoCss.'">'.$preview['Date'].'</h6>';

                    // $output  .= '<label class="btn btn-default">';
                    // $output  .= '<input class="custome-radio" type="radio" name="radio" id="galPreRadio_'.$Order_Id.$key.'" '.$radioChecked.' onclick="checkedGalPreview(\''.$radiobutton.'\', \''.$Order_Id.'\', \''.$key.'\', \''.$preview['ID'].'\');" value="'.$preview['ID'].'" title="Approve">';                  
                    // $output  .= '</label>';

                    $output  .= '<a class="example-image-link" href="javascript:void(0);" data-lightbox="example-1" style="width: 100% !important;display: block !important;">';
                    $output  .= '<div class="divpic" style="padding-bottom: 10px;">';
                    $output  .= '<video width="100%" height="" controls class="preview-vid" id="imgPicId_'.$Order_Id.$key.'" style="'.$styleOpacity.'">';
                    $output  .= '<source class="example-image" src="'.$preview['URL'].'" type="video/mp4">';
                    $output  .= '</video>';
                    $output  .= '</div>';
                    $output  .= '</a>';
                    $output  .= '<div class="form-group" style="'.$visible.'">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalPreNotes" onclick="displayGalPreviewNote('.$Order_Id.$preview['ID'].')" id="gpnote_'.$Order_Id.$preview['ID'].'" data-backdrop="static" data-keyboard="false" style="float: right;">';
                    $output  .= '<i class="bi bi-pencil-fill fs-7"></i>';
                    $output  .= '</a>';
                    $output  .= '<div class="col-sm-10" id="gpdivnote_'.$Order_Id.$preview['ID'].'" style="margin-bottom: 10px;margin-top: -7px; '.$display.'">';
                    $output  .= '<span class="form-control form-control-sm readable" id="gpnoteUp_'.$Order_Id.$preview['ID'].'" style="width: 122%;">'.$preview['Notes'].'</span>';
                    $output  .= '</div>';
                    $output  .= '</div><br><br>';

                    $output  .= '</div>';

                    $output  .= '<div class="modal" id="gpnoteModel_'.$Order_Id.$preview['ID'].'" class="gpnotmod">';
                    $output  .= '<div class="modal-dialog">';
                    $output  .= '<div class="modal-content">';
                    $output  .= '<div class="modal-header">';
                    $output  .= '<h4 class="modal-title" style="color: #fff;">Update Customer Preview Notes:</h4>';
                    $output  .= '<button type="button" class="btn-close-white" onclick="gpnotModel('.$Order_Id.$preview['ID'].')">&times;</button>';
                    $output  .= '</div>';
                    $output  .= '<div class="modal-body">';
                    $output  .= '<div class="form-group">';
                    $output  .= '<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>';
                    $output  .= '<div class="col-sm-10">';
                    $output  .= '<textarea class="form-control form-control-sm" id="gpNotes_'.$Order_Id.$preview['ID'].'" name="Notes" style="width: 117%;"></textarea>';
                   // $output  .= '<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>';
                    $output  .= '</div></div></div>';
                    $output  .= '<div class="modal-footer">';
                    $output  .= '<button type="button" class="btn-sm btn-info" onclick="updateGPNotes('.$Order_Id.', '.$preview['ID'].')">Update Notes</button>';
                    $output  .= '</div></div></div></div>';
                }
            }
            
            return $output;
        }
    }

    public static function setGridContent($OrderID, $orders){
        $output = '';
                                  
        foreach($orders as $order){
            if($order->ID==$OrderID){
                $output = '<div id="extraLargeModal_'.$order->ID.'" class="modal fade" tabindex="-1" role="dialog">@include("pages.orders.includes.allModal")</div>';
              
            }
        }
      
        return $output;
        

    }
    
}
