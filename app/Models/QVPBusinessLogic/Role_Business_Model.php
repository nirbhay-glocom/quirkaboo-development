<?php

namespace App\Models\QVPBusinessLogic;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Role_Business_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Check form validation.
    * @param Request $request
    * @return $validator
    */
    public static function checkRoleValidations($request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'user_group_id' => 'required',
        ]); 

        return $validator; 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 07-03-2022
    * Check form validation.
    * @param Request $request
    * @return $validator
    */
    public static function checkRoleUpdtValidations($request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'user_group_id' => 'required',
        ]); 

        return $validator; 
    }

    public static function setPagePermissionArr($request){
        if(isset($request->PageVisibility) && !empty($request->PageVisibility) && !empty($request->Role_Id)){
            $paramsPageArray = [];
            foreach($request->PageVisibility as $permission_Id => $value){ 
                if($request->pageExists==0){                    
                    $paramsPageArray[] = [
                        'Role_Id' => $request->Role_Id,
                        'Permission_Id' => $permission_Id,
                        'Visibility' => $value,
                        'View' => 0,
                        'View_And_Edit' => 0
                    ];
                }
            }
            return $paramsPageArray;
        }
    }

    public static function createSlug($str, $delimiter = '_'){

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Syn permissions
    * @param Request $request, $role
    * @return true or false
    */
    public static function syncPermissions($request, $role){
        return $role->syncPermissions($request->input('permission'));  
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 01-02-2022
    * set Role Permissions
    * @param Request $permissions
    * @return $arr
    */
    public static function getRoleUsers($permissions){
        if($permissions){
            $arr = [];
            foreach($permissions as $key => $permission){
                $arr[] = explode("-", $permission->name);
            }
            $arr2 = [];
            foreach($arr as $key2 => $a){
                $arr2[] = $a[0];
            }
            $pages = array_values(array_unique($arr2));

            $arrY = [];
            foreach($permissions as $keyy => $permission){
                $pageName = explode("-", $permission->name);
                $arrY[$pageName[0]][$permission->id]= $permission->name;
            }
            return $arrY;
        }
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 06-03-2022
    * set Role Permissions Array
    * @param Request $roles_permissions, $allPermissions, $role_id
    * @return $arr
    */
    public static function setArray($roles_permissions, $allPermissions, $role_id){
        $setPermissionArray = [];
        foreach($allPermissions as $key2 => $permArr2){

           if (count($roles_permissions) > 0){
                foreach($roles_permissions as $key1 => $permArr1){           

                    if($permArr1->Permission_Id==$permArr2->ID){

                       $setPermissionArray[] = [
                            'ID' => $permArr2->ID,
                            'Role_Permission_RowId' => $permArr1->ID,
                            'Role_Id' => $permArr1->Role_Id,
                            'Permission_Id' => $permArr1->Permission_Id,
                            'Name' => $permArr2->Name,
                            'Type' => $permArr2->Type,
                            'Parent_Id' => $permArr2->Parent_Id,
                            'Visibility' => $permArr1->Visibility,
                            'View' => $permArr1->View,
                            'View_And_Edit' => $permArr1->View_And_Edit
                        ];

                    }
                }
            }else{
                    $setPermissionArray[] = [
                        'ID' => $permArr2->ID,
                        'Role_Permission_RowId' => 0,
                        'Role_Id' => $role_id,
                        'Permission_Id' => 0,
                        'Name' => $permArr2->Name,
                        'Type' => $permArr2->Type,
                        'Parent_Id' => $permArr2->Parent_Id,
                        'Visibility' => 0,
                        'View' => 0,
                        'View_And_Edit' => 0
                    ];    
            }
        }
      
        return $setPermissionArray;
    }

    public static function addPermissionsIntoArr($roles, $insertArr, $fieldName){
        if(isset($roles) && !empty($roles)){
            foreach($roles as $key => $role){
                if($role->id){
                    $roles[$key][$fieldName] = $insertArr;
                }
            }
            return $roles;
        }
    }

    //Recursive get permissions function
    public static function buildTree($items) {
        if($items){
             $childs = array();
             foreach($items as &$item){
                 $childs[$item['Parent_Id']][] = &$item;
                 unset($item);
             }
             foreach($items as &$item){
                 if (isset($childs[$item['ID']])){
                     $item['childs'] = $childs[$item['ID']];
                 }
             }
 
             return $childs[2];
        }        
     } 
}
