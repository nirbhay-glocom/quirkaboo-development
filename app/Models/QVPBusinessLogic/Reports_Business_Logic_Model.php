<?php

namespace App\Models\QVPBusinessLogic;
use App\Http\Controllers\Controller;
use App\Models\QVPDOModel\Reports_Do_Model;
use Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportReports;

/**
 * class to implement the bussiness logic for updating the alert data
 */
class Reports_Business_Logic_Model extends Controller
{
	public function __construct(){
		$this->Reports_Do_Model = new Reports_Do_Model();
	}

	public function sendTestMail(){
		$message                   	= "Send mail for customer preview ..!";
		$template_id 		= '1';
		$fileName = '';
		return $this->Email_Queue($fileName, $template_id, "");
	}

	public function getMissingChannelConfiguration() {

		$message                     	      = "Missing Channel Configuration not found ..!";

		$getMissingChannelConfigDetails    	  = array();
		$getMissingChannelConfigDetails  	  = $this->Get_Missing_Channel_Config_From_Product_Master_Accounts_Combo();

		if(!empty($getMissingChannelConfigDetails))
		{
			$message 		= "Missing Channel Configuration Found";
			
			$report_link 	= config('constants.CHANNEL_CONFIG_REPORT_LINK');

			$template_id 	= '5';
			$this->Email_Queue( $report_link, $template_id, "");
		}
	} //getMissingChannelConfiguration


	/* START COMMON FUNCTIONS */

	public function Email_Queue($fileName, $template_id, $report_array=false, $dynamic_content=false, $default_content_template = false) {
        $date = date('Y-m-d');

		$content = array('date' => $date);
		$subject = ["date"=> $date];
		$dynamic_recepient = [];
		$attachment = [];
		$this->Push_To_Email_Queue($template_id, $content, $subject, $dynamic_recepient, $attachment,$fileName, $report_array, $dynamic_content, $default_content_template);
		return ($content) ? 'Emails Queued Successfully ' : 'No Data Found';
	
    }

    public function Push_To_Email_Queue($email_campaign_id=false,$dynamic_tags=[],$dynamic_subject=[],$dynamic_recepients=[],$file_attachments=[],$fileName=[], $report_array=[], $dynamic_content=[], $default_content_template = [])
	{
		$email_campaign_data=$this->Reports_Do_Model->Get_Email_Campaign_Details($email_campaign_id); 

		//echo $this->db->last_query();
		//echo "<pre>";print_r($email_campaign_data);
		$tags = explode(",",$email_campaign_data["Tags"]);
		// $this->Show($email_campaign_data); 
		if($email_campaign_data["err"] == "Success")
		{
			$subjectarray=array();
			$tags_data=array();
			foreach($tags as $key=>$value)
			{
				if(isset($dynamic_tags[$value]))
				$tags_data[$value]=$dynamic_tags[$value];
				
				if(isset($dynamic_subject[$value]))
				$subjectarray[$value]=$dynamic_subject[$value];
			}
				 
			$subject=$this->Reports_Do_Model->Get_Subject($email_campaign_id,$subjectarray);
			$subject=$subject["subject"];

			$from=$email_campaign_data["From_Email"];
			$to=$email_campaign_data["To_Email"];

			if(!empty($report_array)) {
				$to=implode(",", array_column($report_array, 'Email_ID'));	
			}
			else{
				if(count($dynamic_recepients)>0)
				{
					if($to!="")
					$to.=",".implode(",",$dynamic_recepients);
					else
					$to=implode(",",$dynamic_recepients);	
				}
			}
			
			$cc=$email_campaign_data["CC"];			
			$reply_to=$email_campaign_data["Reply_To"];
			// $bcc=$email_campaign_data["BCC"];
			$template_id=$email_campaign_data["Template_ID"];
			
			$attachment_url=!empty($email_campaign_data["Attachment_Url"])?$email_campaign_data["Attachment_Url"]:'';

			if(count($file_attachments)>0)
			{
				$extra_attachment=implode(",",$file_attachments);
				if($attachment_url!="")
				$attachment_url.=",".$extra_attachment;
				else
				$attachment_url=$extra_attachment;
			}
			
			//If file is empty....
			if(empty($fileName)){
			  $content=$this->Reports_Do_Model->Get_Email_Content_From_Template($template_id, $tags_data);
			  $content=$content["content"];

			}

		   //If file is not empty....
		   #### Getting actual template content by passing formed data arranged by tags passed
		//    if($default_content_template == 'true') {
		// 		$content=$dynamic_content;
		//    	} else {
		// 		$content=$this->Reports_Do_Model->Get_Email_Content_From_Template($template_id, $tags_data); 
				
		// 		if($template_id == 5) {
		// 			$content=$content["content"].'<button style="color: #007bff; background-color: transparent; border: transparent; cursor: pointer;"><a href='.$fileName.' target="_blank" >Enter New Channel Configuration</a></button>';
		// 		}
		// 		elseif($template_id == 6){
		// 			$dataValues = '';
		// 			if(!is_array($fileName)) {
		// 				$convertedFileName = json_decode($fileName, true);
		// 			}else{
		// 				$convertedFileName = $fileName;
		// 			}
		// 			foreach ($convertedFileName as $key => $value) {
		// 				$dataValues .='<button style="color: #007bff; background-color: transparent; border: transparent; cursor: pointer;"><a href='.$value.' target="_blank" >'.$key.'</a></button><br/>';
						
		// 			}
		// 			$content=$content["content"].$dataValues;

		// 		}elseif($template_id == 14) {

		// 			$fileName = config('constants.USER_DAILY_REPORT');
		// 			$send_at  = date("Y-m-d");
		// 			$content = $content["content"].'Please<button style="color: #007bff; background-color: transparent; border: transparent; cursor: pointer;"><a href='.$fileName.' target="_blank" >Click here</a></button>to submit report. If it is your planned leave or you are not required to submit report for ('.$send_at.') please ignore this alert.';
		// 		}	
		// 		else{
		// 			$content=$content["content"].'<button style="color: #007bff; background-color: transparent; border: transparent; cursor: pointer;"><a href='.$fileName.' target="_blank" >Click here</a></button>';
		// 		}
		//    }

			$priority = 'Immediate';
			$status = 'Ready';
			$send_at = date("Y-m-d H:i:s");
			$notification = true;

			### pushing to queue with status 'Immediate'
			$initiatedResult=$this->Reports_Do_Model->Initiate_Email_Queue($subject,$from,$to,$reply_to,$cc,htmlentities($content),$priority,$status,$send_at, $attachment_url, $notification);
			
			if($initiatedResult!="Success")
			{			
				$json_resp["result"]="Error";
				$json_resp["msg"]=$initiatedResult;	
			}
			else
			{
				$json_resp["result"]="Success";
			}

		}else
		{
			$json_resp["result"]="Error";
			$json_resp["msg"]="Getting Campaign Details Error";
		}

		return $json_resp;
	} //Push_To_Email_Queue

	public function CommonArrayToCSV($InputArray, $InputRootPath, $InputRequestPath)
    {   
        $dateTime = date('Y-m-d-h:i:s'); 
		$RootPath = config('constants.'.$InputRootPath).$dateTime.'.csv';

		// //getting Root Path
		$RootPathFileName = Request::root().config('constants.'.$InputRequestPath).$dateTime.'.csv';

        $title = array_keys($InputArray[0]);
		array_unshift($InputArray, $title);

		Excel::store(new ExportReports([$InputArray]), $RootPath);

        return $RootPathFileName;
    } //CommonArrayToCSV

	/* END COMMON FUNCTIONS */

	/* START REPORTS LOGIC */

	public function Get_Missing_Price_Stock() {

		/* start of issue 1 logic */
		$sumOfStock = 'Awaiting_Dispatch_Asia + Awaiting_Dispatch_CD + Awaiting_Dispatch_FBA_IN + Awaiting_Dispatch_IN + Awaiting_Dispatch_UK + Awaiting_Dispatch_US + Dispatched_Asia + Dispatched_CD + Dispatched_FBA_IN + Dispatched_IN + Dispatched_UK + Dispatched_US + Removed_Asia + Removed_CD + Removed_FBA_IN + Removed_IN + Removed_UK + Removed_US + Reserved_Asia + Reserved_CD + Reserved_FBA_IN + Reserved_IN + Reserved_UK + Reserved_US + Transfer_Asia + Transfer_CD + Transfer_FBA_IN + Transfer_IN + Transfer_UK + Transfer_US';
		$sumOfReturnedStock = 'Returned_Quantity_Asia + Returned_Quantity_CD + Returned_Quantity_IN + Returned_Quantity_UK + Returned_Quantity_US + Returned_Quantity_FBA_IN';
		
		$messageArrray = array();
		
		$issueOneData = $this->Reports_Do_Model->Get_Total_Stock_Unmatch_Data($sumOfStock, $sumOfReturnedStock);

		

		if(count($issueOneData) > 0) {

			for($i=0; $i<count($issueOneData); $i++) {
				$messageArrray[] = array(
					'Variant_SKU' => $issueOneData[$i]['Variant_SKU'],
					'Issue_Identified' => 'Received quantity do not match with operational stock',
					'Location' => ''
				);
			}
		}
		/* end of issue 1 logic */
		
		/* start of issue 2 logic */
		// $sumOfWarehouseStock = '(Warehouse_Stock_CD + Awaiting_Dispatch_CD) + (Warehouse_Stock_IN + Awaiting_Dispatch_IN) + (Warehouse_Stock_UK + Awaiting_Dispatch_UK) + (Warehouse_Stock_US + Awaiting_Dispatch_US) + (Warehouse_Stock_Asia + Awaiting_Dispatch_Asia) + (FBA_Stock_IN + FBA_Stock_IN)';

		$issueTwoData = $this->Reports_Do_Model->Get_Total_Stock_Unmatch_Data_Issue_Two();
		
		if(count($issueTwoData) > 0) {

			for($i=0; $i<count($issueTwoData); $i++) {
				$messageArrray[] = array(
					'Variant_SKU' => $issueTwoData[$i]['Variant_SKU'],
					'Issue_Identified' => 'Total Sale quantity do not match with warehouse stock',
					'Location' => ''
				);
			}
		}
		/* end of issue 2 logic */

		/* start of issue 3 logic */
		$issueThreeDataPriceStock =  $this->Reports_Do_Model->Get_Total_Price_Stock_Issue_Three();
		
		$issueThreeDataStorage = $this->Reports_Do_Model->Get_Total_Storage_Issue_Three();
		
		foreach($issueThreeDataPriceStock as $key => $value) {
			
			if(!in_array($value, $issueThreeDataStorage)) {
				/* 
					checking value found in Storage array, if exists means trying to get the Index key on Storage Array.
					if Index key not found on Storage Array means it will get all the value and store in final MessageArray,
					if Index key founded means it will check the difference of that two array and store the unmatch value only.
				*/
				$indexOfInArray = array_search($value['Variant_SKU'], array_column($issueThreeDataStorage, 'Variant_SKU'));
				
				if($indexOfInArray !== false) {
					// $fullDiff=array_diff_assoc($value,$issueThreeDataStorage[$indexOfInArray]);
					$fullDiff = array_merge(array_diff($value, $issueThreeDataStorage[$indexOfInArray]), array_diff($issueThreeDataStorage[$indexOfInArray], $value));

					foreach($fullDiff as $key1 => $value1) {
						$fetchedLocation = strpos($key1, 'Warehouse_Stock_') !== false ? str_replace("Warehouse_Stock_", "",$key1) : $key1 ;
						$messageArrray[] = array(
							'Variant_SKU' => $value['Variant_SKU'],
							'Issue_Identified' => 'Warehouse stock do not match with storage count',
							'Location' => $fetchedLocation
						);
					}
				}

			}
		}
		if(count($messageArrray) > 0) {
			return $messageArrray;
			// echo json_encode( $messageArrray );
		}
		/* end of issue 3 logic */
	} //Get_Missing_Price_Stock

	public function Get_Missing_Channel_Config_From_Product_Master_Accounts_Combo() {

		$getRetailProducts		 		   	  = $this->Reports_Do_Model->Get_Retail_Products();
			
		$getMasterAccountsRetail		   	  = $this->Reports_Do_Model->Get_Master_Accounts_Retail();
			
		$missingChannelConfigComboArray = array();

		if(!empty($getRetailProducts) && !empty($getMasterAccountsRetail)) {
			foreach($getMasterAccountsRetail as  $masterAccountValue) {
				$salesChannel = $masterAccountValue->Sales_Channel;
				$storeName 	  = $masterAccountValue->Store_Name;

				foreach($getRetailProducts as $retailProductsValue ) {
					$department = $retailProductsValue->Department;
					$vendorCode = $retailProductsValue->Vendor_Code;

					$getMisssingChannelConfig = $this->Reports_Do_Model->Get_Missing_Channel_Configuration($department, $vendorCode, $salesChannel, $storeName);

					if(count($getMisssingChannelConfig) == 0){	
						$missingChannelConfigComboArray[] = array('Department' => $department, 
																  'Vendor_Code' => $vendorCode, 
																  'Sales_Channel' => $salesChannel, 
																  'Store_Name' => $storeName
																);
					}
				}
			}
		}
		return $missingChannelConfigComboArray;
	} //Get_Missing_Channel_Config_From_Product_Master_Accounts_Combo

	public function Get_Missing_Catalogue_Products() {

		$getVendorRetailProducts			= $this->Reports_Do_Model->Get_Missing_Vendor_Retail_Products(); // Get missing vendor products
 
		$getRetailProducts					= $this->Reports_Do_Model->Get_Missing_Retail_Products(); // Get missing products

		$getRetailListingLibrary			= $this->Reports_Do_Model->Get_Missing_Retail_Listing_Library(); // Get listing library

		$getRetailVariants					= $this->Reports_Do_Model->Get_Missing_Retail_Variants(); // Get missing retail variants
		$getRetailPriceStock				= $this->Reports_Do_Model->Get_Missing_Retail_Price_Stock(); // Get missing retail price stock
		$getMissingPrice     				= array_merge($getRetailVariants , $getRetailPriceStock); // Get missing price 

		$vendorRetailProductsFilePath = "";
		if($getVendorRetailProducts) {
			$InputRootPath							= "CATALOGUE_MISSING_VENDOR_PRODUCTS_ROOT_PATH";
			$InputRequestPath						= "CATALOGUE_MISSING_VENDOR_PRODUCTS_REQUEST_PATH";
			$vendorRetailProductsFilePath 			= $this->CommonArrayToCSV($getVendorRetailProducts, $InputRootPath, $InputRequestPath);
		}

		$retailProductsFilePath = "";
		if($getRetailProducts) {
			$InputRootPath							= "CATALOGUE_MISSING_PRODUCTS_ROOT_PATH";
			$InputRequestPath						= "CATALOGUE_MISSING_PRODUCTS_REQUEST_PATH";
			$retailProductsFilePath 				= $this->CommonArrayToCSV($getRetailProducts, $InputRootPath, $InputRequestPath);
		}

		$retailRetailListingLibraryFilePath = "";
		if($getRetailListingLibrary) {
			$InputRootPath							= "CATALOGUE_MISSING_PRODUCTS_LISTING_LIBRARY_ROOT_PATH";
			$InputRequestPath						= "CATALOGUE_MISSING_PRODUCTS_LISTING_LIBRARY_REQUEST_PATH";
			$retailRetailListingLibraryFilePath 	= $this->CommonArrayToCSV($getRetailListingLibrary, $InputRootPath, $InputRequestPath);
		}

		$retailRetailMissingPriceFilePath = "";
		if($getMissingPrice) {
			$InputRootPath							= "CATALOGUE_MISSING_PRODUCTS_MISSING_PRICE_ROOT_PATH";
			$InputRequestPath						= "CATALOGUE_MISSING_PRODUCTS_MISSING_PRICE_REQUEST_PATH";
			$retailRetailMissingPriceFilePath	 	= $this->CommonArrayToCSV($getMissingPrice, $InputRootPath, $InputRequestPath);
		}

		return array('Pending at cataloging'        => $vendorRetailProductsFilePath, 
					 'Pending at QC stage' 		    => $retailProductsFilePath, 
					 'Pending in channel catalogue' => $retailRetailListingLibraryFilePath,
					 'Pending in retail Price'      => $retailRetailMissingPriceFilePath
					);

	} // Get_Missing_Catalogue_Products

	function isThisDayAWeekend($Report_Date) {

		$timestamp = strtotime($Report_Date);

		$weekday= date("l", $timestamp );
	
		if ($weekday == "Saturday" OR $weekday =="Sunday") { 
			return true; 
		} 
		else { 
			return false;
		}
	} //isThisDayAWeekend

	/* END REPORTS LOGIC */

	public function Get_Report_Analytics($data="") {
		$reportLevel  = isset($data['reportLevel'])?$data['reportLevel']:"";
		$rowID 		  = isset($data['rowID'])?$data['rowID']:"";

		$getTabLevels = $this->Reports_Do_Model->Get_Tab_Menus("", ""); // level 1

		$tabmenus 	  = array();
		foreach ($getTabLevels as $key => $value) {
			$level2 = $this->Reports_Do_Model->Get_Tab_Menus("level2", $value['Menus_IDs']); // level 2
			$level3 = $this->Reports_Do_Model->Get_Tab_Menus("level3", $value['Menus_IDs']); // level 3
			$level4 = $this->Reports_Do_Model->Get_Tab_Menus("level4", $value['Menus_IDs']); // level 4
			$level5 = $this->Reports_Do_Model->Get_Tab_Menus("level5", $value['Menus_IDs']); // level 5

			$tabmenus[$value['Level1']]["Level1"] = $value['Level1'];
			$tabmenus[$value['Level1']]["Level2"] = $level2;
			$tabmenus[$value['Level1']]["Level3"] = $level3;
			$tabmenus[$value['Level1']]["Level4"] = $level4;
			$tabmenus[$value['Level1']]["Level5"] = $level5;
		}
		$getTabLevels = $this->Reports_Do_Model->Get_Report_Analytics(); // tab menus
		// $level1 = array_values(array_unique(array_column($getTabLevels, 'Level1')));
		// $level2 = array_values(array_unique(array_column($getTabLevels, 'Level2')));
		// $level3 = array_values(array_unique(array_column($getTabLevels, 'Level3')));
		// $level4 = array_values(array_unique(array_column($getTabLevels, 'Level4')));
		// $level5 = array_values(array_unique(array_column($getTabLevels, 'Level5')));
		
		//$tabmenus = array('level1' => $level1,'level2' => $level2,'level3' => $level3,'level4' => $level4,'level5' => $level5); // tab menus

		$getReportAnalytics = $this->Reports_Do_Model->Get_Report_Analytics($reportLevel, $rowID); // reports

		$result = array('tabMenus' => $tabmenus, 'reports'=> $getReportAnalytics);

		return  $result;
	}

	public function Get_Report_Analytics_Multi_Level($data="") {
		$reportLevel  = isset($data['report_level'])?$data['report_level']:"";
		$rowID 		  = isset($data['row_id'])?$data['row_id']:"";

		$getReportAnalytics = $this->Reports_Do_Model->Get_Report_Analytics_Multi_Level($reportLevel, $rowID); // reports

		$result = array('reports'=> $getReportAnalytics);

		return  $result;
	}
}