<?php

namespace App\Models\QVPBusinessLogic;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Support\Facades\Cookie;
use App\Models\DbModel\User;

class User_Authentication_Model
{
    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * SignIn & SignUp Check Data Validation.
    * @param Request $request
    * @param Function Name $funcName
    * @return Validated Request Response
    */
    public static function formValidation($request, $funcName){

        //Signup form fields validate...
        if($funcName == 'signup'){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]); 
        }

        //Signin form fields validate...
        if($funcName == 'signin'){
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users,email',
                'password' => 'required|min:3',
            ], [
                'email.exists' => 'The user credentials were incorrect.',
            ]);
        }
        
        return $validator; 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * SignIn Auth Verification.
    * @param Request $request
    * @return JSON Response
    */
    public static function signInVerification($request){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            if($user){
                $success['token'] = $user->createToken('authToken')->accessToken;
                $success['user'] = $user;
                return $success;
            }
            else{
                $data['error'] = ['email' => 'The user credentials were incorrect.'];
                $result = response()->json($data, 401);
            }
        }else{
            $data['error'] = ['email' => 'The user credentials were incorrect.'];
            $result = response()->json($data, 401);
        }
        return $result;
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 05-01-2022
    * Update Auth Token into user session_token
    * @param Request $request
    * @return true or false
    */
    public static function updateUser($request){
        $user = User::where('id',$request['user']['id'])->update(['api_token'=>$request['token']]);
        return $user ? true : false;     
    }
}
