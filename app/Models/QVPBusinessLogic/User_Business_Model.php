<?php

namespace App\Models\QVPBusinessLogic;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Support\Facades\Cookie;
use App\Models\DbModel\User;
use App\Models\DbModel\UserInfo;
use App\Models\DbModel\Role;
use App\Core\Traits\ImageUploadTrait;
use \Illuminate\Support\Arr;

class User_Business_Model
{
    use ImageUploadTrait;
    
    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Check image type and validation.
    * @param Request $request
    * @return $validator
    */
    public static function checkUserValidations($request, $id=null){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            'Vendors' => 'required',
            'Price' => 'required',
            'Sales_Channel' => 'required',
        ]); 

        return $validator; 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * Password Bycrypt and set into input
    * @param Request $request
    * @return $input
    */
    public static function setPasswordBycrypt($input){
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }
        return $input; 
    }

    /*
    * Author: Nirbhay Sharma
    * Date: 24-01-2022
    * assign role to user
    * @param Request $request
    * @return Response Array $params
    */
    public static function assignRoleToUser($user, $roleArr){
        return $user->roles()->attach($roleArr);
    }
}
