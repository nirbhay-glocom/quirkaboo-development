<?php

namespace App\DataTables;

use Illuminate\Http\Request;
use App\Models\QVPBusinessLogic\Order_Information_Model;
use Illuminate\Validation\Rule;
use Datatables;

class OrdersDataTable
{
	public function getDataTable()
	{
          $orders = Order_Information_Model::getAllOrdersInfo();
             return Datatables::of($orders)
            ->editColumn(
                'Vendor',
                function ($row) {                  
                    return $row->Vendor;
                }
            )  
            ->editColumn(
                'Order_Date',
                function ($row) {                  
                    return $row->Order_Date;
                }
            ) 
            ->editColumn(
                'Sales_Channel',
                function ($row) {                  
                    return $row->Sales_Channel;
                }
            )             
            ->editColumn(
                'created_at',
                function ($row) {
                    return $row->Created_At;
                }
            )
            ->rawColumns([])
            ->make(true);
  }
}