<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\SampleDataController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\UserAuthController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\ImageCustomizedController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

// Sample API route
Route::post('/register', [RegisteredUserController::class, 'apiStore']);
Route::get('/login', [AuthenticatedSessionController::class, 'apiStore']);
Route::get('/profits', [SampleDataController::class, 'profits'])->name('profits');
Route::get('/forgot_password', [PasswordResetLinkController::class, 'apiStore']);
Route::get('/verify_token', [AuthenticatedSessionController::class, 'apiVerifyToken']);
Route::get('/users', [SampleDataController::class, 'getUsers']);

//QVP APIS...
// Route::post('signup',[App\Http\Controllers\API\UserAuthController::class, 'signup'])->name('signup');

// Route::post('signin',[App\Http\Controllers\API\UserAuthController::class, 'signin'])->name('signin');

Route::get('orders-list', [App\Http\Controllers\API\OrderController::class, 'index'])->name('orders-list.index');

Route::post('order-detail', [App\Http\Controllers\API\OrderController::class, 'orderDetail'])->name('order-detail');

Route::post('update-order', [App\Http\Controllers\API\OrderController::class, 'updateOrderDetail'])->name('update-order');

Route::post('upload-image', [App\Http\Controllers\API\ImageCustomizedController::class,'save'])->name('upload-image');

Route::post('delete-image',  [App\Http\Controllers\API\ImageCustomizedController::class, 'deleteImage'])->name('delete-image');

Route::post('update-content-detail',  [App\Http\Controllers\API\ImageCustomizedController::class, 'updateCustomizedImage'])->name('update-content-detail');

Route::post('get-customized-order-data',  [App\Http\Controllers\API\ImageCustomizedController::class, 'getDataAfterDeleteImageRow'])->name('get-customized-order-data');

Route::post('reject-customization-image',  [App\Http\Controllers\API\ImageCustomizedController::class, 'rejectImage'])->name('reject-customization-image');

Route::post('approve-customization-image',  [App\Http\Controllers\API\ImageCustomizedController::class, 'approveImage'])->name('approve-customization-image');

Route::post('update-vendor-comment',  [App\Http\Controllers\API\ImageCustomizedController::class, 'updateComments'])->name('update-vendor-comment');

Route::post('update-rejection-reason',  [App\Http\Controllers\API\ImageCustomizedController::class, 'updateRejectionReason'])->name('update-rejection-reason');

Route::post('update-notes',  [App\Http\Controllers\API\ImageCustomizedController::class, 'updateNotes'])->name('update-notes');

Route::post('ajx-update-notes',  [App\Http\Controllers\API\ImageCustomizedController::class, 'ajxUpdateNotes'])->name('ajx-update-notes');

Route::post('upload-preview-image', [App\Http\Controllers\API\PreviewController::class,'save'])->name('upload-preview-image');

Route::post('select-preview-image', [App\Http\Controllers\API\PreviewController::class,'approveSelectedImage'])->name('select-preview-image');

Route::post('upload-gallery-image', [App\Http\Controllers\API\GalleryController::class,'save'])->name('upload-gallery-image');

Route::post('approve-gallery-image', [App\Http\Controllers\API\GalleryController::class,'approveSelectedImages'])->name('approve-gallery-image');

Route::post('update-preview-notes',  [App\Http\Controllers\API\PreviewController::class, 'updatePreviewNotes'])->name('update-preview-notes');

Route::post('update-gallery-notes',  [App\Http\Controllers\API\GalleryController::class, 'updateGalleryNotes'])->name('update-gallery-notes');

Route::post('select-customer-preview-image', [App\Http\Controllers\API\GalleryController::class,'selectCustomerPreviewImages'])->name('select-customer-preview-image');

Route::post('update-customer-preview-notes',  [App\Http\Controllers\API\GalleryController::class, 'updateCustomerPreviewNotes'])->name('update-customer-preview-notes');

Route::post('set-content',  [App\Http\Controllers\API\GalleryController::class, 'setContent'])->name('set-content');

Route::post('update-order-texts', [App\Http\Controllers\API\OrderController::class, 'updateOrderText'])->name('update-order-texts');

Route::post('update-order-vendor-notes', [App\Http\Controllers\API\OrderController::class, 'updateOrderVendorNotes'])->name('update-order-vendor-notes');

Route::post('update-order-customer-notes', [App\Http\Controllers\API\OrderController::class, 'updateOrderCustomerNotes'])->name('update-order-customer-notes');

Route::post('update-order-gift-message', [App\Http\Controllers\API\OrderController::class, 'updateOrderGiftMessage'])->name('update-order-gift-message');

Route::post('get-order-preview-media',  [App\Http\Controllers\API\PreviewController::class, 'getOrderPreviewMedia'])->name('get-order-preview-media');

