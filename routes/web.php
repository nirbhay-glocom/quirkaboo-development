<?php

use App\Http\Controllers\Account\SettingsController;
use App\Http\Controllers\Auth\SocialiteLoginController;
use App\Http\Controllers\Documentation\ReferencesController;
use App\Http\Controllers\Logs\AuditLogsController;
use App\Http\Controllers\Logs\SystemLogsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\UsersController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\CustomerUrlController;
use App\Http\Controllers\Backend\Reports;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Backend\PermissionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect('index');
// });

$menu = theme()->getMenu();
array_walk($menu, function ($val) {
    if (isset($val['path'])) {
        $route = Route::get($val['path'], [PagesController::class, 'index']);

        // Exclude documentation from auth middleware
        if (!Str::contains($val['path'], 'documentation')) {
            $route->middleware('auth');
        }
    }
});

// Documentations pages
Route::prefix('documentation')->group(function () {
    Route::get('getting-started/references', [ReferencesController::class, 'index']);
    Route::get('getting-started/changelog', [PagesController::class, 'index']);
});

// Route::get('customer/{id}/{token}', [BackendController::class, 'customer'])
//                 ->name('customer');

Route::middleware('auth')->group(function () {
    Route::group(['middleware' => 'customAuth'], function () {
        // Account pages
        Route::prefix('account')->group(function () {
            Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
            Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
            Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail');
            Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
        });

        // Logs pages
        Route::prefix('log')->name('log.')->group(function () {
            Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
            Route::resource('audit', AuditLogsController::class)->only(['index', 'destroy']);
        });

        //Orders Listing Page...
        Route::prefix('orders')->group(function () {
            Route::get('orders-list',  [OrderController::class, 'index'])->name('orders.index');
        });

        //Users Page...
        Route::resource('users', UsersController::class);

        //Roles Page...
        Route::resource('roles', RoleController::class);

        Route::get(
            'add-permissions/{id}',
            [RoleController::class, 'addPermissions']
        )
        ->name('roles.add_permissions');
        
        Route::get(
                'update-permissions/{id}',
                [RoleController::class, 'updatePermissions']
            )
            ->name('roles.update_permissions');
        
        Route::post(
                'add-role-permissions',
                [RoleController::class, 'addRolePermissions']
            );
        
        Route::post(
                'update-role-permissions',
                [RoleController::class, 'updateRolePermissions']
            );
    
        Route::post(
            'send-qvp-mail',
            [Reports::class, 'SendQvpMail']
        );

    });
});
// Route::resource('users', UsersController::class);

Route::get(
    'customer-preview/{public_key}/{token}', 
    [CustomerUrlController::class, 'index']
)
->name('customer-url');

//Check For Auth User...
// Route::group(['middleware' => 'customAuth'], function () {
//     Route::get('/dashboard',  [App\Http\Controllers\Backend\BackendController::class, 'index'])->name('dashboard');
//     Route::get('logout',  [App\Http\Controllers\Backend\BackendController::class, 'logout'])->name('logout');
    
// });

/**
 * Socialite login using Google service
 * https://laravel.com/docs/8.x/socialite
 */
Route::get('/auth/redirect/{provider}', [SocialiteLoginController::class, 'redirect']);

require __DIR__.'/auth.php';
