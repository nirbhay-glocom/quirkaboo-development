<div id="order-customization_{{$order->ID}}" role="tabpanel" aria-labelledby="order-customization-tab_{{$order->ID}}">
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/customization-image.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/lightbox.min.css" />

@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
@foreach(auth()->user()->roleHasContainerPermissions() as $val)
@if(($val['Type']=='Tab') && ($val['Name']=='Customization') && ($val['section']=='Show'))

		@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
		@foreach(auth()->user()->roleHasContainerPermissions() as $val)
		@if(($val['Type']=='Section') && ($val['Name']=='Customization Image Upload') && ($val['section']=='Edit'))
	
		<div class="headTitle">
			<h3 class="fw-bolder m-0" style="color:#000">{{ __('1.) Text Customization') }}</h3>                
		</div>
		<div class=" container cus_details">
			<div class="form-group row" style="margin:5px;">
				<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm"><h6>Customization Text</h6></label>
				<div class="col-sm-4" style="margin-left:-45px;">
					<textarea readonly class="form-control form-control-sm" style="border: 1.5px solid #ccc;height: 50px;" id="custionTxt">{{preg_replace('/[-?]/', '', $order->Custom_Text)}}</textarea>
				</div>
				<div class="col-sm-4"></div>

				<div class="col-sm-4 col-form-label col-form-label-sm"></div>
				<div class="col-sm-4 col-form-label col-form-label-sm" style="margin: -20px 0 0 -42px !important;">
					<span class="OrdrimpNote" style="font-size: 13px !important;">Important *</span>
				</div>
				<div class="col-sm-4"></div>
			</div>
			</div>

		<div class="headTitle">
			<h3 class="fw-bolder m-0" style="color:#000">{{ __('2.) Image Customization') }}</h3>                
		</div><br/>

		<!-- Customized Image -->
		<form method="POST" enctype="multipart/form-data" class="upload_image_form" action="javascript:void(0)" >
			<input type="hidden" id="Order_Id" name="Order_Id"  value="{{$order->ID}}">
			<input type="hidden" id="Order_Product_Id" name="Order_Product_Id"  value="@if(isset($order->products[0]->ID)) {{$order->products[0]->ID}} @endif">
			<input type="hidden" name="UserId" value="{{$userId}}" />
			<div class=" container cus_details">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm"><h6>Upload Image </h6></label>
					<div class="col-sm-4" style="margin-left: -40px;">
						<input type="file" class="form-control btn-upload" name="Image_URL" placeholder="Choose image" id="Image_URL" accept="image/*" required/>
						<!-- <span class="text-danger">{{ $errors->first('title') }}</span> -->	
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn-sm btn-red" id="customizeSub">Submit</button>
					</div>
				</div>
				<div class="form-group row">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-4">
                            <img id='loadingCustom' style="width:100px" src='{{asset('/')}}assets/img/loading.gif' style='visibility: hidden;'>
                        </div>
                        <div class="col-sm-4"> </div>
                    </div>
				<br>
				</form>
				<!-- <div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Custom Text:</label>
					<div class="col-sm-10">
						<textarea class="form-control form-control-sm" id="Customize_Text" name="Customize_Text">{{preg_replace('/[-?]/', '', $order->Custom_Text)}}</textarea>
					</div>
				</div><br>

				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
					<div class="col-sm-10">
						<textarea class="form-control form-control-sm" id="Notes" name="Notes"></textarea>
					</div>
				</div><br> -->
				
				<div class="row">
					<div class="col-sm-4 col-form-label col-form-label-sm">
						<label for="colFormLabelSm"><h6>Order Notes</h6></label>
						
						<a href="javascript:void(0);" style="margin: -5px;border: 2px solid #ccc;background-color: #ddd!important;" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow orderText" onclick="displayOrderText('{{ $order->ID}}', '{{$order->Notes}}', '{{"Notes"}}')" id="order_note_{{$order->ID}}" data-backdrop="static" data-keyboard="false" style="float: right;">
							<i class="bi bi-pencil-fill fs-7"></i>
						</a>
						
					</div>
					{{-- <div class="col-sm-5 col-form-label col-form-label-sm" style="border:4px solid red" id="OrderNotes{{$order->ID}}"><h6>Order Notes -</h6> {{$order->Notes}}</div> --}}
				</div>
				
				
				<div class="row">
					<div class="col-sm-4 col-form-label col-form-label-sm">				
						<label for="colFormLabelSm"><h6>Order Custom Text</h6></label>
						
						<a href="javascript:void(0);" style="margin: -5px;border: 2px solid #ccc;background-color: #ddd!important;" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow orderText" onclick="displayOrderText('{{ $order->ID}}', '{{$order->Custom_Text}}', '{{"Custom_Text"}}')" id="order_custome_text_{{$order->ID}}" data-backdrop="static" data-keyboard="false" style="float: right;">
							<i class="bi bi-pencil-fill fs-7"></i>
						</a>	
					</div>
					{{-- <div class="col-sm-10 col-form-label col-form-label-sm" id="OrderTexts{{$order->ID}}">{{$order->Custom_Text}}</div> --}}
				</div>
				
				
			</div>
			<!-- Order Text Modal -->
			<div class="modal" id="orderTextModal_{{$order->ID}}" class="ordTxtmod">
				<div class="modal-dialog">
					<div class="modal-content">					
						<!-- Modal Header -->
						<div class="modal-header">
							<h4 class="modal-title" style="color: #fff;" id="headingTxt"></h4>
							<button type="button" class="btn-close-white" onclick="orderTxtModel({{$order->ID}})">&times;</button>
						</div>
						
						<!-- Modal body -->
						<div class="modal-body" id="cusBod" style="display:none;">
							<div class="form-group">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Custom Text:</label>
								<div class="col-sm-10">
									<textarea class="form-control form-control-sm" id="OrderCustom_Text_{{$order->ID}}" name="Custom_Text" style="width: 117%;">{{$order->Custom_Text}}</textarea>
									<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
								</div>
							</div>
						</div>
						
						<div class="modal-body" id="notBod" style="display:none;">
							<div class="form-group">
								<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Order Notes:</label>
								<div class="col-sm-10">
									<textarea class="form-control form-control-sm" id="OrderNotes_{{$order->ID}}" name="Notes" style="width: 117%;">{{$order->Notes}}</textarea>
									<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
								</div>
							</div>
						</div>
						
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-info" onclick="updateOrderText('{{$order->ID}}')">Update</button>
						</div>
					</div>
				</div>
			</div>
			<!-- End Updated Order Text Modal -->
		    <br><hr><br>
		@endif
		@endforeach
		@endif

		@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
		@foreach(auth()->user()->roleHasContainerPermissions() as $val)
		@if(($val['Type']=='Section') && ($val['Name']=='Customization Image Boxes') && ($val['section']!='None'))
		
		
		{{--<div class=" container cus_details" id="customTx">
		@if(!empty($order->Custom_Text))
			<div class="form-group row">
				<div class="col-sm-12">
				<p id="cusTxt" style="background-color:yellow;padding:5px 5px 5px 5px;border: 4px solid red;"><strong>Customization Text - </strong>{{preg_replace('/[-?]/', '', $order->Custom_Text)}}</p>
				</div>
			</div>
		@endif
		</div><br>--}}
		

		<div class="outer-grid grid-container customizationGrid" id="customizationGrid_{{$order->ID}}">
			@foreach($order->customizations as $key => $value)
				<div class="inner-grid img-wraps" id="box_{{$value->ID}}">

				 {{-- <span class="closes" title="Delete" onclick="deleteCustomizeImage({{$value->ID}})">×</span> --}}
				 
				  
					    <a class="example-image-link" href="{{$value->Image_URL}}" data-lightbox="example-1" style="width: 100% !important;display: block !important;">
							<div class="divpic">
								<img class="example-image" src="{{$value->Image_URL}}" alt="image-1" />
							</div>
						</a>					
						<a href="{{$value->Image_URL}}" download class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">
							<i class="bi bi-download"></i>
						</a>
					
						<div class="form-group">
							<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label> 

							@if(($val['Type']=='Section') && ($val['Name']=='Customization Image Boxes') && ($val['section']=='Edit'))								
								<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow customizationNot" onclick="displayNote({{$value->ID}})" id="note_{{$value->ID}}" data-backdrop="static" data-keyboard="false" style="float: right;">
									<i class="bi bi-pencil-fill fs-7"></i>
								</a>
							@endif
							
							<div class="col-sm-10" id="divnote_{{$value->ID}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($value->Notes)) style="display:none" @endif >							
								<span class="form-control form-control-sm readable" id="noteUp_{{$value->ID}}" style="width: 122%;">{{$value->Notes}}</span>							
							</div>
						</div>
					
					

					<!-- Updated Notes Modal -->
					<div class="modal" id="noteModel_{{$value->ID}}" class="notmod">
						<div class="modal-dialog">
							<div class="modal-content">					
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title" style="color: #fff;">Update Notes</h4>
									<button type="button" class="btn-close-white" onclick="notModel({{$value->ID}})">&times;</button>
								</div>
								
								<!-- Modal body -->
								<div class="modal-body">
									<div class="form-group">
										<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
										<div class="col-sm-10">
											<textarea class="form-control form-control-sm" id="Notes_{{$value->ID}}" name="Notes" style="width: 117%;"></textarea>
											<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
										</div>
									</div>
								</div>
								
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn-sm btn-info" onclick="updateNotes({{$value->ID}})">Update Notes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Updated Notes Modal -->
			
				</div>
			@endforeach
		</div>
		<div class="ajax-outer-grid grid-container"></div>
		@endif
		@endforeach
		@endif
@endif
@endforeach
@endif
</div>