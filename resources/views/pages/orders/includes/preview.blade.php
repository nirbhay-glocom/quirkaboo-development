<div id="preview_{{$order->ID}}" role="tabpanel" aria-labelledby="preview-tab_{{$order->ID}}">
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/customization-image.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/lightbox.min.css" />

<style>
/* The container */
.conta {
  cursor: pointer;
}

/* Hide the browser's default radio button */
.conta input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #ddd;
  border-radius: 50%;
  margin:5px;
}

/* On mouse-over, add a grey background color */
.conta:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.conta input:checked ~ .checkmark {
  background-color: #FF0000;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.conta input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.conta .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}

.selectBox{
    border:2px solid #000 !important;
    opacity: 1;
}

.checko{
    background-color: #FF0000;
}

</style>


@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
@foreach(auth()->user()->roleHasContainerPermissions() as $val)
@if(($val['Type']=='Tab') && ($val['Name']=='Preview') && ($val['section']=='Show'))
    <br/>
    <div class="headTitle">
         <h3 class="fw-bolder m-0" style="color:#000">{{ __('Preview Image/Video') }}</h3>                
    </div><br/>
    @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
	@foreach(auth()->user()->roleHasContainerPermissions() as $val)
	@if(($val['Type']=='Section') && ($val['Name']=='Preview Image Upload') && ($val['section']=='Edit'))
    
        <!-- Preview Image Upload -->
		<form method="POST" enctype="multipart/form-data" class="upload_preview_form" action="javascript:void(0)" >
			<input type="hidden" id="Order_Id" name="Order_Id"  value="{{$order->ID}}">
			<div class=" container cus_details">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm"><h6>Upload Image/Video </h6></label>
					<div class="col-sm-4">
						<input type="file" class="form-control btn-upload" name="URL" placeholder="Choose image/video" id="URL" accept="video/*,image/*" required/>	
                    </div> 

                    <div class="col-sm-4"> 
                        <button type="submit" class="btn-sm btn-red" id="preSubmit">Submit</button>
					</div>

                    <div class="form-group row">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-4">
                            <img id='loadingPre' style="width:100px" src='{{asset('/')}}assets/img/loading.gif' style='visibility: hidden;'>
                        </div>
                        <div class="col-sm-4"> </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-4 col-form-label col-form-label-sm">
                            <label for="colFormLabelSm"><h6>Vendor Notes </h6></label>

                            <a href="javascript:void(0);" style="border: 2px solid #ccc;background-color: #ddd!important;" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow vendorNotes" onclick="displayOrderVendorNotes('{{ $order->ID}}', '{{$order->Vendor_Notes}}')" id="ordervendornote_{{$order->ID}}" data-backdrop="static" data-keyboard="false">
                                <i class="bi bi-pencil-fill fs-7"></i>
                            </a>
                        </div>
                            
                        
                        <textarea class="col-sm-4 col-form-label col-form-label-sm noteBoxDiv" id="OrderVendorNote{{$order->ID}}" readonly>{{preg_replace('/[-?]/', '', $order->Vendor_Notes)}}</textarea>
                            
                            
                    </div>

                    <div class="col-sm-4 col-form-label col-form-label-sm"></div>
                    <div class="col-sm-4 col-form-label col-form-label-sm" style="margin: -10px 0 0 4px !important;">
                        <span class="OrdrimpNote" style="font-size: 13px !important;">Important *</span>
                    </div>
                    <div class="col-sm-4"></div>
                   
				</div><br>
            <!-- Order Vendor Note Modal -->
            <div class="modal" id="orderVendornote_{{$order->ID}}" class="ordVenmod">
				<div class="modal-dialog">
					<div class="modal-content">					
						<!-- Modal Header -->
						<div class="modal-header">
							<h4 class="modal-title" style="color: #fff;">Update Vendor Notes</h4>
							<button type="button" class="btn-close-white" onclick="orderVendNotModel({{$order->ID}})">&times;</button>
						</div>
						
						<!-- Modal body -->
						<div class="modal-body">
							<div class="form-group">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Vendor Notes:</label>
								<div class="col-sm-9">
									<textarea class="form-control form-control-sm" id="OrderVendor_Notes_{{$order->ID}}" name="Vendor_Notes" style="width: 133%;">{{$order->Vendor_Notes}}</textarea>
									<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
								</div>
							</div>
						</div>
						
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-info" onclick="updateOrderVendorNote('{{$order->ID}}')">Update</button>
						</div>
					</div>
				</div>
			</div>
			<!-- End Updated Order Text Modal -->
			</div>

           
		</form><br><hr><br>
    @endif
    @endforeach
    @endif

    @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
    
    @foreach(auth()->user()->roleHasContainerPermissions() as $val)
    @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
        
        <form method="POST" enctype="multipart/form-data" class="updatePreviewContent" action="javascript:void(0)"> 
        <input type="hidden" name="Order_Id" value="{{$order->ID}}">
        <input type="hidden" class="checkedRadio" name="radio" >
        <div class="outer-grid grid-container" id="previewGrid_{{$order->ID}}">        
            @if($order->Preview_Media != 'null')               
                @php
                    $previewArr = json_decode($order->Preview_Media, true); 
                @endphp

                @if($previewArr)
                @foreach($previewArr as $key => $preview)
                
                    @if($preview['Media_Type']=='Image')
                    
                        <div class="inner-grid img-wraps previewBox_{{$order->ID.$key}} @if($preview['Status']=='Approved') selectBox  @endif" id="previewBox_{{$order->ID.$key}}">
                            <h6 style="padding: 10px 0px 0px 0px;text-align:center;">{{ $preview['Date'] }}</h6>
                            
                            {{--
                            <label class="conta">
                                <input type="radio" @if($preview['Status']=="Approved") checked="checked" @endif name="radio" id="{{$key}}" onchange="checkedPreview('{{$order->ID}}', '{{$key}}')" value="{{$key}}">
                                <span class="checkmark"></span>
                            </label>
                            --}}
                            
                           
                            <a class="example-image-link" href="{{$preview['URL']}}" data-lightbox="example-1" style="width: 100% !important;display: block !important;">
                                <div class="divpic">
                                    <img class="example-image" src="{{$preview['URL']}}" alt="image-1" />
                                </div>
                            </a>
                            <a href="{{$preview['URL']}}" download class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">
                                <i class="bi bi-download"></i>
                            </a>
                           
                            @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
                            <div class="form-group">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label> 

                                    <a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openPreNotes" onclick="displayPreviewNote({{$order->ID.$preview['ID']}})" id="pnote_{{$order->ID.$preview['ID']}}" data-toggle="modal" data-target="#pnoteModel_{{$order->ID.$preview['ID']}}" data-backdrop="static" data-keyboard="false" style="float: right;">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                    </a>
                               

                                <div class="col-sm-10" id="divnote_{{$order->ID.$preview['ID']}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($preview['Notes'])) style="display:none" @endif>							
                                    <span class="form-control form-control-sm readable" id="pnoteUp_{{$order->ID.$preview['ID']}}" style="width: 122%;">{{$preview['Notes']}}</span>							
                                </div>
                            </div><br><br>
                            @endif
                            
                        </div>
                    @endif

                    @if($preview['Media_Type']=='Video')
                        
                        <div class="inner-grid img-wraps previewBox_{{$order->ID.$key}} @if($preview['Status']=='Approved') selectBox  @endif" id="previewBox_{{$order->ID.$key}}">
                            <h6 style="padding: 10px 0px 0px 0px;text-align:center;">{{ $preview['Date'] }}</h6>

                            {{--
                            <label class="conta">
                                <input type="radio" @if($preview['Status']=="Approved") checked="checked" @endif name="radio" id="{{$key}}" onchange="checkedPreview('{{$order->ID}}', '{{$key}}')" value="{{$key}}">
                                <span class="checkmark"></span>
                            </label>
                            --}}
                            
                            <div class="divpic">
                                <video width="100%" height="" controls class="preview-vid" >
                                    <source class="example-image" src="{{$preview['URL']}}" type="video/mp4">
                                </video>
                            </div>
                            
                            @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
                            <div class="form-group">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label> 

                                    <a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openPreNotes" onclick="displayPreviewNote({{$order->ID.$preview['ID']}})" id="pnote_{{$order->ID.$preview['ID']}}" data-toggle="modal" data-target="#pnoteModel_{{$order->ID.$preview['ID']}}" data-backdrop="static" data-keyboard="false" style="float: right;">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                    </a>
                                

                                <div class="col-sm-10" id="divnote_{{$order->ID.$preview['ID']}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($preview['Notes'])) style="display:none" @endif>							
                                    <span class="form-control form-control-sm readable" id="pnoteUp_{{$order->ID.$preview['ID']}}" style="width: 122%;">{{$preview['Notes']}}</span>							
                                </div>
                            </div><br><br>
                            @endif
                        </div>
                    @endif

                    <!-- Updated Notes Modal -->
					<div class="modal" id="pnoteModel_{{$order->ID.$preview['ID']}}" class="pnotmod">
                        
						<div class="modal-dialog">
							<div class="modal-content">					
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title" style="color: #fff;">Update Preview Notes:</h4>
									<button type="button" class="btn-close-white" onclick="pnotModel({{$order->ID.$preview['ID']}})">&times;</button>
								</div>
								
								<!-- Modal body -->
								<div class="modal-body">
									<div class="form-group">
										<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
										<div class="col-sm-10">
											<textarea class="form-control form-control-sm" id="pNotes_{{$order->ID.$preview['ID']}}" name="Notes" style="width: 117%;"></textarea>
											<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
										</div>
									</div>
								</div>
                                
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn-sm btn-info" onclick="updatePNotes({{$order->ID}}, {{ $preview['ID'] }})">Update Notes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Updated Notes Modal -->
                @endforeach 
                @endif
            @endif
        </div><br/>

           
        <div id="pbutt_{{$order->ID}}" style="float: right !important;" @if($order->Preview_Media == 'null') style="display: none" @else style="display: block" @endif>
            <div class="col-sm-12" style="float:right;position: fixed;bottom: 70px;margin: 0px 0px 0px -80px !important;"> 
                <button type="" class="btn-sm btn-info">Submit</button>
            </div>
        </div>
       
        </form>
    @endif
    @endforeach
    @endif

@endif
@endforeach
@endif
</div>


