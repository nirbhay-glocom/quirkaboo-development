<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/order-info.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/lightbox.min.css" />
@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
@foreach(auth()->user()->roleHasContainerPermissions() as $val)
@if(($val['Type']=='Tab') && ($val['Name']=='Order Detail') && ($val['section']=='Show'))

	<div class="headTitle">
		<h3 class="fw-bolder m-0" style="color:#000">{{ __('Order Detail') }}</h3>                
	</div><br/>

	{{-- <div class="col-sm-12 col-form-label col-form-label-sm" style="border:1.5px solid #E2001B;padding:5px 5px 5px 5px;" id="OrderNotes{{$order->ID}}"><h6>Order Notes -</h6> {{$order->Notes}}</div> --}}

	<div class="form-group row">
		<div class="col-sm-2 col-form-label col-form-label-sm">
			<label for="colFormLabelSm"><h6>Order Notes </h6></label>
		</div>
			
		
			<textarea readonly class="col-sm-10 col-form-label col-form-label-sm OrderNoteBoxDiv" id="OrderNotes{{$order->ID}}">{{preg_replace('/[-?]/', '', $order->Notes)}}</textarea>

			<div class="col-sm-2 col-form-label col-form-label-sm"></div>
			<div class="col-sm-10 col-form-label col-form-label-sm" style="margin: -10px 0 0 0 !important;">
				<span class="OrdrimpNote" style="font-size: 13px !important;">Important *</span>
			</div>
	</div>

	<!-- Order Detail Code Here -->
	<div id="orderInfo_{{$order->ID}}" class="tabcontent">
	 @csrf
		<div class="row">
			<input type="hidden" id="ID" name="ID"  value="{{$order->ID}}">
			<input type="hidden" id="ProdID" name="ProdID"  value="@if(isset($order->products[0]->ID)) {{$order->products[0]->ID}} @endif">
			@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
			@foreach(auth()->user()->roleHasContainerPermissions() as $val)
			@if(($val['Type']=='Section') && ($val['Name']=='Order Detail Section - 1') && ($val['section']!='None'))
			<?php
				$oneClass = '';
				if($val['section']=='Show'){
					$oneClass = 'readable';
				}

				if($val['section']=='View'){
					$oneClass = 'readable';
				}

				if($val['section']=='Edit'){
					$oneClass = 'form-control form-control-sm';
				}
			?>
			
			<!-- First Column -->
			<div class="col-sm-4 first_cont">
				<div class="first_coloumn">
					<div class="col_first_content">
						<label>Glocom SKU</label>
						<div class="back_1">
							<input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif name="Glocom_SKU" id="Glocom_SKU_{{$order->ID}}"  value="{{$order->Glocom_SKU}}">
						</div>
					</div>

					<div class="col_first_content">
						<label>Vendor SKU</label>
						<div class="back_1">
							<input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Vendor_SKU_{{$order->ID}}" name="Vendor_SKU"  value="{{$order->Vendor_SKU}}">
						</div>
					</div>

					<div class="col_first_content">
						<label>Product Type</label>
						<div class="back_1">
							<input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Product_Type_{{$order->ID}}" name="Product_Type"  value="@if(isset($order->products[0]->Product_Type)) {{$order->products[0]->Product_Type}} @endif">
						</div>
					</div>

					<div class="col_first_content">
						<label>Product Name</label>
						<div class="back_1">
							<input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Product_Name_{{$order->ID}}" name="Product_Name"  value="@if(isset($order->products[0]->Product_Name)){{$order->products[0]->Product_Name}} @endif">
						</div>
					</div>

					<div class="col_first_content">
						<label>Variation 1</label>
						<div class="col-sm-10">
							<input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Variation_1_{{$order->ID}}" name="Variation_1"  value="@if(isset($order->products[0]->Variation_1)) {{$order->products[0]->Variation_1}} @endif">
						</div>
					</div>

					<div class="col_first_content">
						<label>Variation 2</label>
						<div class="col-sm-10">
							<input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Variation_2_{{$order->ID}}" name="Variation_2"  value="@if(isset($order->products[0]->Variation_1)) {{$order->products[0]->Variation_2}} @endif">
						</div>
					</div>	
				</div>
			</div>
			@endif
			@endforeach
			@endif

			<!-- Second Column -->
			@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
			@foreach(auth()->user()->roleHasContainerPermissions() as $val)
			@if(($val['Type']=='Section') && ($val['Name']=='Order Detail Section - 2') && ($val['section']!='None'))
			<?php
				$oneClass = '';
				if($val['section']=='Show'){
					$oneClass = 'secondReadable';
				}

				if($val['section']=='View'){
					$oneClass = 'secondReadable';
				}

				if($val['section']=='Edit'){
					$oneClass = 'form-control';
				}
			?>
			<div class="col-sm-4 second_cont" >
				<div class="first_coloumn"><br/><br/><br/>
					<div class="form-group row">
					<div class="col-sm-5"><lable>Preview Needed </lable></div>
							<div class="col-sm-7">
								{{-- <input type="text" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Preview_Needed_{{$order->ID}}" name="Preview_Needed" value="{{$order->Preview_Needed}}"> --}}

								<select name="Preview_Needed" id="Preview_Needed_{{$order->ID}}" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) disabled @endif>
								    <option value="Yes">Select</option>
									<option @if($order->Preview_Needed=='Yes') {{"selected"}} @endif value="Yes">Yes</option>
									<option @if($order->Preview_Needed=='No') {{"selected"}} @endif value="No">No</option>
									<option @if($order->Preview_Needed=='Closed') {{"selected"}} @endif value="Closed">Closed</option>
								</select>
							</div>
					</div><br/><br/>

					<div class="form-group row">
							<div class="col-sm-5"><lable>Preview Due </lable></div>
							<div class="col-sm-7">
								<input type="date" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Preview_Due_{{$order->ID}}" name="Preview_Due" value="{{$order->Preview_Due}}">
							</div>
					</div><br/><br/>

					<div class="form-group row">
							<div class="col-sm-5"><lable>Preview Received </lable></div>
							<div class="col-sm-7">
								<input type="date" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Preview_Received_{{$order->ID}}" name="Preview_Received" value="{{$order->Preview_Received}}">
							</div>
					</div><br/><br/>
					<div class="form-group row">
							<div class="col-sm-5"><lable>Revision Needed</lable></div>
							<div class="col-sm-7">
								
								<select name="Revision_Needed" id="Revision_Needed_{{$order->ID}}" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) disabled @endif>
								    <option value="Yes">Select</option>
									<option @if($order->Revision_Needed=='Yes') {{"selected"}} @endif value="Yes">Yes</option>
									<option @if($order->Revision_Needed=='No') {{"selected"}} @endif value="No">No</option>
									<option @if($order->Revision_Needed=='Closed') {{"selected"}} @endif value="Closed">Closed</option>
								</select>
							</div>
					</div><br/><br/>
					<div class="form-group row">
							<div class="col-sm-5"><lable>Revision Due </lable></div>
							<div class="col-sm-7">
								<input type="date" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Revision_Due_{{$order->ID}}" name="Revision_Due" value="{{$order->Revision_Due}}">
							</div>
					</div><br/><br/>
					<div class="form-group row">
							<div class="col-sm-5"><lable>Last Revision Received </lable></div>
							<div class="col-sm-7">
								<input type="date" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Revision_Received_{{$order->ID}}" name="Revision_Received" value="{{$order->Revision_Received}}">
							</div>
					</div><br/><br/>
					<div class="form-group row">
							<div class="col-sm-5"><lable>Warehouse Delivery Due</lable></div>
							<div class="col-sm-7">
								<input type="date" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Warehouse_Delivery_Due_{{$order->ID}}" name="Warehouse_Delivery_Due" value="{{$order->Warehouse_Delivery_Due}}">
							</div>
					</div><br/><br/>
					<div class="form-group row">
							<div class="col-sm-5"><lable>Warehouse Delivery Received </lable></div>
							<div class="col-sm-7">
								<input type="date" class="{{$oneClass}}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Warehouse_Delivery_Received_{{$order->ID}}" name="Warehouse_Delivery_Received"  value="{{$order->Warehouse_Delivery_Received}}">
							</div>
					</div><br/><br/>
				</div>
			</div>
			@endif
			@endforeach
			@endif
				
			<!-- Third Column -->
			@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
			@foreach(auth()->user()->roleHasContainerPermissions() as $val)
			@if(($val['Type']=='Section') && ($val['Name']=='Order Detail Section - 3') && ($val['section']!='None'))
				<?php
					$oneClass = '';
					if($val['section']=='Show'){
						$oneClass = 'readable';
					}
					if($val['section']=='View'){
						$oneClass = 'readable';
					}

					if($val['section']=='Edit'){
						$oneClass = 'form-control form-control-sm';
					}
				?>
				<div class="col-sm-4 third_cont">
					<div class="first_coloumn">
						{{-- <div class="col_first_content">
							<label>Customer Notes</label>
							<div class="col-sm-10">
								<textarea class="{{ $oneClass }}" @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Customer_Notes_{{$order->ID}}" name="Customer_Notes">{{$order->Customer_Notes}}</textarea>
							</div>
						</div>

						<div class="col_first_content">
							<label>Vendor Notes</label>
							<div class="col-sm-10">
								<textarea class="{{ $oneClass }}"  @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Vendor_Notes_{{$order->ID}}" name="Vendor_Notes">{{$order->Vendor_Notes}}</textarea>
							</div>
						</div> --}}

						<div class="col_first_content">
							<label>Glocom Notes</label>
							<div class="col-sm-10">
								<textarea class="{{ $oneClass }}"  @if(($val['section']=='View') || ($val['section']=='Show')) readonly @endif id="Glocom_Notes_{{$order->ID}}" name="Glocom_Notes">{{$order->Glocom_Notes}}</textarea>
							</div>
						</div>
					</div>
				</div>
				@endif
				@endforeach
				@endif
				
			@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
			@php 
				$button = 'false'; 
			@endphp
			@foreach(auth()->user()->roleHasContainerPermissions() as $key => $val)

			@if(($val['Type']=='Section') && ($val['section']!='None'))
			@if(($val['Name']=='Order Detail Section - 1' && $val['section']=='Edit') || ($val['Name']=='Order Detail Section - 2' && $val['section']=='Edit') || ($val['Name']=='Order Detail Section - 3' && $val['section']=='Edit'))
			
			@php 
				$button = 'true'; 
			@endphp
				
			@endif
			@endif
			@endforeach
			@endif
			
			@if($button=='true')
			<div class="form-group row">
				<div class="col-sm-10">
					<button type="button" class="btn btn-primary" id="orderInfoBut" onclick="updateOrder({{$order->ID}})">Update Order</button>
				</div>
			</div>
			@endif
		</div>
	</div>

@endif
@endforeach
@endif