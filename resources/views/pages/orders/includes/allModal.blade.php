
    <div class="modal-dialog modal-xl" id="modOpen_{{$order->ID}}" fadeInLeft animated ml-auto" style="max-width: 100% !important;">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" style="color: #fff;">Order Detail</h3>
            <button type="button" class="btn-close btn-close-white" data-dismiss="modal" style="color: #fff;" onclick="closeMainModel({{$order->ID}})"></button>
        </div>
        @if(in_array($order->Vendor, $vendorArr) || empty($vendorArr))
        <div class="modal-body">
        <div class="row">
            <div class="col-sm-12 col-form-label col-form-label-sm" style="background: #eee;">
              <ul class="nav nav-tabs" id="myTab_{$order->ID}}" role="tablist_{$order->ID}}">
                <li class="tbcls">{{ $order->Vendor}}</li>
                
                @if(auth()->check() && auth()->user()->hasRole(['admin', 'super_admin', 'business_user']))
                  <li class="tbcls">Glocom Order Ref: {{ $order->Glocom_Order_Ref}}</li>
                  <li class="tbcls">Channel PO: {{ $order->Channel_PO}}</li>
                @endif

                <li class="tbcls">Order Date: {{ $order->Order_Date}}</li>
                <li class="tbcls">Order Status: {{ $order->Order_Status}}</li>
              </ul>
            </div>
            <!-- <div class="col-sm-6 col-form-label col-form-label-sm nav nav-tabs" style="border:4px solid red;padding:5px 5px 5px 5px;" id="OrderNotes{{$order->ID}}"><h6>Order Notes -</h6> {{$order->Notes}}</div> -->
           
            <!-- Tab links -->
            <ul class="nav nav-tabs tabrow" id="myTab_{$order->ID}}" role="tablist_{$order->ID}}">
              
              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)
              @if(($val['Type']=='Tab') && ($val['Name']=='Order Detail') && ($val['section']=='Show'))
                <li class="nav-item" role="presentation_OrderInfo">
                    <button class="nav-link active" id="order-detail-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#order-detail_{{$order->ID}}" type="button" role="tab" aria-controls="order-detail_{{$order->ID}}" aria-selected="true">Order Detail</button>
                </li>
                @endif
                @endforeach
              @endif

              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)
              @if(($val['Type']=='Tab') && ($val['Name']=='Customization') && ($val['section']=='Show'))
              <li class="nav-item" role="presentation_Customization" id="{{$order->ID}}">
                  <button class="nav-link" id="order-customization-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#order-customization_{{$order->ID}}" type="button" role="tab" aria-controls="order-customization_{{$order->ID}}" aria-selected="false" data-value="{{$order->ID}}">Customization</button>
              </li> 
              @endif
              @endforeach
              @endif


              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)

              @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
                  @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
                     <?php $visible = 'display:block'; ?>
                  @else
                     <?php $visible = 'display:none'; ?>               
                  @endif
              @endif

              @if(($val['Type']=='Tab') && ($val['Name']=='Preview') && ($val['section']=='Show'))
                <li class="nav-item" role="presentation_Preview">
                    <button class="nav-link" id="preview-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#preview_{{$order->ID}}" type="button" role="tab" aria-controls="preview_{{$order->ID}}" aria-selected="false" onclick="getPreviw('{{$order->ID}}','PreviewView')">Preview</button>
                </li>
              @endif
              @endforeach
              @endif

              {{--
              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)
              @if(($val['Type']=='Tab') && ($val['Name']=='Business Zone') && ($val['section']=='Show'))
                <li class="nav-item" role="presentation_BusinessZone">
                    <button class="nav-link" id="business-zone-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#business-zone_{{$order->ID}}" type="button" role="tab" aria-controls="business-zone_{{$order->ID}}" aria-selected="false">Business Zone</button>
                </li>
              @endif
              @endforeach
              @endif

              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)
              @if(($val['Type']=='Tab') && ($val['Name']=='Vendor Zone') && ($val['section']=='Show'))
                <li class="nav-item" role="presentation_VendorZone">
                    <button class="nav-link" id="vendor-zone-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#vendor-zone_{{$order->ID}}" type="button" role="tab" aria-controls="vendor-zone_{{$order->ID}}" aria-selected="false">Vendor Zone</button>
                </li>
              @endif
              @endforeach
              @endif
              --}}
              
              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)
              @if(($val['Type']=='Tab') && ($val['Name']=='Gallery') && ($val['section']=='Show'))
                <li class="nav-item" role="presentation_Gallery">
                    <button class="nav-link" id="gallery-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#gallery_{{$order->ID}}" type="button" role="tab" aria-controls="gallery_{{$order->ID}}" aria-selected="false" onclick="getPreviw('{{$order->ID}}','GalleryView')">Gallery</button>
                </li>
              @endif
              @endforeach
              @endif

              @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
              @foreach(auth()->user()->roleHasContainerPermissions() as $val)
              @if(($val['Type']=='Tab') && ($val['Name']=='Customer Zone') && ($val['section']=='Show'))
                <li class="nav-item" role="presentation_CustomerZone">
                    <button class="nav-link" id="customer-zone-tab_{{$order->ID}}" data-bs-toggle="tab" data-bs-target="#customer-zone_{{$order->ID}}" type="button" role="tab" aria-controls="customer-zone_{{$order->ID}}" aria-selected="false">Customer Zone</button>
                </li>
              @endif
              @endforeach
              @endif

              
            </ul>
            </div>
              <div class="tab-content" id="myTabContent_{{$order->ID}}">
                <div class="tab-pane fade show active" id="order-detail_{{$order->ID}}" role="tabpanel" aria-labelledby="order-detail-tab_{{$order->ID}}"> 
                  <br>
                    @include('pages.orders.includes.order_info')
                </div>
                
                <div class="tab-pane fade" id="order-customization_{{$order->ID}}" role="tabpanel" aria-labelledby="order-customization-tab_{{$order->ID}}">
                  <br>
                    @include('pages.orders.includes.order_customization')
                </div> 
              
                <div class="tab-pane fade" id="preview_{{$order->ID}}" role="tabpanel" aria-labelledby="preview-tab_{{$order->ID}}">
                  @include('pages.orders.includes.preview')
                </div>
                {{--
                <div class="tab-pane fade" id="business-zone_{{$order->ID}}" role="tabpanel" aria-labelledby="business-zone-tab_{{$order->ID}}">
                  @include('pages.orders.includes.business_zone')
                </div>

                <div class="tab-pane fade" id="vendor-zone_{{$order->ID}}" role="tabpanel" aria-labelledby="vendor-zone-tab_{{$order->ID}}">
                    @include('pages.orders.includes.vendor_zone')
                </div>
                --}}
                
                <div class="tab-pane fade" id="gallery_{{$order->ID}}" role="tabpanel" aria-labelledby="gallery_{{$order->ID}}">
                    @include('pages.orders.includes.gallery')
                </div>

                <div class="tab-pane fade" id="customer-zone_{{$order->ID}}" role="tabpanel" aria-labelledby="customer-zone-tab_{{$order->ID}}">
                  @include('pages.orders.includes.customer_zone')
                </div>
                
            </div>
        </div>
       
        <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary" style="position: fixed;bottom: 20px;" data-dismiss="modal" onclick="closeMainModel({{$order->ID}})">Close</button> 
        </div>
        @else
           <h3 style="text-align:center;"> {{"Access Denied!!"}} </h3>
        @endif  
    </div>
</div>
