<div id="gallery_{{$order->ID}}" role="tabpanel" aria-labelledby="gallery-tab_{{$order->ID}}">
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/customization-image.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/lightbox.min.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/custom-checkbox.css" />

@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
@foreach(auth()->user()->roleHasContainerPermissions() as $val)
@if(($val['Type']=='Tab') && ($val['Name']=='Gallery') && ($val['section']=='Show'))
        <br/>
        <div class="headTitle">
			<h3 class="fw-bolder m-0" style="color:#000">{{ __('Gallery Image/Video') }}</h3>                
		</div><br/>

		@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
		@foreach(auth()->user()->roleHasContainerPermissions() as $val)
		@if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Upload') && ($val['section']=='Edit'))
	
		<!-- Gallery Image -->
		<form method="POST" enctype="multipart/form-data" class="upload_gallery_image_form" action="javascript:void(0)" >
			<input type="hidden" id="Order_Id" name="Order_Id"  value="{{$order->ID}}">
			<input type="hidden" id="Order_Product_Id" name="Order_Product_Id"  value="@if(isset($order->products[0]->ID)) {{$order->products[0]->ID}} @endif">
			<input type="hidden" name="UserId" value="{{$userId}}" />
			<div class=" container cus_details">
				<div class="form-group row">
					<label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm"><h6>Upload Image/Video </h6></label>
					<div class="col-sm-4">
						<input type="file" class="form-control btn-upload" name="URL" placeholder="Choose image or video" id="URL" accept="image/*, video/*" required/>
					</div>
                    <div class="col-sm-2"> 
                        <button type="submit" class="btn-sm btn-red" id="galPrSub">Submit</button>
					</div>
                   <!--Send Email Start-->
                    <div class="col-sm-2"> 
                        <button type="button" onclick="sendMail('Customer Preview')" class="btn-sm btn-info">Send Mail</button>
                    </div>
                    <!--Send Email End -->

                    <div class="form-group row">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-4">
                           <img id='loadingGal' style="width:100px" src='{{asset('/')}}assets/img/loading.gif' style='visibility: hidden;'>
                        </div>
                        <div class="col-sm-4"> </div>
                    </div>
                   
                    
                    <div class="form-group row" style="margin-bottom: 10px;">
                        <div class="col-sm-4 col-form-label col-form-label-sm">
                            <label for="colFormLabelSm"><h6>Order Gift Message </h6></label>

                            <a href="javascript:void(0);" style="border: 2px solid #ccc;background-color: #ddd!important;" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow giftMess" onclick="displayOrderGiftMessage('{{ $order->ID}}', '{{$order->Gift_Message}}')" id="orderGiftMessag_{{$order->ID}}" data-backdrop="static" data-keyboard="false">
                                <i class="bi bi-pencil-fill fs-7"></i>
                            </a>
                        </div>
                            
                        
                            <textarea class="col-sm-4 col-form-label col-form-label-sm noteBoxDiv" id="ordergiftmessag_{{$order->ID}}" readonly>{{preg_replace('/[-?]/', '', $order->Gift_Message)}}</textarea>
                            
                       
                    </div>

                    <div class="col-sm-4 col-form-label col-form-label-sm"></div>
                    <div class="col-sm-4 col-form-label col-form-label-sm" style="margin: -20px 0 0 4px !important;">
                        <span class="OrdrimpNote" style="font-size: 13px !important;">Important *</span>
                    </div>
                    <div class="col-sm-4"></div>
                   

                    <div class="form-group row">
                        <div class="col-sm-4 col-form-label col-form-label-sm">
                            <label for="colFormLabelSm"><h6>Customer Notes </h6></label>

                            <a href="javascript:void(0);" style="border: 2px solid #ccc;background-color: #ddd!important;" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow customrNotes" onclick="displayOrderCustomerNotes('{{ $order->ID}}', '{{$order->Customer_Notes}}')" id="ordercustomernote_{{$order->ID}}" data-backdrop="static" data-keyboard="false">
                                <i class="bi bi-pencil-fill fs-7"></i>
                            </a>
                        </div>
                            
                        
                            <textarea readonly class="col-sm-4 col-form-label col-form-label-sm" class="col-sm-6 col-form-label col-form-label-sm noteBoxDiv" id="OrderCustomerNote{{$order->ID}}">{{preg_replace('/[-?]/', '', $order->Customer_Notes)}}</textarea>
                    </div>
                    <div class="col-sm-4 col-form-label col-form-label-sm"></div>
                    <div class="col-sm-4 col-form-label col-form-label-sm" style="margin: -10px 0 0 4px !important;">
                        <span class="OrdrimpNote" style="font-size: 13px !important;">Important *</span>
                    </div>
                    <div class="col-sm-4"></div>

	        </div>
             <!-- Order Customer Notes Modal -->
             <div class="modal" id="orderCustomernote_{{$order->ID}}" class="ordCustomod">
				<div class="modal-dialog">
					<div class="modal-content">					
						<!-- Modal Header -->
						<div class="modal-header">
							<h4 class="modal-title" style="color: #fff;">Update Customer Notes</h4>
							<button type="button" class="btn-close-white" onclick="orderCustNotModel({{$order->ID}})">&times;</button>
						</div>
						
						<!-- Modal body -->
						<div class="modal-body">
							<div class="form-group">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Customer Notes:</label>
								<div class="col-sm-9">
									<textarea class="form-control form-control-sm" id="OrderCustomer_Notes_{{$order->ID}}" name="Customer_Notes" style="width: 133%;">{{$order->Customer_Notes}}</textarea>
									<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
								</div>
							</div>
						</div>
						
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-info" onclick="updateOrderCustomerNote('{{$order->ID}}')">Update</button>
						</div>
					</div>
				</div>
			</div>
			<!-- End Updated Customer Notes Modal -->

             <!-- Order Gift Message Modal -->
             <div class="modal" id="orderGiftMessage_{{$order->ID}}" class="ordGiftmod">
				<div class="modal-dialog">
					<div class="modal-content">					
						<!-- Modal Header -->
						<div class="modal-header">
							<h4 class="modal-title" style="color: #fff;">Update Order Gift Message</h4>
							<button type="button" class="btn-close-white" onclick="orderGiftMessModel({{$order->ID}})">&times;</button>
						</div>
						
						<!-- Modal body -->
						<div class="modal-body">
							<div class="form-group">
								<label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Gift Message:</label>
								<div class="col-sm-9">
									<textarea class="form-control form-control-sm" id="OrderGift_Message_{{$order->ID}}" name="Gift_Message" style="width: 133%;">{{$order->Gift_Message}}</textarea>
									<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
								</div>
							</div>
						</div>
						
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-info" onclick="updateOrderGiftMessage('{{$order->ID}}')">Update</button>
						</div>
					</div>
				</div>
			</div>
			<!-- End Updated Customer Notes Modal -->
		</div>
		</form><br>
		@endif
		@endforeach
		@endif


<!--Customer Preview Code Start -->
 
   <h3 id="preApp" @if($order->Preview_Media == 'null') style="display:none;" @endif style="padding: 10px 10px 10px 10px;background-color: lightgray;color:#000;">Customer Preview & Approval</h3>
   <h6 id="preNoteV" @if($order->Preview_Media == 'null') style="display:none;" @endif style="font-weight: 400;color:red;margin-left: 10px;">Use check box to select images &amp; radio button to select main image</h6>
@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
    @foreach(auth()->user()->roleHasContainerPermissions() as $val)
      @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
        
        <form method="POST" enctype="multipart/form-data" class="updateGalleryPreviewContent" action="javascript:void(0)"> 
        <input type="hidden" name="Order_Id" value="{{$order->ID}}">
        <input type="hidden" class="checkedRadio" name="radio" >
        <div class="outer-grid grid-container" id="galleryPreviewGrid_{{$order->ID}}">        
            @if($order->Preview_Media != 'null')               
                @php
                    $previewArr = json_decode($order->Preview_Media, true); 
                @endphp

                @if($previewArr)
                @foreach($previewArr as $key => $preview)
                
                    @if($preview['Media_Type']=='Image')
                    
                            <div class="inner-grid img-wraps gPreviewBox_{{$order->ID.$key}} @if($preview['Customer_Preview']=='Yes') selectCheckBox  @endif @if($preview['Status']=='Approved') selectBox  @endif" id="gPreviewBox_{{$order->ID.$key}}" >

                            <input type="checkbox" class="galPreview" @if($preview['Customer_Preview']=="Yes") checked="checked" @endif name="check[]" id="galPreGrid_{{$order->ID.$key}}" onchange="checkedGalPreview('Checkbox', '{{$order->ID}}', '{{$key}}', '{{$preview['ID']}}')" value="{{$preview['ID']}}" title="Customer Preview">
                            
                            <label class="btn btn-default">
                            <input class="custome-radio" type="radio" @if($preview['Status']=="Approved") checked="checked" @endif name="radio" id="galPreRadio_{{$order->ID.$key}}" onclick="checkedGalPreview('Radio', '{{$order->ID}}', '{{$key}}', '{{$preview['ID']}}')" value="{{$preview['ID']}}" title="Approve">
                            </label>

                            <h6 style="margin: -35px 0px 0px 0px;text-align:center;">{{ $preview['Date'] }}</h6>
                            
                            <a class="example-image-link setImgHeight" href="{{$preview['URL']}}" data-lightbox="example-1" style="width: 100% !important;display: block !important;">
                                <div class="divpic">
                                    <img class="example-image" src="{{$preview['URL']}}" alt="image-1" />
                                </div>
                            </a>
                            <a href="{{$preview['URL']}}" download class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">
                                <i class="bi bi-download"></i>
                            </a>

                            @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
                            <div class="form-group">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label> 

                                    <a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalPreNotes" onclick="displayGalPreviewNote({{$order->ID.$preview['ID']}})" id="gpnote_{{$order->ID.$preview['ID']}}" data-toggle="modal" data-target="#gpnoteModel_{{$order->ID.$preview['ID']}}" data-backdrop="static" data-keyboard="false" style="float: right;">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                    </a>                                

                                    <div class="col-sm-10" id="gpdivnote_{{$order->ID.$preview['ID']}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($preview['Notes'])) style="display:none" @endif>							
                                        <span class="form-control form-control-sm readable" id="gpnoteUp_{{$order->ID.$preview['ID']}}" style="width: 122%;">{{$preview['Notes']}}</span>							
                                    </div>
                            </div> <br><br>
                            @endif
                           
                        </div>
                    @endif

                    @if($preview['Media_Type']=='Video')
                        
                        <div class="inner-grid img-wraps gPreviewBox_{{$order->ID.$key}} @if($preview['Customer_Preview']=='Yes') selectCheckBox  @endif @if($preview['Status']=='Approved') selectBox  @endif" id="gPreviewBox_{{$order->ID.$key}}" >

                            <input type="checkbox" class="galPreview" @if($preview['Customer_Preview']=="Yes") checked="checked" @endif name="check[]" id="galPreGrid_{{$order->ID.$key}}" onchange="checkedGalPreview('Checkbox', '{{$order->ID}}', '{{$key}}', '{{$preview['ID']}}')" value="{{$preview['ID']}}"  title="Customer Preview">

                            <h6 style="margin: 10px 0px 0px 0px;text-align:center;">{{ $preview['Date'] }}</h6>

                            {{--
                                <label class="btn btn-default">
                                <input class="custome-radio" type="radio" @if($preview['Status']=="Approved") checked="checked" @endif name="radio" id="galPreRadio_{{$order->ID.$key}}" onclick="checkedGalPreview('Radio', '{{$order->ID}}', '{{$key}}', '{{$preview['ID']}}')" value="{{$preview['ID']}}" title="Approve">                                
                            </label>
                            --}}
                            
                            <div class="divpic setImgHeight" style="padding-bottom: 10px;">
                                <video width="100%" height="100%" controls class="preview-vid" >
                                    <source class="example-image" src="{{$preview['URL']}}" type="video/mp4">
                                </video>
                            </div>

                            @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
                            <div class="form-group">
                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label> 
     
                                    <a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalPreNotes" onclick="displayGalPreviewNote({{$order->ID.$preview['ID']}})" id="gpnote_{{$order->ID.$preview['ID']}}" data-toggle="modal" data-target="#gpnoteModel_{{$order->ID.$preview['ID']}}" data-backdrop="static" data-keyboard="false" style="float: right;">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                    </a>
                                

                                <div class="col-sm-10" id="gpdivnote_{{$order->ID.$preview['ID']}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($preview['Notes'])) style="display:none" @endif>							
                                    <span class="form-control form-control-sm readable" id="gpnoteUp_{{$order->ID.$preview['ID']}}" style="width: 122%;">{{$preview['Notes']}}</span>							
                                </div>
                            </div><br><br>
                            @endif
                        </div>
                    @endif

                    <!-- Updated Notes Modal -->
					<div class="modal" id="gpnoteModel_{{$order->ID.$preview['ID']}}" class="gpnotmod">
                        
						<div class="modal-dialog">
							<div class="modal-content">					
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title" style="color: #fff;">Update Preview Notes:</h4>
									<button type="button" class="btn-close-white" onclick="gpnotModel({{$order->ID.$preview['ID']}})">&times;</button>
								</div>
								
								<!-- Modal body -->
								<div class="modal-body">
									<div class="form-group">
										<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
										<div class="col-sm-10">
											<textarea class="form-control form-control-sm" id="gpNotes_{{$order->ID.$preview['ID']}}" name="Notes" style="width: 117%;"></textarea>
											<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
										</div>
									</div>
								</div>
								
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn-sm btn-info" onclick="updateGPNotes({{$order->ID}}, {{ $preview['ID'] }})">Update Notes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Updated Notes Modal -->
                @endforeach 
                @endif
            @endif
        </div><br/><br/>
           
         <div id="pbutt_{{$order->ID}}" class="updPrevButt" style="float: right !important;" @if($order->Preview_Media == 'null') style="display: none" @else style="display: block" @endif>
            <div class="col-sm-12" style="float:right;"> 
                <button type="" class="btn-sm btn-info">Update Preview</button>
            </div>
        </div>
       
        </form><br/><br/><hr><br>
    @endif
    @endforeach
    @endif
<!--Customer Preview Code End -->

<!-- Gallery Code Start -->

{{--
<div class=" container cus_details" id="galleryTx">
		@if(!empty($order->Gift_Message) && !empty($order->Gallery))
			<div class="form-group row">
				<div class="col-sm-12">
				<p id="cusTxt" style="text-align: center;background-color:greenyellow;padding:5px 5px 5px 5px"><strong>Gift Message -  {{preg_replace('/[-?]/', '', $order->Gift_Message)}}</strong></p>
				</div>
			</div>
		@endif
	</div><br> 
--}}


   <h3 id="gallryApp" @if($order->Gallery == 'null') style="display:none" @endif style="padding: 10px 10px 10px 10px;background-color: lightgray;color: #000;">Order Gallery</h3>

    @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
    @foreach(auth()->user()->roleHasContainerPermissions() as $val)
    @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']!='None'))
        
    @if(isset($order->Gallery) && !empty($order->Gallery))

        <form method="POST" enctype="multipart/form-data" class="updateGalleryContent" id="checkko_{{$order->ID}}" action="javascript:void(0)"> 
        <input type="hidden" name="Order_Id" id="Ordr_Id"  value="{{$order->ID}}">
         <input type="hidden" class="checBox" name="checkedBoxes" >
        <div class="outer-grid grid-container" id="galleryGrid_{{$order->ID}}">        
            @if(isset($order->Gallery) && !empty($order->Gallery))               
                @php
                    $galleryArr = json_decode($order->Gallery, true); 
                @endphp

                @if($galleryArr)
                @foreach($galleryArr as $key => $gallery)                    
                    @if($gallery['Media_Type']=='Image')
                    
                        <div class="inner-grid img-wraps galleryBox_{{$order->ID.$key}} @if($gallery['Status']=='Approved') selectCheckBox  @endif" id="galleryBox_{{$order->ID.$key}}">  

                        <h6 style="padding: 10px 0px 0px 0px;text-align:center;">{{ $gallery['Date'] }}</h6>

                        @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']=='Edit'))                                              
                            <input type="checkbox" class="myinput large" @if($gallery['Status']=="Approved") checked="checked" @endif name="check[]" id="grid_{{$order->ID.$key}}" onchange="checkedGallery('{{$order->ID}}', '{{$key}}')" value="{{$key}}">
                        @endif
                                                              

                            <a class="example-image-link setImgHeight" href="{{$gallery['URL']}}" data-lightbox="example-1" style="width: 100% !important;display: block !important;">
                                <div class="divpic">
                                    <img id="imgGalPicId_{{$order->ID.$key}}" class="example-image" src="{{$gallery['URL']}}" alt="image-1"  @if(!is_null($gallery['Status']) && $gallery['Status']=="Approved") style="opacity:1" @else style="opacity:0.5" @endif/>
                                </div>
                            </a>

                            <a href="{{$gallery['URL']}}" download class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">
                                <i class="bi bi-download"></i>
                            </a>

                            @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']=='Edit'))
                            <div class="form-group">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label> 

                                    <a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalNotes" onclick="displayGalleryNote({{$order->ID.$gallery['ID']}})" id="gnote_{{$order->ID.$gallery['ID']}}" data-backdrop="static" data-keyboard="false" style="float: right;">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                    </a>
                                

                                <div class="col-sm-10" id="gdivnote_{{$order->ID.$gallery['ID']}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($gallery['Notes'])) style="display:none" @endif>							
                                    <span class="form-control form-control-sm readable" id="gnoteUp_{{$order->ID.$gallery['ID']}}" style="width: 122%;">{{$gallery['Notes']}}</span>							
                                </div>
                            </div><br><br>
                            @endif
                        </div>
                    @endif

                    @if($gallery['Media_Type']=='Video')
                        
                        <div class="inner-grid img-wraps galleryBox_{{$order->ID.$key}} @if($gallery['Status']=='Approved') selectCheckBox  @endif" id="galleryBox_{{$order->ID.$key}}">

                        <h6 style="padding: 10px 0px 0px 0px;text-align:center;">{{ $gallery['Date'] }}</h6>
                        @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']=='Edit'))
                            <input type="checkbox" class="myinput large" @if($gallery['Status']=="Approved") checked="checked" @endif name="check[]" id="grid_{{$order->ID.$key}}" onchange="checkedGallery('{{$order->ID}}', '{{$key}}')" value="{{$key}}">
                        @endif

                            
                               
                            <a class="example-image-link" href="{{$gallery['URL']}}" data-lightbox="example-1" style="width: 100% !important;display: block !important;">
                            <div class="divpic">
                                <video id="imgGalPicId_{{$order->ID.$key}}" width="100%" height="" controls class="gallery-vid"  @if(!is_null($gallery['Status']) && $gallery['Status']=="Approved") style="opacity:1" @else style="opacity:0.5" @endif>
                                    <source class="example-image" src="{{$gallery['URL']}}" type="video/mp4">
                                </video>
                            </div>
                            </a>
                            
                            @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']=='Edit'))
                            <div class="form-group">
                                <label for="colFormLabelSm" class="col-sm-4 col-form-label col-form-label-sm">Notes</label> 

                                    <a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openGalNotes" onclick="displayGalleryNote({{$order->ID.$gallery['ID']}})" id="gnote_{{$order->ID.$gallery['ID']}}" data-backdrop="static" data-keyboard="false" style="float: right;">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                    </a>
                               

                                    <div class="col-sm-10" id="gdivnote_{{$order->ID.$gallery['ID']}}" style="margin-bottom: 10px;margin-top: -7px;" @if(is_null($gallery['Notes'])) style="display:none" @endif>							
                                        <span class="form-control form-control-sm readable" id="gnoteUp_{{$order->ID.$gallery['ID']}}" style="width: 122%;">{{$gallery['Notes']}}</span>							
                                    </div>
                            </div><br><br>
                            @endif
                        </div>
                    @endif

                     <!-- Updated Notes Modal -->
					<div class="modal" id="gnoteModel_{{$order->ID.$gallery['ID']}}" class="gnotmod">
                        
						<div class="modal-dialog">
							<div class="modal-content">					
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title" style="color: #fff;">Update Gallery Notes:</h4>
									<button type="button" class="btn-close-white" onclick="gnotModel({{$order->ID.$gallery['ID']}})">&times;</button>
								</div>
								
								<!-- Modal body -->
								<div class="modal-body">
									<div class="form-group">
										<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
										<div class="col-sm-10">
											<textarea class="form-control form-control-sm" id="gNotes_{{$order->ID.$gallery['ID']}}" name="Notes" style="width: 117%;"></textarea>
											<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
										</div>
									</div>
								</div>
								
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn-sm btn-info" onclick="updateGNotes({{$order->ID}}, {{ $gallery['ID'] }})">Update Notes</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Updated Notes Modal -->
                @endforeach 
                @endif
            @endif
        </div><br />

         
        <div id="gbutt_{{$order->ID}}" class="updGalPrButt" style="float: right !important;" @if($order->Gallery == 'null') style="display: none" @else style="display: block" @endif>
            <div class="col-sm-12" style="float:right;"> 
                <button type="submit" class="btn-sm btn-info">Update Gallery</button>
            </div>
        </div>
        </form>
    @endif
    @endif
    @endforeach
    @endif
<!-- Gallery Code End -->

@endif
@endforeach
@endif
</div>