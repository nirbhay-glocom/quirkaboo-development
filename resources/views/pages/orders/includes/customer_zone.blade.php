
@if(auth()->check() && auth()->user()->hasRole(['admin', 'super_admin', 'business_user']))
    <br/>
    <div class="headTitle">
            <h3 class="fw-bolder m-0" style="color:#000">{{ __('Customer Preview') }}</h3>                
    </div><br/>

    @php 
        $actual_link = 'http://'.$_SERVER['HTTP_HOST'];
    @endphp
    @if(!empty($order->Public_Key) && !empty($order->Token) && !empty($order->Expiry))

    <a class="example-image-link" target="_blank" href="{{ $actual_link."/customer-preview/".$order->Public_Key.'/'.$order->Token }}"  style="width: 100% !important;display: block !important;">
    {{ $actual_link."/customer-preview/".$order->Public_Key.'/'.$order->Token }}
    </a> 
    
    @else
     <h5 style="text-align:center;">Customer URL Not Found.</h5>
    @endif
@endif