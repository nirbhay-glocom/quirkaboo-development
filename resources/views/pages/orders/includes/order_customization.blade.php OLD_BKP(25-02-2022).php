
<style>
	<style>
* {
   box-sizing: border-box;
}
h1 {
   text-align: center;
}
.outer-grid {
   display: flex;
   flex-wrap: wrap;
   padding: 0 4px;
}
.ajax-outer-grid {
   display: flex;
   flex-wrap: wrap;
   padding: 0 4px;
}
.inner-grid {
	flex: 30%;
    max-width: 30%;
    padding: 0px 30px;
    border: 1px solid lightcyan;
	margin: 5px;
}
.inner-grid img {
   margin-top: 8px;
   width: 100%;
   padding: 10px;
}
@media screen and (max-width: 800px) {
   .inner-grid {
      flex: 50%;
      max-width: 50%;
   }
}
@media screen and (max-width: 600px) {
   .inner-grid {
      flex: 100%;
      max-width: 100%;
   }
}

/* Delete Icon Image */
.img-wraps {
    position: relative;
    display: inline-block;
    
    font-size: 0;
}
.img-wraps .closes {
    position: absolute;
    top: 0px;
    right: 0px;
    z-index: 100;
    background-color: red;
    padding: 4px 3px;
     
    color: #fff;
    font-weight: bold;
    cursor: pointer;
    
    text-align: center;
    font-size: 22px;
    line-height: 14px;
    border-radius: 75%;
    border:1px solid red;
}
.img-wraps:hover .closes {
    opacity: 1;
}


.divpic{
   width:100%;
    height:250px;
    display:flex;
    justify-content:center;
    align-items:center;
    overflow:hidden
}
.divpic img{
  flex-shrink:0;
    -webkit-flex-shrink: 0;
    max-width:70%;
    max-height:90%;
}

.downstyle{
	float: right;
	margin: -35px 55px 0 0;
	position: relative;
}

.readable{
	color: #161616;
    font-size: 13px;
    border: 1px solid #ddd;
    background-color: #eee;
    border-radius: 5px;
	padding: 5px;
	text-decoration: none;
}

.headTitle{
	background-color: aquamarine;
    padding: 10px 10px 10px 10px;
}

</style>
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/lightbox.min.css" />
@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
@foreach(auth()->user()->roleHasContainerPermissions() as $val)
@if(($val['Type']=='Tab') && ($val['Name']=='Customization') && ($val['section']=='Show'))

		@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
		@foreach(auth()->user()->roleHasContainerPermissions() as $val)
		@if(($val['Type']=='Section') && ($val['Name']=='Customization Image Upload') && ($val['section']=='Edit'))
	
		<div class="headTitle">
			<h3 class="fw-bolder m-0">{{ __('Customization Image') }}</h3>                
		</div><br/>

		<!-- Customized Image -->
		<form method="POST" enctype="multipart/form-data" class="upload_image_form" action="javascript:void(0)" >
			<input type="hidden" id="Order_Id" name="Order_Id"  value="{{$order->ID}}">
			<input type="hidden" id="Order_Product_Id" name="Order_Product_Id"  value="{{$order->products[0]->ID}}">
			<input type="hidden" name="UserId" value="{{$userId}}" />
			<div class=" container cus_details">
			<div class="form-group row">
				<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Upload Customize Image:</label>
				<div class="col-sm-10">
					<input type="file" class="btn btn-sm btn-info" name="Image_URL" placeholder="Choose image" id="Image_URL" accept="image/*" required/>
					<!-- <span class="text-danger">{{ $errors->first('title') }}</span> -->
					
				</div>
			</div><br>

			<div class="form-group row">
				<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Custome Text:</label>
				<div class="col-sm-10">
					<textarea class="form-control form-control-sm" id="Customize_Text" name="Customize_Text"></textarea>
				</div>
			</div><br>

			<div class="form-group row">
				<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
				<div class="col-sm-10">
					<textarea class="form-control form-control-sm" id="Notes" name="Notes"></textarea>
				</div>
			</div><br>

			<div class="form-group row">
				<div class="col-sm-10">
					<button type="submit" class="btn btn-primary">Upload New Customize Image</button>
				</div>
			</div>
		</form><br><hr>
		@endif
		@endforeach
		@endif

		@if(auth()->check() && auth()->user()->roleHasContainerPermissions())
		@foreach(auth()->user()->roleHasContainerPermissions() as $val)
		@if(($val['Type']=='Section') && ($val['Name']=='Customization Image Boxes') && ($val['section']!='None'))
		<div class="outer-grid grid-container">
			@foreach($order->customizations as $key => $value)
				<div class="inner-grid img-wraps" id="box_{{$value->ID}}"
					{{-- @if($value->Status=='Approved') style="background-color: #90ee90;" @endif 
					@if($value->Status=='Rejected') style="background-color: #FF6666;" @endif> --}}

					{{-- @role(['Super Admin|Admin|Business User'])
					@can('order-update')
					<span class="closes" title="Delete" onclick="deleteCustomizeImage({{$value->ID}})">×</span>
					@endcan
					@endrole --}}

					{{-- @role(['Vendor']) --}}
						<!-- @if($value->Status=='Approved')
							<span title="Approved" style="top: -10px;right: -10px;position: absolute;"><i class="fa fa-check" style="font-size: 17px;color: #fff;border: 1px solid #0B3;border-radius: 50%;background-color: green;padding: 3px 2px 3px 2px;"></i></span>
						@endif -->

						@if($value->Status=='Rejected')						
							<!-- <span class="closes" title="Delete" style="top: -10px !important; right: -10px !important;">×</span> -->
						@endif
					{{-- @endrole --}}
					
						<a class="example-image-link" href="{{$value->Image_URL}}" data-lightbox="example-1">
						<div class="divpic">
							<img class="example-image" src="{{$value->Image_URL}}" alt="image-1" />
						</div>
						</a>
					
					<a href="{{$value->Image_URL}}" download class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle">
						<i class="bi bi-download"></i>
					</a>
					
					<input type="hidden" id="userID" value="{{$userId}}" />
					<!-- <div class="form-group">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Text:</label>

						<div class="col-sm-10">
							<textarea class="form-control form-control-sm" id="Customize_Text_{{$value->ID}}" name="Customize_Text" style="width: 117%;">{{$value->Customize_Text}}</textarea>
						</div>
					</div><br><br> -->

					<div class="form-group">
						<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label> 

						{{-- @role(['Super Admin|Admin|Business User'])
							@can('order-update') --}}
							<!-- <button type="button" class="badge badge-success" id="note_{{$value->ID}}" style="float: right;margin-bottom: 5px;color: #fff;font-weight: 600;" data-toggle="modal" data-target="#noteModel_{{$value->ID}}" data-backdrop="static" data-keyboard="false">
								<i class="fa fa-pencil" aria-hidden="true"></i> 
							</button> -->

							<a href="javascript:void(0);" class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openNotes" onclick="displayNote({{$value->ID}})" id="note_{{$value->ID}}" data-toggle="modal" data-target="#noteModel_{{$value->ID}}" data-backdrop="static" data-keyboard="false" style="float: right;">
									<i class="bi bi-pencil-fill fs-7"></i>
							</a>
							
							{{-- @endcan
						@endrole --}}

						<div class="col-sm-10" id="divnote_{{$value->ID}}" style="margin-bottom: 10px;margin-top: -7px;">
							
							<span class="form-control form-control-sm" id="noteUp_{{$value->ID}}" style="width: 122%;">{{$value->Notes}}</span>
							
						</div>
					</div><br><br>
			
					{{-- @role(['Super Admin|Admin|Business User'])
						@can('order-update') --}}
						<!-- <div class="form-group">
							<div class="col-sm-12">
							<button type="button" class="form-control btn-sm btn-success" onclick="updateContent({{$value->ID}})" style="margin-bottom: 5px;color: #fff;font-weight: 600;"> Update </button>
							</div>
						</div><br> -->

						<!-- @if($value->Status=='Rejected')
						<div class="form-group">
							<div class="col-sm-12">
							<button type="button" class="form-control btn-sm btn-primary" onclick="approvedCustomization({{$value->ID}})" style="margin-bottom: 5px;color: #fff;font-weight: 600;"> Resubmit </button>
							</div>
						</div><br>
						@endif -->
					{{-- 
						@endcan
						@endrole 
					--}}
					
					<!--- View Comment History -->
					<!-- <div class="form-group">
						<div class="col-sm-12">
							<button type="button" id="viewHistory_{{$value->ID}}" class="form-control btn-sm btn-warning" style="margin-bottom: 5px;color: #fff;font-weight: 600;" data-toggle="modal" data-target="#viewCommentHistory_{{$value->ID}}" data-backdrop="static" data-keyboard="false">View Comment History </button>
						</div>
					</div><br> -->

					{{-- @role(['Vendor']) --}}
					<!-- @if($value->Status!='Approved')
						<div class="form-group">
							<div class="col-sm-12">
								<button type="button" id="comment_{{$value->ID}}" class="form-control btn-sm btn-primary" style="margin-bottom: 5px;color: #fff;font-weight: 600;" data-toggle="modal" data-target="#imgModel_{{$value->ID}}" data-backdrop="static" data-keyboard="false">Add Comment </button>			
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<button type="button" id="approved_{{$value->ID}}" class="form-control btn-sm btn-success" onclick="approvedCustomization({{$value->ID}})" style="margin-bottom: 5px;color: #fff;font-weight: 600;"> Approved </button>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<button type="button" id="rejected_{{$value->ID}}" class=" form-control btn-sm btn-danger" onclick="rejectCustomization({{$value->ID}})" style="margin-bottom: 5px;color: #fff;font-weight: 600;"> Rejected </button>
							</div>
						</div>
					@endif -->

					<!-- The Add Comment Modal -->
					<div class="modal" id="imgModel_{{$value->ID}}">
						<div class="modal-dialog">
							<div class="modal-content">					
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title" style="color: #fff;">Add Comment</h4>
									<button type="button" class="btn-close-white" onclick="closeModel({{$value->ID}})">&times;</button>
								</div>
								
								<!-- Modal body -->
								<div class="modal-body">
									<div class="form-group">
										<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Comment:</label>
										<div class="col-sm-10">
											<textarea class="form-control form-control-sm" id="Comments_{{$value->ID}}" name="Comments" style="width: 117%;"></textarea>
										</div>
									</div>
								</div>
								
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="updateComment({{$value->ID}})">Add Comment</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Add Comment Modal -->

					<!-- The Rejection Modal -->
					<div class="modal" id="rejectionModel_{{$value->ID}}">
							<div class="modal-dialog">
							<div class="modal-content">					
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title" style="color: #fff;">Add Rejection Reason</h4>
									<button type="button" class="btn-close-white" onclick="closeRejectModel({{$value->ID}})">&times;</button>
								</div>
								
								<!-- Modal body -->
								<div class="modal-body">
									<div class="form-group">
										<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Reason:</label>
										<div class="col-sm-10">
											<textarea class="form-control form-control-sm" id="Comment_{{$value->ID}}" name="Comments" style="width: 117%;"></textarea>
										</div>
									</div>
								</div>
								
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" onclick="updateRejectionReson({{$value->ID}})">Add Reason</button>
								</div>
							</div>
						</div>
					</div>
					<!-- End Rejection Modal -->
					{{-- @endrole --}}

					<!-- The Add View Comment History Modal -->
					<div class="modal" id="viewCommentHistory_{{$value->ID}}">
						<div class="modal-dialog">
						<div class="modal-content">					
							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title" style="color: #fff;">View Comments History</h4>
								<button type="button" class="btn-close-white" onclick="closeCHModel({{$value->ID}})">&times;</button>
							</div>
							
							<!-- Modal body -->
							<div class="modal-body">
								<div class="form-group">								
									<div class="col-sm-12">
										<?php 
											$existComments = json_decode($value->Comments, TRUE);
											if(!empty($existComments)){
												foreach($existComments as $val){
													$time = '';
													if($val["Commented_By"]=='Vendor'){
														$myclass = "main-div-container chaty";			$char = substr($val["Commented_By"], 0,1);
														$img = '<h6 class="circle-div-right">'.$char.'</h6>';									
														if(isset($val["Date"])){
															$createdAt = date("d-m-Y, H:i", strtotime($val['Date']));
															$time = '<h6 class="time-right" style="float: left !important;">'.$createdAt.'</h6>';
														}
														
													}else{
														$myclass = "main-div-container darker";		
														// $img = '<img src="https://bootdey.com/img/Content/avatar/avatar4.png" alt="Avatar" class="left">';
														$char = substr($val["Commented_By"], 0,1);
														$img = '<h6 class="circle-div-left">'.$char.'</h6>';
														if(isset($val["Date"])){
															$createdAt = date("d-m-Y, H:i", strtotime($val['Date']));
															$time = '<h6 class="time-left" style="margin-left: 52px;">'.$createdAt.'</h6>';
														}
													}

													echo '<div class="'.$myclass.'">';
													echo $img;
														echo '<h6> Commented By: '.$val["Commented_By"].'</h6>';
													echo '<h6> Comment: '.$val["Comments"].'</h6>';
													echo $time;
													echo '</div>';
												
												}
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End View Comment History Modal -->

				<!-- Updated Notes Modal -->
				<div class="modal" id="noteModel_{{$value->ID}}" class="notmod">
					<div class="modal-dialog">
						<div class="modal-content">					
							<!-- Modal Header -->
							<div class="modal-header">
								<h4 class="modal-title" style="color: #fff;">Update Notes:</h4>
								<button type="button" class="btn-close-white" onclick="notModel({{$value->ID}})">&times;</button>
							</div>
							
							<!-- Modal body -->
							<div class="modal-body">
								<div class="form-group">
									<label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes:</label>
									<div class="col-sm-10">
										<textarea class="form-control form-control-sm" id="Notes_{{$value->ID}}" name="Notes" style="width: 117%;"></textarea>
										<span style="color:red;" class="form-control errorcls" style="width: 117%;display:none;">This field is empty.</span>
									</div>
								</div>
							</div>
							
							<!-- Modal footer -->
							<div class="modal-footer">
								<button type="button" class="btn btn-info" onclick="updateNotes({{$value->ID}})">Update Notes</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End Updated Notes Modal -->
			
				</div>
			@endforeach
		</div>
		@endif
		@endforeach
		@endif
		<div class="ajax-outer-grid grid-container"></div>

@endif
@endforeach
@endif