@php
    $breadcrumb = bootstrap()->getBreadcrumb();

    if(!is_null(auth()->user()->Vendors)){
      $vendorArr = explode(",", auth()->user()->Vendors);
    }else{
      $vendorArr = [];
    }
    
@endphp

<x-base-layout>
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/order-modal.css" />
<style>
 .pull-left{float:left!important;}
.pull-right{float:right!important;}
</style>
<div class="card mb-5 mb-xl-10">
    <!--begin::Card header-->
    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details222" aria-expanded="true" aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-5">
                    <h3 class="fw-bolder m-0">{{ __('Orders Listing') }}</h3>                
                </div>

                <div class="card-title m-5">
                @if ( theme()->getOption('layout', 'page-title/breadcrumb') === true && !empty($breadcrumb))
                    @if (theme()->getOption('layout', 'page-title/direction') === 'row')
                        <!--begin::Separator-->
                        <span class="h-20px border-black opacity-75 border-start mx-4"></span>
                        <!--end::Separator-->
                    @endif

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                        @foreach ($breadcrumb as $item)
                            <!--begin::Item-->
                            @if ( $item['active'] === true )
                                <li class="breadcrumb-item text-black opacity-75">
                                    {{ $item['title'] }}
                                </li>
                            @else
                                <li class="breadcrumb-item text-black opacity-75">
                                    @if ( ! empty($item['path']) )
                                        <a href="{{ theme()->getPageUrl($item['path']) }}" class="text-black text-hover-primary">
                                            {{ $item['title'] }}
                                        </a>
                                    @else
                                        {{ $item['title'] }}
                                    @endif
                                </li>
                            @endif
                            <!--end::Item-->
                            @if (next($breadcrumb))
                                <!--begin::Item-->
                                <li class="breadcrumb-item">
                                    <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                                </li>
                                <!--end::Item-->
                            @endif
                        @endforeach
                    </ul>
                    <!--end::Breadcrumb-->
                @endif
                </div>
                <!--end::Card title-->
    </div>
    <!--begin::Card header-->
   
    <!--begin::Content-->
    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
        
        <label id="customSearch" style="position: absolute; padding: 13px;margin-left: 185px; display:none;">
          Custome Search:<br>
        <select class="dataTables_filter form-control-sm" id="search_filters" style="border:1px solid #E4E6EF !important;">
          <option value="">Custome Search Filter</option>
          @if(auth()->check() && auth()->user()->Sales_Channel=='Yes')
          <option value="Sales_Channel">Sales Channel</option>
          @endif
          <option value="Order_Status">Order Status</option>
          <option value="Vendor">Vendor</option>
          <option value="Order_Date">Order Date</option>
          <option value="Delivery_Due_Date">Delivery Due Date</option>        
        </select>
        </label>

        <select class="dataTables_filter form-control-sm" id="salesChannel" style="display:none; position: absolute;margin-left: 385px;margin-top: 32px;">
          <option value="">Sales Channel</option>
          <option value="Amazon">Amazon</option>
          <option value="Quirkaboo">Quirkaboo</option>
          <option value="Shopify">Shopify</option>
          <option value="Wish">Wish</option> 
        </select>

        <select class="dataTables_filter form-control-sm" id="orderStatus" style="display:none; position: absolute;margin-left: 385px;margin-top: 32px;">
          <option value="">Order Status</option>
          <option value="Active">Active</option>
          <option value="Inactive">Inactive</option>
          <option value="Processing">Processing</option>
          <option value="Delivered">Delivered</option> 
        </select>

        <input type="text" class="dataTables_filter form-control-sm" id="vendor" placeholder="Vendor" style="display:none; position: absolute;margin-left: 385px;margin-top: 32px;"/>

      <div id="orderDate" style="display:none;">
        <input type="date" class="form-control-sm date-range-filter" id="min_orderDate" style=" position: absolute;margin-left: 385px;margin-top: 32px;" />

        <input type="date" class=" form-control-sm date-range-filter" id="max_orderDate" style="position: absolute;margin-left: 545px;margin-top: 32px;" />

        <input type="hidden" id="mn" />
        <input type="hidden" id="maxn" />
        
      </div>

        <input type="date" class="form-control-sm" id="deliveryDueDate" placeholder="Delivery Due Date" data-date-format="yyyy-mm-dd" style="display:none; position: absolute;margin-left: 385px;margin-top: 32px;"/>
      
        <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                  <thead style="background: rgb(75, 69, 69);">
                      <tr class="fw-bold fs-6 text-muted">
                          <th class="txt">Order Date</th>                          
                          <th class="txt">Vendor</th> 
                                                   
                          @if(auth()->check() && auth()->user()->Sales_Channel=='Yes')
                          <th class="txt">Sales Channel</th>
                          @endif

                          <th class="txt">Channel PO</th>                         
                          <th class="txt">Glocom Order Ref</th>
                          <th class="txt">Glocom SKU</th>
                          <th class="txt">Warehouse Delivery Due</th>
                          <th class="txt">Vendor SKU</th>
                          <th class="txt">Product Type</th>
                          <th class="txt">Order Status</th>
                      </tr>
                  </thead>
                 
                  <tbody>
                    
                      @if($orders)
                        @foreach($orders as $key => $order)
                        @if(in_array($order->Vendor, $vendorArr) || empty($vendorArr))
                        <tr>
                          <td>{{date('Y-m-d', strtotime($order->Order_Date))}} </td>
                          <td>{{$order->Vendor}}</td> 

                          @if(auth()->check() && auth()->user()->Sales_Channel=='Yes')
                          <td>{{$order->Sales_Channel}}</td>
                          @endif

                          <td>{{$order->Channel_PO}}</td>                          
                          <td><a href="javascript:void(0);" class="glcomOrd" data-id="{{$order->ID}}" data-toggle="modal" data-target="#extraLargeModal_{{$order->ID}}" data-backdrop="static" data-keyboard="false">{{$order->Glocom_Order_Ref}} </a></td>
                          <td>{{$order->Glocom_SKU}}</td>
                          <td>{{$order->Warehouse_Delivery_Due}}</td>
                          <td>{{$order->Vendor_SKU}}</td>
                          <td>@if(isset($order->products[0]->Product_Type))
                                  {{$order->products[0]->Product_Type}}
                              @endif
                          </td>
                          <td>{{$order->Order_Status}}</td>                        
                        </tr>
                        @endif
                        @endforeach
                      @endif
                  </tbody>
            </table>
           
           <div id="myaj"></div>
           @php 
              $url = $_SERVER['REQUEST_URI']; 
              $currentOrderID = explode("=", $url);
          @endphp
          
          @if(isset($currentOrderID[1]) && !empty($currentOrderID[1]))
              @foreach($orders as $order)
                @if($order->ID==$currentOrderID[1])
                  <div id="extraLargeModal_{{$order->ID}}" class="modal fade" tabindex="-1" role="dialog">                          
                      @include('pages.orders.includes.allModal')
                  </div>
                @endif
              @endforeach
          @endif
         
        </div>
    </div>
</div>

<div class="ajxNotes"></div>
<!--<div class="outer-grid grid-container"></div> -->
</x-base-layout>

<script src="{{asset('/')}}assets/js/lightbox-plus-jquery.min.js"></script>
<script src="{{asset('/')}}assets/js/jquery.dataTables.min.js"></script>
<script src="{{asset('/')}}assets/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('/')}}assets/js/bootstrap.min.js"></script>

<script>

$(document).ready(function(){
    $(".glcomOrd").on("click", function(){
        var ID = $(this).attr("data-id");
        const url = new URL(window.location.href);
        url.searchParams.set('Order_ID', ID);  
        window.history.replaceState(null, null, url);
        window.location.reload();
    })
  }); 

  $(document).ready(function () {
    // Hide the div
    $("#customSearch").show();
    // Show the div after 4s
    $("#customSearch").delay(4000).fadeIn(100);  
  });   

  $(function(){
    //set min date...
    $('[type="date"]#min_orderDate').prop('max', function(){
        return new Date().toJSON().split('T')[0];    
    });

    //set max date according to min date...
    $('[type="date"]#min_orderDate').change('max', function(){
        var minDate = this.value;
        $('[type="date"]#max_orderDate').prop('min', function(){
          return new Date(minDate).toJSON().split('T')[0];    
        });
    });
});

  $(document).ready(function(e) {
  
    var url = document.URL;
    var myurl = url.split('=');
    if(myurl[1] != ""){
      $("#extraLargeModal_"+myurl[1]).modal('show');
    }  
  
    var oTable = $("#kt_datatable_example_1").DataTable({
      "order": [[ 0, "desc" ]],
      "pageLength": 25,
      "dom": '<"pull-left"f><"pull-right"l>tip',
      stateSave: true
    });
  
    $("#search_filters").change(function(){
      if(this.value=='Sales_Channel'){
        $("#salesChannel").css("display","block");
        $("#orderStatus").css("display","none");
        $("#vendor").css("display","none");
        $("#orderDate").css("display","none");        
        $("#deliveryDueDate").css("display","none");

        
       // $('[type="search"]').val("");
      }else if(this.value=='Order_Status'){
        $("#orderStatus").css("display","block");
        $("#salesChannel").css("display","none");
        $("#vendor").css("display","none");
        $("#orderDate").css("display","none");        
        $("#deliveryDueDate").css("display","none");

      }else if(this.value=='Vendor'){
        $("#vendor").css("display","block");
        $("#salesChannel").css("display","none");
        $("#orderStatus").css("display","none");        
        $("#orderDate").css("display","none");        
        $("#deliveryDueDate").css("display","none");

      }else if(this.value=='Order_Date'){
        $("#orderDate").css("display","block");        
        $("#salesChannel").css("display","none");
        $("#orderStatus").css("display","none");        
        $("#vendor").css("display","none");
        $("#deliveryDueDate").css("display","none");

      }else if(this.value=='Delivery_Due_Date'){
        $("#deliveryDueDate").css("display","block");
        $("#salesChannel").css("display","none");
        $("#orderStatus").css("display","none");        
        $("#vendor").css("display","none");
        $("#orderDate").css("display","none");        
      }
    });

    $('#salesChannel').change(function(){
      oTable.search($(this).val()).draw() ;
   });

   $('#orderStatus').change(function(){
      oTable.search($(this).val()).draw() ;
   });

    $('#vendor').keyup(function(){
      oTable.search($(this).val()).draw() ;
   });

   $('#min_orderDate').change(function(){
      var miin = $("#mn").val($(this).val());
      var mxn = $("#maxn").val();
      if(miin >= mxn){
        oTable.search(false).draw();
      }
      if($(this).val() <= mxn){
        let combi = convertArrToSting(miin, mxn);
        oTable.search(combi,true,false).draw();
      }
   });

  $('#max_orderDate').change(function() {
    $("#maxn").val($(this).val());
    var min = $("#mn").val();
    var max = $(this).val()

    if(min <= max){
      let comb = convertArrToSting(min, max);
      oTable.search(comb,true,false).draw();
    }
  });

   $('#deliveryDueDate').change(function(){
      oTable.search($(this).val()).draw() ;
   });
});

// function changeURL(ID){ 
//     const url = new URL(window.location.href);
//     url.searchParams.set('Order_ID', ID);  
//     window.history.replaceState(null, null, url);
//     window.location.reload();
    
//     //localStorage.removeItem("currUrlOrderID");
//     //localStorage.setItem('currUrlOrderID', JSON.stringify(ID));
// }

function convertArrToSting(startDate, endDate){
  var listDate = [];
    var dateMove = new Date(startDate);
    var strDate = startDate;

    while (strDate < endDate){
      var strDate = dateMove.toISOString().slice(0,10);
      listDate.push(strDate);
      dateMove.setDate(dateMove.getDate()+1);
    };
    
    var arrayToString = listDate.toString();
    var arr = arrayToString.split(",");
    return arr.join("|");
}
</script>

<!-- Update Order INFO  -->
<script>
	function updateOrder(OrderId){
    if(OrderId != ''){
      $("#orderInfoBut").text("Uploading...");
      var inputs = document.getElementById('orderInfo_'+OrderId).getElementsByTagName('input');
      var selects = document.getElementById('orderInfo_'+OrderId).getElementsByTagName('select');
      var textareas = document.getElementById('orderInfo_'+OrderId).getElementsByTagName('textarea');
      var formData = {}
      for (const input of inputs){
        formData[input.name] = input.value
      }

      for (const select of selects){
        formData[select.name] = select.value
      }

      for (const textarea of textareas){
        formData[textarea.name] = textarea.value
      }
  
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
              type : 'POST',
              url: '{{route('update-order')}}',
              data : formData,
              dataType: "JSON",
              success : function(response)
              {
                $("#orderInfoBut").text("Update Order");
                  const obj = JSON.parse(JSON.stringify(response));
                  if(obj.data){
                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Order Updated Successfully!!',
                      showConfirmButton: true,
                      //timer: 1500
                    });
                  }                   
          
              }
      });
    }else{
      Swal.fire("Something went wrong. Please try again!!");
    }
	}
</script>

<!-- Upload Customize Image -->
<script>
$(document).ready(function (e) {
  $("#Customize_Text").focus();
  $("#Notes").focus();

	$.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
   });
 
   $('.upload_image_form').submit(function(e) {
    e.preventDefault();    
    $("#customizeSub").text("Uploading...");
    document.getElementById("loadingCustom").style = "visibility: visible";

		var formData = new FormData(this);
		$.ajax({
			type:'POST',
			url: "{{ route('upload-image')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: (response) => {
       
        if(response.data){ 
          this.reset();
          var custom_text =  $("#Customize_Text").val();   
          $("#customizeSub").text("Submit");
          document.getElementById("loadingCustom").style = "visibility: hidden";
          
          $("#Customize_Text").text(custom_text);
          $("#cusTxt").text(custom_text);

          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Image has been uploaded successfully!!',
            showConfirmButton: true,
          });

          var notes;
          if(response.data.Notes == null){
            notes = '';
          }else{
            notes = response.data.Notes;
          }

          var url = document.URL;
          var myurl = url.split('=');
          if(myurl[1] != ""){
            var noteHtml = '';
            if(response.data.notes == null){
                noteHtml = '<div class="col-sm-10" id="divnote_'+response.data.ID+'" style="margin-bottom: 10px;margin-top: -7px;display:none;"><span class="form-control form-control-sm readable" id="noteUp_'+response.data.ID+'" style="width: 122%;">'+notes+'</span></div>';
            }else{
              noteHtml = '<div class="col-sm-10" id="divnote_'+response.data.ID+'" style="margin-bottom: 10px;margin-top: -7px;"><span class="form-control form-control-sm readable" id="noteUp_'+response.data.ID+'" style="width: 122%;">'+notes+'</span></div>';
            }

              $("#customizationGrid_"+myurl[1]).prepend('<div class="inner-grid img-wraps" id="box_'+response.data.ID+'"><a class="example-image-link" href="'+response.data.Image_URL+'" data-lightbox="example-1" style="width: 100% !important;display: block !important;"><div class="divpic"><img id="'+response.data.ID+'" src="'+response.data.Image_URL+'"/></div></a><a href="'+response.data.Image_URL+'" download class="btn btn-icon btn-circle w-25px h-25px bg-body shadow downstyle"><i class="bi bi-download"></i></a><div class="form-group"><label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label><label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openNotes"  onclick="displayNote('+response.data.ID+')" id="note_'+response.data.ID+'" style="float: right;"><i class="bi bi-pencil-fill fs-7"></i></label>'+noteHtml+'</div></div>');
          }
        }
      
			},
			error: function(response){
        if(response.status==422){
          $("#customizeSub").text("Submit");
          document.getElementById("loadingCustom").style = "visibility: hidden";
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Invalid image format!!',
            showConfirmButton: true,
          });
        }
				console.log(">>>>",response);
			}
		});
	});

  $(document).on('click','.openNotes', function(){  
    var myid = "#"+this.id; 
    var dd = this.id.split("_");
     
    $(".ajxNotes").html('<div class="modal" id="noteModel_'+dd[1]+'" class="notmod"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" style="color: #fff;">Update Notes</h4><button type="button" class="btn-close-white" onclick="notModel('+dd[1]+')">&times;</button></div><div class="modal-body"><div class="form-group"><label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label><div class="col-sm-10"><textarea class="form-control form-control-sm" id="Notes_'+dd[1]+'" name="Notes" style="width: 117%;"></textarea><span style="color:red;width: 117%;display:none;" class="form-control errorcls"></span></div></div></div><div class="modal-footer"><button type="button" class="btn-sm btn-info" onclick="ajax_updateNotes('+dd[1]+')">Update Notes</button></div></div></div></div>');
    
      $("#noteModel_"+dd[1]).modal('show');
      $("#Notes_"+dd[1]).focus();
  });
});


function displayNote(ID){
  const Notes = $("#noteUp_"+ID).text();
  $("#Notes_"+ID).val(Notes);
  $("#noteModel_"+ID).show();
}

</script>

<!-- Customize Image Delete -->
<script>
  function deleteCustomizeImage(ID){
    Swal.fire({
        title: 'Are you sure you want to delete this image?',
        showDenyButton: true,
        // showCancelButton: true,
        confirmButtonText: 'Delete',
        denyButtonText: `Cancel`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });                
          // confirm then
          $.ajax({
              url: "{{ route('delete-image')}}",
              type: 'POST',
              dataType: 'json',
              data: {ID:ID}
          }).always(function (data) {
            console.log(data.status);
            $(window).trigger('resize');
            if(data.status=='Success'){
              $('#box_'+ID).hide();
            }else if(data.status=='No Record Found'){
              $(".ajax-outer-grid").html("");
              Swal.fire('Something went wrong!', '', 'Error');
            }else{
              $('.ajax-outer-grid').html("");
              Swal.fire('Something went wrong!', '', 'Error');
            }                 
          });
        } else if (result.isDenied) {
          Swal.fire('Your Image is safe!', '', 'info')
        }
    });
  }

  function deleteCustomizeImageData(ID, Order_Id, Order_Product_Id){
     if(ID != "" && Order_Id != "" && Order_Product_Id != ""){
        $.ajax({
                type : 'POST',
                url: '{{ route('get-customized-order-data') }}',
                data : {ID:ID, Order_Id:Order_Id, Order_Product_Id:Order_Product_Id},
                dataType: "JSON",
                success : function(response)
                {

                }
        });
      }else{
        Swal.fire("Something went wrong!!");
      }
    }

  function updateContent(OrderCustomizationID){
    if(OrderCustomizationID != ''){
       var Customize_Text = $("#Customize_Text_"+OrderCustomizationID).val();
       var Notes = $("#Notes_"+OrderCustomizationID).val();
       var UserId = "{{$userId}}";
       $.ajax({
              type : 'POST',
              url: '{{ route('update-content-detail') }}',
              data : {ID:OrderCustomizationID, UserId:UserId, Customize_Text:Customize_Text, Notes:Notes},
              dataType: "JSON",
              success : function(response)
              {
                 //console.log(response);
                  if(response.status=='Updated Successfully'){
                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Data Updated Successfully!!',
                      showConfirmButton: true,
                      //timer: 1500
                    });
                  }else{
                    Swal.fire("Something went wrong!!");
                  }
              }
      });
    }else{
      Swal.fire("Something went wrong!!");
    }
  }


  function updateTextContent(OrderCustomizationID,text){
    var Customize_Text = $("#Customize_Text_"+OrderCustomizationID).val(text);
  }

  function updateNotesContent(OrderCustomizationID,notes){
    var Notes = $("#Notes_"+OrderCustomizationID).val(notes);
  }

//Approved Customization by Vendor...
function approvedCustomization(rowID){
  if(rowID != ""){
    var UserId = "{{$userId}}";
    Swal.fire({
        title: 'Are you sure you want to approve this image?',
        showDenyButton: true,
        // showCancelButton: true,
        confirmButtonText: 'Approve',
        denyButtonText: `Cancel`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });                
          // confirm then
          $.ajax({
              url: "{{ route('approve-customization-image')}}",
              type: 'POST',
              dataType: 'json',
              data: {ID:rowID, UserId:UserId}
          }).always(function (data) {
            //console.log(">>>>Status", data.status);

            if(data.status==="Success"){
              $("#box_"+rowID).css("background-color", "#90ee90");
              //$('#comment_'+rowID).hide();
            }
            
            if(data.status==422){
              Swal.fire('This image already approved by vendor!', '', 'Error');
            }  

          });
        } else if (result.isDenied) {
          Swal.fire('Image Status not changed!', '', 'info')
        }
    });
  }else{
      Swal.fire("Something went wrong!!");
  } 
}

//Reject Customization by Vendor...
function rejectCustomization(rowID){
  if(rowID != ""){
    var UserId = "{{$userId}}";
    Swal.fire({
        title: 'Are you sure you want to reject this image?',
        showDenyButton: true,
        // showCancelButton: true,
        confirmButtonText: 'Reject',
        denyButtonText: `Cancel`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        
        $("#rejectionModel_"+rowID).modal('show'); 

          // $.ajaxSetup({
          //     headers: {
          //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //     }
          // });                
          // // confirm then
          // $.ajax({
          //     url: "{{ route('reject-customization-image')}}",
          //     type: 'POST',
          //     dataType: 'json',
          //     data: {ID:rowID, UserId:UserId}
          // }).always(function (data) {
            
          //   if(data.status==="Success"){
          //     $("#box_"+rowID).css("background-color", "#FF6666");
          //   }
            
          //   if(data.status==422){
          //     Swal.fire('This image already rejected by vendor!', '', 'Error');
          //   }  

          // });
        } else if (result.isDenied) {
          Swal.fire('Your Image is safe!', '', 'info')
        }
    });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

//Add Comment by Vendor...
function updateComment(rowID){
  if(rowID != ""){
    const User_Id = "{{$userId}}";
    const Comments = $("#Comments_"+rowID).val();
    const Commented_By = "{{$role}}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('update-vendor-comment')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Comments:Comments, Commented_By:Commented_By,User_Id:User_Id}
      }).always(function (data) {
          if(data.status==="Success"){
            $("#imgModel_"+rowID).hide();
            Swal.fire('Comment Successfully Updated!', '', 'Success');
          }
        
          if(data.status==422){
            Swal.fire('This image already rejected by vendor!', '', 'Error');
          }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

//close modal...
function closeModel(rowID){
  $("#imgModel_"+rowID).hide();
}

//Add Rejection Reason by Vendor...
function updateRejectionReson(rowID){
  if(rowID != ""){
    const User_Id = "{{$userId}}";
    const Comments = $("#Comment_"+rowID).val();
    const Commented_By = "{{$role}}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('update-rejection-reason')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Comments:Comments, Commented_By:Commented_By,User_Id:User_Id}
      }).always(function (data) {
          if(data.status==="Success"){
            $("#rejectionModel_"+rowID).hide();
            $("#box_"+rowID).css("background-color", "#FF6666");
            Swal.fire('Image Rejected Successfully!', '', 'Success');
          }
        
          if(data.status==422){
            Swal.fire('This image already rejected!', '', 'Error');
          }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

//close modal...
function closeRejectModel(rowID){
  $("#rejectionModel_"+rowID).hide();
}

function closeCHModel(rowID){
   $("#viewCommentHistory_"+rowID).hide();
}

//Update Notes...
function updateNotes(rowID){
  if(rowID != ""){
    const Notes = $("#Notes_"+rowID).val();
    // if($("#Notes_"+rowID).val() == "")
    //  {
    //      $("#Notes_"+rowID).css("border","1px solid red");
    //      $(".errorcls").css("display","block");
    //      return false;
    //  }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('update-notes')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Notes:Notes}
      }).always(function (data) {
          if(data.status==="Success"){
            $("#noteModel_"+rowID).hide();
            $('body').removeClass('modal-open');
            $('body').css('padding-right', '0px');
            $('.modal-backdrop').remove();

            if(Notes != ""){
              $("#divnote_"+rowID).show();
              $("#noteUp_"+rowID).text(Notes);
            }
             
            //Swal.fire('Notes Successfully Updated!');
          }
        
          if(data.status==422){
            Swal.fire('Something went wrong!!');
          }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

//close notes popup screen
function notModel(rowID){
   $("#noteModel_"+rowID).hide();
}

function ajax_updateNotes(rowID){
  if(rowID != ""){
    const Notes = $("#Notes_"+rowID).val();
    if($("#Notes_"+rowID).val() == "")
     {
         $("#Notes_"+rowID).css("border","1px solid red");
         $(".errorcls").css("display","block");
         $(".errorcls").text('This field is empty.');
         return false;
     }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('ajx-update-notes')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Notes:Notes}
      }).always(function (data) {
        
        $(".outer-grid").html("");
        $("#noteModel_"+rowID).hide();
        
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '0px');
        $('.modal-backdrop').remove();

        const obj = JSON.parse(JSON.stringify(data.status));
        var noteHtml = '';
        $.each(obj, function( index, value ) {

            if(value.Notes == null){
                noteHtml = '<div class="col-sm-10" id="divnote_'+value.ID+'" style="margin-bottom: 10px;margin-top: -7px;display:none;"><span class="form-control form-control-sm" id="noteUp_'+value.ID+'" style="width: 122%;">'+value.Notes+'</span></div>';
            }else{
              noteHtml = '<div class="col-sm-10" id="divnote_'+value.ID+'" style="margin-bottom: 10px;margin-top: -7px;"><span class="form-control form-control-sm" id="noteUp_'+value.ID+'" style="width: 122%;">'+value.Notes+'</span></div>';
            }

           //console.log(value.Notes);
           var body = '<div class="inner-grid img-wraps" id="box_'+value.ID+'"><a class="example-image-link" href="'+value.Image_URL+'" data-lightbox="example-1"><div class="divpic"><img id="'+value.ID+'" src="'+value.Image_URL+'"/></div></a><a href="'+value.Image_URL+'" download class="btn btn-icon btn-circle w-25px h-25px bg-body shadow downstyle"><i class="bi bi-download"></i></a><div class="form-group"><label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes </label><label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow openNotes" onclick="displayNote('+value.ID+')" id="note_'+value.ID+'" data-toggle="modal" data-target="#noteModel_'+value.ID+'" data-backdrop="static" data-keyboard="false" style="float: right;"><i class="bi bi-pencil-fill fs-7"></i></label>'+noteHtml+'</div></div>'
           $(".customizationGrid").append(body);

           
        });
          
         if(data.status==422){
            Swal.fire('Something went wrong!!');
         }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

// Upload Preview Form...
$(document).ready(function (e) {

	$.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
   });
 
   //preview image upload...
    $('.upload_preview_form').submit(function(e) {
      e.preventDefault();
      $("#preSubmit").text("Uploading...");
      document.getElementById("loadingPre").style = "visibility: visible";

      //set Permissions
      var visible;
      @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
        @foreach(auth()->user()->roleHasContainerPermissions() as $val)
          @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
              @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
              visible =  "<?php echo 'display:block'; ?>";
              @else
              visible =  "<?php echo 'display:none'; ?>";               
              @endif
          @endif
        @endforeach
      @endif

      var formData = new FormData(this);
      formData.append('visible', visible);
      $.ajax({
        type:'POST',
        url: "{{ route('upload-preview-image')}}",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: (response) => { 
          this.reset();
          $("#preSubmit").text("Submit");
          document.getElementById("loadingPre").style = "visibility: hidden";
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Preview Data has been uploaded successfully!!',
            showConfirmButton: true,
          });

          var url = document.URL;
          var myurl = url.split('=');
          if(myurl[1] != ""){
             $("#pbutt_"+myurl[1]).show();
             $("#previewGrid_"+myurl[1]).html("");
             $("#previewGrid_"+myurl[1]).html(response.data);
             
          }
        },
        error: function(response){
          if(response.status==422){
            $("#preSubmit").text("Submit");
            document.getElementById("loadingPre").style = "visibility: hidden";
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Invalid image/video format!!',
                showConfirmButton: true,
              });
          }
          console.log(response);
        }
      });
    });

    //preview image selet...
    $('.updatePreviewContent').submit(function(e) {
    e.preventDefault();

    //set Permissions
    var visible;
      @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
        @foreach(auth()->user()->roleHasContainerPermissions() as $val)
          @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
              @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
              visible =  "<?php echo 'display:block'; ?>";
              @else
              visible =  "<?php echo 'display:none'; ?>";               
              @endif
          @endif
        @endforeach
      @endif

		var formData = new FormData(this);
    formData.append('visible', visible);

		$.ajax({
			type:'POST',
			url: "{{ route('select-preview-image')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: (response) => { 
				this.reset();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Preview Image selected successfully!!',
          showConfirmButton: true,
        });

        var url = document.URL;
        var myurl = url.split('=');
        if(myurl[1] != ""){
          $("#previewGrid_"+myurl[1]).html("");
          $("#previewGrid_"+myurl[1]).html(response.data);
        }
			},
			error: function(response){
				console.log(response);
			}
		});
	});

  //gallery image upload...
  $('.upload_gallery_image_form').submit(function(e) {
      e.preventDefault();
      $("#galPrSub").text("Uploading...");
      document.getElementById("loadingGal").style = "visibility: visible"; 

      //set Permissions
      var visible;
      @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
        @foreach(auth()->user()->roleHasContainerPermissions() as $val)
          @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']!='None'))
              @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']=='Edit'))
              visible =  "<?php echo 'display:block'; ?>";
              @else
              visible =  "<?php echo 'display:none'; ?>";               
              @endif
          @endif
        @endforeach
      @endif

      var formData = new FormData(this);
      formData.append('visible', visible);
      $.ajax({
        type:'POST',
        url: "{{ route('upload-gallery-image')}}",
        data: formData,
        cache:false,
        contentType: false,
        processData: false,
        success: (response) => { 
          this.reset();
          $("#galPrSub").text("Submit");
          document.getElementById("loadingGal").style = "visibility: hidden";
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Gallery Data has been uploaded successfully!!',
            showConfirmButton: true,
          });

          var url = document.URL;
          var myurl = url.split('=');
          if(myurl[1] != ""){
            $("#gallryApp").show();
            $(".updGalPrButt").show();

            $("#gbutt_"+myurl[1]).show();
            $("#galleryGrid_"+myurl[1]).html("");
            $("#galleryGrid_"+myurl[1]).html(response.data);
          }
        },
        error: function(response){
          if(response.status==422){
            $("#galPrSub").text("Submit");
            document.getElementById("loadingGal").style = "visibility: hidden";
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Invalid image/video format!!',
                showConfirmButton: true,
              });
          }
          console.log(response);
        }
      });
    });

    //gallery image selet...
    $('.updateGalleryContent').submit(function(e) {
    e.preventDefault();  

		 //set Permissions
     var visible;
      @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
        @foreach(auth()->user()->roleHasContainerPermissions() as $val)
          @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']!='None'))
              @if(($val['Type']=='Section') && ($val['Name']=='Gallery Image Boxes') && ($val['section']=='Edit'))
              visible =  "<?php echo 'display:block'; ?>";
              @else
              visible =  "<?php echo 'display:none'; ?>";               
              @endif
          @endif
        @endforeach
      @endif

      var formData = new FormData(this);
      formData.append('visible', visible);

    var checkedBoxes=[];
    $('input[name="check[]"]:checked').each(function () {
      checkedBoxes[checkedBoxes.length] = (this.checked ? $(this).val() : "");
    });
    
    formData.append('checkedBoxes', checkedBoxes);

   
		$.ajax({
			type:'POST',
			url: "{{ route('approve-gallery-image')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: (response) => { 
				this.reset();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Gallery Images selected successfully!!',
          showConfirmButton: true,
        });

        var url = document.URL;
        var myurl = url.split('=');
        if(myurl[1] != ""){
          $("#galleryGrid_"+myurl[1]).html("");
          $("#galleryGrid_"+myurl[1]).html(response.data);
        }
			},
			error: function(response){
				console.log(response);
			}
		});
	});

});

function checkedPreview(OrderId, rId){
  $(".checkedRadio").val(rId);
  $("#"+rId).attr("checked");
  $(".inner-grid").removeClass("selectBox");
  $(".previewBox_"+OrderId+rId).addClass("selectBox");
  $(".checkmark").removeClass("checko");
}

function checkedGallery(OrderId, rId){

  if($("#grid_"+OrderId+rId).is(':checked')){
    $("#grid_"+OrderId+rId).prop('checked',true);
    $(".galleryBox_"+OrderId+rId).addClass("selectCheckBox");

    $("#imgGalPicId_"+OrderId+rId).css("opacity",1);
  }else{
    $("#grid_"+OrderId+rId).prop('checked',false);
    $(".galleryBox_"+OrderId+rId).removeClass("selectCheckBox"); 
     
    $("#imgGalPicId_"+OrderId+rId).css("opacity",0.5);
  }
}

function checkedGallery2(OrderId, rId){

  if($("#grid_"+OrderId+rId).is(':checked')){
    $("#grid_"+OrderId+rId).prop('checked', false);
    $(".galleryBox_"+OrderId+rId).removeClass("selectCheckBox");
  }else{
    $(".galleryBox_"+OrderId+rId).addClass("selectCheckBox");
    $("#grid_"+OrderId+rId).prop('checked', true);
  }
}

//close modal...
function closeMainModel(orderId){
  $("#extraLargeModal_"+orderId).modal('hide');
  $('body').removeClass('modal-open');
  $('body').css('padding-right', '0px');
  $('.modal-backdrop').remove();

  //replace history order grid...
  var currURL = window.location.href;
  window.history.replaceState(null, null, currURL.split('?')[0]);
  
  var table=('#kt_datatable_example_1').Datatable();
  table.draw();
  //window.location.href="{{route("orders.index")}}";
 
}

function sendMail(ReportType){
  if(ReportType != ''){
    var url = document.URL;
    var currentUrl = url.split('?');
    $.ajax({
			type:'POST',
			url: "{{ url('send-qvp-mail')}}",
			dataType: 'JSON',
      data: {ReportType:ReportType, currentUrl:currentUrl[0]},
			success: (response) => { 
			
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Mail Sent Successfully!!',
          showConfirmButton: true,
        });

			},
			error: function(response){
				console.log(response);
			}
		});
  }
}

//Update Preview Notes...
function updatePNotes(Order_Id, rowID){
  if(rowID != "" && Order_Id != ""){
    const Notes = $("#pNotes_"+Order_Id+rowID).val();
    if($("#pNotes_"+Order_Id+rowID).val() == "")
     {
         $("#pNotes_"+Order_Id+rowID).css("border","1px solid red");
         $(".errorcls").css("display","block");
         return false;
     }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('update-preview-notes')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Notes:Notes, Order_Id:Order_Id}
      }).always(function (data) {
          if(data.status==="Success"){
            $("#pnoteModel_"+Order_Id+rowID).hide();
            $('body').removeClass('modal-open');
            $('body').css('padding-right', '0px');
            $('.modal-backdrop').remove();

            if(Notes != ""){
              $("#divnote_"+Order_Id+rowID).show();
              $("#pnoteUp_"+Order_Id+rowID).text(Notes);
            }
            
            //Swal.fire('Notes Successfully Updated!');
          }
        
          if(data.status==422){
            Swal.fire('Something went wrong!!');
          }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

//close notes popup screen
function pnotModel(rowID){
   $("#pnoteModel_"+rowID).hide();
   $('body').removeClass('modal-open');
   $('body').css('padding-right', '0px');
   $('.modal-backdrop').remove();
}

//close notes popup screen
function gpnotModel(rowID){
   $("#gpnoteModel_"+rowID).hide();
   $('body').removeClass('modal-open');
   $('body').css('padding-right', '0px');
   $('.modal-backdrop').remove();
}

function displayPreviewNote(mixIDs){
  const Notes = $("#pnoteUp_"+mixIDs).text();
  $("#pNotes_"+mixIDs).val(Notes);

  $("#pnoteModel_"+mixIDs).show();
}

//Update Gallery Notes...
function updateGNotes(Order_Id, rowID){
  if(rowID != "" && Order_Id != ""){
    const Notes = $("#gNotes_"+Order_Id+rowID).val();
    if($("#gNotes_"+Order_Id+rowID).val() == "")
     {
         $("#gNotes_"+Order_Id+rowID).css("border","1px solid red");
         $(".errorcls").css("display","block");
         return false;
     }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('update-gallery-notes')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Notes:Notes, Order_Id:Order_Id}
      }).always(function (data) {
          if(data.status==="Success"){
            $("#gnoteModel_"+Order_Id+rowID).hide();
            $('body').removeClass('modal-open');
            $('body').css('padding-right', '0px');
            $('.modal-backdrop').remove();

            $("#gdivnote_"+Order_Id+rowID).show();
            $("#gnoteUp_"+Order_Id+rowID).text(Notes);
            //Swal.fire('Notes Successfully Updated!');
          }
        
          if(data.status==422){
            Swal.fire('Something went wrong!!');
          }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}

//close notes popup screen
function gnotModel(rowID){
   $("#gnoteModel_"+rowID).hide();
}

function displayGalleryNote(mixIDs){
  const Notes = $("#gnoteUp_"+mixIDs).text();
  $("#gNotes_"+mixIDs).val(Notes);

  $("#gnoteModel_"+mixIDs).show();
}

//Gallery Preview Section...
    //preview image selet...
    $('.updateGalleryPreviewContent').submit(function(e) {
    e.preventDefault(); 
    
     //set Permissions
     var visible;
      @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
        @foreach(auth()->user()->roleHasContainerPermissions() as $val)
          @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
              @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
              visible =  "<?php echo 'display:block'; ?>";
              @else
              visible =  "<?php echo 'display:none'; ?>";               
              @endif
          @endif
        @endforeach
      @endif

    var formData = new FormData(this);
    formData.append('visible', visible);
		
    var checkedBoxes=[];
    $('input[name="check[]"]:checked').each(function () {
      checkedBoxes[checkedBoxes.length] = (this.checked ? $(this).val() : "");
    });
  
    formData.append('checkedBoxes', checkedBoxes);

		$.ajax({
			type:'POST',
			url: "{{ route('select-customer-preview-image')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: (response) => { 
				this.reset();
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Customer Preview Images selected successfully!!',
          showConfirmButton: true,
        });

        var url = document.URL;
        var myurl = url.split('=');
        if(myurl[1] != ""){
          $("#galleryPreviewGrid_"+myurl[1]).html("");
          $("#galleryPreviewGrid_"+myurl[1]).html(response.data);
        }
			},
			error: function(response){
				console.log(response);
			}
		});
	});

  function checkedGalPreview(type, OrderId, rId, previewID){
    if(type=='Radio'){
      
      // $(".checkedRadio").val(rId);
      // $("#"+rId).attr("checked");
      // $(".inner-grid").removeClass("selectBox");
      // $(".gPreviewBox_"+OrderId+rId).addClass("selectBox");
      // $(".checkmark").removeClass("checko");

      if($("#galPreRadio_"+OrderId+rId).is(':checked')){
        if($(".gPreviewBox_"+OrderId+rId).hasClass("selectBox")){
          //un-checked time...        
          $("#galPreRadio_"+OrderId+rId).prop('checked',false);
          $(".gPreviewBox_"+OrderId+rId).removeClass("selectBox");
          
          $(".checkedRadio").val("");
          

          if($(".gPreviewBox_"+OrderId+rId).hasClass("selectCheckBox")){
            $("#imgPicId_"+OrderId+rId).css("opacity",1);
          }else{
            $("#imgPicId_"+OrderId+rId).css("opacity",0.5);
          }

        }else{
          //checked time...
          $("#galPreRadio_"+OrderId+rId).prop('checked',true);
          $(".inner-grid").removeClass("selectBox");
          $(".gPreviewBox_"+OrderId+rId).addClass("selectBox");

          $(".checkedRadio").val(previewID);
          $("#imgPicId_"+OrderId+rId).css("opacity",1);

         
        }
      }

      //$("#galPreGrid_"+OrderId+rId).prop('checked',false);
      //$(".gPreviewBox_"+OrderId+rId).removeClass("selectCheckBox"); 
    }

    if(type=='Checkbox'){
      if($("#galPreGrid_"+OrderId+rId).is(':checked')){
        $("#galPreGrid_"+OrderId+rId).prop('checked',true);
        $(".gPreviewBox_"+OrderId+rId).addClass("selectCheckBox");
        $("#imgPicId_"+OrderId+rId).css("opacity",1);
        // $(".inner-grid").removeClass("selectBox");
        // $(".gPreviewBox_"+OrderId+rId).removeClass("selectBox");
        // $(".checkmark").removeClass("checko");
      }else{
        $("#galPreGrid_"+OrderId+rId).prop('checked',false);
        $(".gPreviewBox_"+OrderId+rId).removeClass("selectCheckBox"); 
    
        if($(".gPreviewBox_"+OrderId+rId).hasClass("selectBox")){
            $("#imgPicId_"+OrderId+rId).css("opacity",1);
          }else{
            $("#imgPicId_"+OrderId+rId).css("opacity",0.5);
          }
      }
    }
  }

  //Update Customer Preview Notes...
  function displayGalPreviewNote(mixIDs){
    const Notes = $("#gpnoteUp_"+mixIDs).text();
    $("#gpNotes_"+mixIDs).val(Notes);

    $("#gpnoteModel_"+mixIDs).show();
  }
function updateGPNotes(Order_Id, rowID){
  if(rowID != "" && Order_Id != ""){
    const Notes = $("#gpNotes_"+Order_Id+rowID).val();
    if($("#gpNotes_"+Order_Id+rowID).val() == "")
     {
         $("#gpNotes_"+Order_Id+rowID).css("border","1px solid red");
         $(".errorcls").css("display","block");
         return false;
     }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
          url: "{{ route('update-customer-preview-notes')}}",
          type: 'POST',
          dataType: 'JSON',
          data: {ID:rowID, Notes:Notes, Order_Id:Order_Id}
      }).always(function (data) {
          if(data.status==="Success"){
            $("#gpnoteModel_"+Order_Id+rowID).hide();
            $('body').removeClass('modal-open');
            $('body').css('padding-right', '0px');
            $('.modal-backdrop').remove();

            $("#gpdivnote_"+Order_Id+rowID).show();
            $("#gpnoteUp_"+Order_Id+rowID).text(Notes);
          }
        
          if(data.status==422){
            Swal.fire('Something went wrong!!');
          }  

      });
  }else{
      Swal.fire("Something went wrong!!");
  }
}
</script>

<script>
  function displayOrderText(OrderId, text, type){
    $("#orderTextModal_"+OrderId).show();

    if(type=='Custom_Text'){
      $("#cusBod").show();
      $("#notBod").hide();
      $("#headingTxt").text("Update Order Custom Text");
      $("#OrderCustom_Text_"+OrderId).text(text);
    }

    if(type=='Notes'){
      $("#notBod").show();
      $("#cusBod").hide();
      $("#headingTxt").text("Update Order Notes");
      $("#OrderNotes_"+OrderId).text(text);
    }
  }

  //close Order Text popup screen
  function orderTxtModel(rowID){
    $("#orderTextModal_"+rowID).hide();
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0px');
    $('.modal-backdrop').remove();
  }

  

  function updateOrderText(Order_Id){
    if(Order_Id != ""){
        let Custom_Text = $("#OrderCustom_Text_"+Order_Id).val();
        let Notes = $("#OrderNotes_"+Order_Id).val();
        // if($("#gpNotes_"+Order_Id+rowID).val() == "")
        // {
        //     $("#gpNotes_"+Order_Id+rowID).css("border","1px solid red");
        //     $(".errorcls").css("display","block");
        //     return false;
        // }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              url: "{{ route('update-order-texts')}}",
              type: 'POST',
              dataType: 'JSON',
              data: {Custom_Text:Custom_Text, Notes:Notes, Order_Id:Order_Id}
          }).always(function (data) {
              if(data.data==="Success"){

                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Update Successfully!!',
                  showConfirmButton: true,
                });

                $("#orderTextModal_"+Order_Id).hide();
                $("#OrderTexts"+Order_Id).text(Custom_Text);
                $("#custionTxt").html(Custom_Text);
                
                 var notsTxrt = "<h6>Order Notes -</h6>"+Notes;
                $("#OrderNotes"+Order_Id).html(Notes);
                $('body').removeClass('modal-open');
                $('body').css('padding-right', '0px');
                $('.modal-backdrop').remove();
              }
            
              if(data.status==422){
                Swal.fire('Something went wrong!!');
              }  

          });
      }else{
          Swal.fire("Something went wrong!!");
      }
  }


  function displayOrderVendorNotes(OrderId, text){ 
      $("#orderVendornote_"+OrderId).show();
      $("#OrderVendor_Notes_"+OrderId).text(text);
  }

  //close Order Text popup screen
  function orderVendNotModel(rowID){
    $("#orderVendornote_"+rowID).hide();
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0px');
    $('.modal-backdrop').remove();
  }

  function updateOrderVendorNote(Order_Id){
    if(Order_Id != ""){
        let Vendor_Notes = $("#OrderVendor_Notes_"+Order_Id).val();
       
        // if($("#OrderVendor_Notes_"+Order_Id).val() == "")
        // {
        //     $("#OrderVendor_Notes_"+Order_Id).css("border","1px solid red");
        //     $(".errorcls").css("display","block");
        //     return false;
        // }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              url: "{{ route('update-order-vendor-notes')}}",
              type: 'POST',
              dataType: 'JSON',
              data: {Vendor_Notes:Vendor_Notes, Order_Id:Order_Id}
          }).always(function (data) {
              if(data.data==="Success"){

                  Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Vendor Notes Updated Successfully!!',
                    showConfirmButton: true,
                  });

                  $("#orderVendornote_"+Order_Id).hide();
                  //$("#OrderVendorNote"+Order_Id).show();
                  $("#OrderVendorNote"+Order_Id).html(Vendor_Notes);
                  $('body').removeClass('modal-open');
                  $('body').css('padding-right', '0px');
                  $('.modal-backdrop').remove();

                  if(Vendor_Notes == ""){
                    //$("#OrderVendorNote"+Order_Id).hide();
                  }
              }
            
              if(data.status==422){
                Swal.fire('Something went wrong!!');
              }  

          });
      }else{
          Swal.fire("Something went wrong!!");
      }
  }

  function displayOrderCustomerNotes(OrderId, text){ 
      $("#orderCustomernote_"+OrderId).show();
      $("#OrderCustomer_Notes_"+OrderId).text(text);
  }

  //close Order Text popup screen
  function orderCustNotModel(rowID){
    $("#orderCustomernote_"+rowID).hide();
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0px');
    $('.modal-backdrop').remove();
  }
  function updateOrderCustomerNote(Order_Id){
    if(Order_Id != ""){
        let Customer_Notes = $("#OrderCustomer_Notes_"+Order_Id).val();
       
        // if($("#OrderCustomer_Notes_"+Order_Id).val() == "")
        // {
        //     $("#OrderCustomer_Notes_"+Order_Id).css("border","1px solid red");
        //     $(".errorcls").css("display","block");
        //     return false;
        // }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              url: "{{ route('update-order-customer-notes')}}",
              type: 'POST',
              dataType: 'JSON',
              data: {Customer_Notes:Customer_Notes, Order_Id:Order_Id}
          }).always(function (data) {
              if(data.data==="Success"){

                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Customer Notes Updated Successfully!!',
                  showConfirmButton: true,
                });

                $("#orderCustomernote_"+Order_Id).hide();
                //$("#OrderCustomerNote"+Order_Id).show();
                $("#OrderCustomerNote"+Order_Id).html(Customer_Notes);
                $('body').removeClass('modal-open');
                $('body').css('padding-right', '0px');
                $('.modal-backdrop').remove();

                if(Customer_Notes == ""){
                    //$("#OrderCustomerNote"+Order_Id).hide();
                  }
              }
            
              if(data.status==422){
                Swal.fire('Something went wrong!!');
              }  

          });
      }else{
          Swal.fire("Something went wrong!!");
      }
  }

  function displayOrderGiftMessage(OrderId, text){ 
      $("#orderGiftMessage_"+OrderId).show();
      $("#OrderGift_Message_"+OrderId).text(text);
  }

  //close Order Text popup screen
  function orderGiftMessModel(rowID){
    $("#orderGiftMessage_"+rowID).hide();
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0px');
    $('.modal-backdrop').remove();
  }
  function updateOrderGiftMessage(Order_Id){
    if(Order_Id != ""){
        let Gift_Message = $("#OrderGift_Message_"+Order_Id).val();
       
        // if($("#OrderGift_Message_"+Order_Id).val() == "")
        // {
        //     $("#OrderGift_Message_"+Order_Id).css("border","1px solid red");
        //     $(".errorcls").css("display","block");
        //     return false;
        // }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
              url: "{{ route('update-order-gift-message')}}",
              type: 'POST',
              dataType: 'JSON',
              data: {Gift_Message:Gift_Message, Order_Id:Order_Id}
          }).always(function (data) {
              if(data.data==="Success"){

                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Gift Message Updated Successfully!!',
                  showConfirmButton: true,
                });

                $("#orderGiftMessage_"+Order_Id).hide();
                //$("#ordergiftmessag_"+Order_Id).show();
                $("#ordergiftmessag_"+Order_Id).html(Gift_Message);
                $('body').removeClass('modal-open');
                $('body').css('padding-right', '0px');
                $('.modal-backdrop').remove();

                  if(Gift_Message == ""){
                   // $("#ordergiftmessag_"+Order_Id).hide();
                  }
              }
            
              if(data.status==422){
                Swal.fire('Something went wrong!!');
              }  

          });
      }else{
          Swal.fire("Something went wrong!!");
      }
  }

  function getPreviw(Order_Id, viewType){
     if(Order_Id != "" && viewType != ""){
      var visible;
      @if(auth()->check() && auth()->user()->roleHasContainerPermissions())
        @foreach(auth()->user()->roleHasContainerPermissions() as $val)
          @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']!='None'))
          //if(viewType=='PreviewView'){
              @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
                  visible =  "<?php echo 'display:block'; ?>";
              @else
                  visible =  "<?php echo 'display:none'; ?>";               
              @endif
          //}

          // if(viewType=='GalleryView'){
          //     @if(($val['Type']=='Section') && ($val['Name']=='Preview Image Boxes') && ($val['section']=='Edit'))
          //         visible =  "<?php echo 'display:block'; ?>";
          //     @else
          //         visible =  "<?php echo 'display:none'; ?>";               
          //     @endif
          // }

          // if(viewType=='GalleryView'){
          //   $("#pbutt_"+Order_Id).show();
          // }

          

          @endif
        @endforeach
      @endif

      $.ajax({
          type:'POST',
          url: "{{ route('get-order-preview-media')}}",
          data: {Order_Id:Order_Id, viewType:viewType, visible:visible},
          success: (response) => { 
             if(viewType=='GalleryView'){

                if(response.data != null){
                  $("#preApp").show();
                  $("#preNoteV").show();
                  $(".updPrevButt").show();
                }

                $("#galleryPreviewGrid_"+Order_Id).html("");
                $("#galleryPreviewGrid_"+Order_Id).html(response.data);
             }

             if(viewType=='PreviewView'){
                $("#previewGrid_"+Order_Id).html("");
                $("#previewGrid_"+Order_Id).html(response.data);
             }
              
          },
          error: function(response){
            console.log(response);
          }
        });
     }
  }
</script>
