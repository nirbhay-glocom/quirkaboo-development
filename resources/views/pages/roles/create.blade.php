<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">{{ __('Create New Role') }}</h3>
        </div>
        <div class="card-title m-10">
            <a class="btn btn-sm btn-info align-self-center" href="{{ route('roles.index') }}"> Back</a>
         </div>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
        <form method="POST" action="{{route('roles.store')}}">
        @csrf
            <div class="row">
                <div class="row mb-6">
                    <label class="col-lg-2 col-form-label required fw-bold fs-6">Role Name:</label>
                    <div class="col-lg-10 fv-row">  
                    <input type="text" name="name" id="name" class="form-control" required/>
                    </div>
                </div> 
              

                <div class="row mb-6">
                <label class="col-lg-2 col-form-label required fw-bold fs-6">Group Name:</label>
                <div class="col-lg-10 fv-row"> 
                <select name="user_group_id" class="form-control" required>
                    <option value="">Select User Group</option>
                    @foreach($user_groups as $value)
                        <option value="{{$value->ID}}">{{$value->Name}}</option>            
                    @endforeach
                </select>
                 </div>
                </div>

                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn-sm btn-info">
                        <span class="indicator-label">
                            Save 
                        </span>
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</x-base-layout>
