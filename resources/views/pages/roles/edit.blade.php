<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
        <div class="card-title m-3">
            <h3 class="fw-bolder m-0">{{ __('Edit Role') }}</h3>
        </div>
        <div class="card-title m-10">
            <a class="btn btn-sm btn-info align-self-center" href="{{ route('roles.index') }}"> Back</a>
         </div>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
        {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
        <div class="row">
                <div class="row mb-6">
                    <label class="col-lg-2 col-form-label required fw-bold fs-6">Role Name:</label>
                    <div class="col-lg-10 fv-row"> 
                        {!! Form::text('name', null, array('placeholder' => 'Role Name','class' => 'form-control form-control-lg form-control-solid','required')) !!}
                    </div>
                </div>

                <div class="row mb-6">
                <label class="col-lg-2 col-form-label required fw-bold fs-6">Group Name:</label>
                <div class="col-lg-10 fv-row"> 
                <select name="user_group_id" class="form-control" required>
                    <option value="">Select User Group</option>
                    @foreach($user_groups as $value)
                        <option @if($value->ID==$role->user_group_id) {{"selected"}} @endif value="{{$value->ID}}">{{$value->Name}}</option>            
                    @endforeach
                </select>
                 </div>
                </div>
                <hr/>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn-sm btn-info">
                        <span class="indicator-label">
                            Save 
                        </span>
                    </button>
                </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
</x-base-layout>
