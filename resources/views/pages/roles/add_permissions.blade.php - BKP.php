<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">{{ __('Add Role Permission') }}</h3>
        </div>
        <div class="card-title m-10">
            <a class="btn btn-sm btn-info align-self-center" href="{{ route('roles.index') }}"> Back</a>
         </div>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
        <div id="AddPermission">
        <form id="permissionFormAdd" method="POST" action="{{url('add-role-permissions')}}">
        @csrf
        <div class="row">
          <div class="card-body">             
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <h4 style="text-align:center;">{{ $message }}</h4>
                </div>
            @endif
            <h5 >{{ __('Manage Page Permissions:') }}</h5>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #eee !important;border-bottom: 1px solid #ddd !important;padding-left: 10px !important;">Pages</th>
                        <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">Action</th> 
                    </tr>
                </thead>
                <tbody>
                    @if($pagesArr)
                    @foreach($pagesArr as $key => $page) 
                        <?php 
                            $pageNameArr[$page['Name']] = $page['Visibility'];
                            ?>
                        <input type="hidden" name="Role_Id" value="{{$role->id}}">
                        @if($page['Name']=='Manage Orders')
                        <input type="hidden" name="pageExists" value="{{$page['Role_Permission_RowId']}}" />
                        <input type="hidden" class="OrderStatus" name="OrderStatus" value="{{$page['Visibility']}}" />
                        @endif
                            <tr>
                                <th scope="row" style="font-weight: 600 !important;background-color: #eee !important;padding-left: 10px !important;">
                                    {{ ucfirst($page['Name']) }} </th>

                                <td>
                                    <?php
                                        if(!empty($page['Visibility'])){
                                            $type = "Edit";
                                        }else{
                                            $type = "Add";
                                        }
                                    ?>
                                    <label>
                                        <input type="radio" class="pagevisibility" @if($page['Role_Permission_RowId']==0) name="PageVisibility[{{ $page['ID'] }}]" @endif @if($page['Role_Permission_RowId'] > 0) name="PageVisibility[{{ $page['Role_Permission_RowId'] }}]" @endif
                                                value="1" @if($page['Visibility']=='1') {{"checked"}} @endif onchange="getPageValue(this.value, '<?php echo $page['Name']; ?>');" /> Show </label>
                                    <label>
                                        <input type="radio" class="pagevisibility" @if($page['Role_Permission_RowId']==0) name="PageVisibility[{{ $page['ID'] }}]" @endif @if($page['Role_Permission_RowId'] > 0) name="PageVisibility[{{ $page['Role_Permission_RowId'] }}]" @endif 
                                                value="0" @if($page['Visibility']==0) {{"checked"}} @endif onchange="getPageValue(this.value,'<?php echo $page['Name']; ?>');" @if($type=='Add') {{"checked" }} @endif /> Hide </label>
                                </td>
                            </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center Addble" id="containerPermissionAdd" style="display:none;">

        <div class="col-md-12">
            <div class="card">
                
                    <div class="card-body">
                    <h5>{{ __('Manage Container Permissions') }}</h5>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #eee !important;border-bottom: 1px solid #ddd !important;padding-left:10px !important;">UI Element Name</th>
                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">None</th>
                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">View</th>
                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">View&Edit</th>
                                    
                                </tr>
                            </thead>

                             <tbody>
                                @if($container_permissions)
                                @foreach($container_permissions as $key1 => $tab)

                                @php 
                                    $tabIDs[] = $tab['ID'];
                                @endphp
                                <tr>
                                    <th scope="row" style="font-size: 14px !important;font-weight: 600 !important;background-color: #eee !important;padding-left:10px !important;">{{ ucfirst($tab['Name']) }}</th>

                                    <td>
                                       <input type="radio" class="Add VisibilityTab" id="Visibility_{{ $tab['ID'] }}" name="Visibility[{{ $tab['ID'] }}]" onclick="getVisibility(this.id, this.value, {{ $tab['ID'] }}, 'tab');" 
                                                               @if($tab['Visibility']==1) {{"checked"}} value="1" @else value="0" @endif /> 
                                    </td>                                    
                                </tr>

                                    @if(isset($tab['childs']) && !empty($tab['childs']))
                                        @foreach($tab['childs'] as $key2 => $section)
                                            <tr class="sec">
                                                <th scope="row" style="font-weight: 600 !important;background-color: #F5F5F5 !important;padding-left: 50px !important;">{{ ucfirst($section['Name']) }}</th>

                                                <td>
                                                    <input type="radio" class="Visibility_{{$tab['ID'] }}" id="Visibility_{{ $section['ID'] }}" name="Visibility[{{ $section['ID'] }}]" onclick="getVisible(this.id, {{ $section['ID'] }}, 0);"
                                                               @if($section['Visibility']==1) {{"checked"}} value="1" @else value="0" @endif @if(($tab['Visibility']==0) || is_null($tab['Visibility'])) disabled @endif />
                                                </td>

                                                
                                                <td>
                                                  @if($section['Type'] !='Tab')  
                                                   <input type="radio" class="View_{{$tab['ID'] }} SectionView_{{$section['ID']}}" id="View_{{ $section['ID'] }}" name="View[{{ $section['ID'] }}]" 
                                                               @if($section['View']==1) {{"checked"}} value="1" @else value="0" @endif  onclick="getView(this.id, {{ $section['ID'] }}, 'View_And_Edit');" @if(is_null($section['View'])) disabled @endif @if(($tab['Visibility']==0) || is_null($tab['Visibility'])) disabled @endif /> 
                                                  @endif
                                                </td>
                                                
                                                <td>
                                                  @if($section['Type'] !='Tab')
                                                   <input type="radio" class="View_And_Edit_{{$tab['ID'] }} SectionEdit_{{$section['ID']}}" id="View_And_Edit_{{ $section['ID'] }}" name="View_And_Edit[{{ $section['ID'] }}]" 
                                                                @if($section['View_And_Edit']==1) {{"checked"}} value="1" @else value="0" @endif onclick="getEdit(this.id, {{ $section['ID'] }}, 'View');" @if(is_null($section['View_And_Edit'])) disabled @endif  @if(($tab['Visibility']==0) || is_null($tab['Visibility'])) disabled @endif/>
                                                  @endif
                                                </td>
                                            </tr>
                                        @endforeach  
                                    @endif
                                        
                                @endforeach
                                @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

            
        <div class="card-footer d-flex justify-content-end py-6 px-9">
            <button type="submit" class="btn-sm btn-info">
                <span class="indicator-label">
                    Save 
                </span>
            </button>
        </div>
        </div>
        </form>
            </div>
        </div>
    </div>
</div>
</x-base-layout>
<script type="text/javascript">
   $(document).ready(function(){
        var tempArray = JSON.parse('<?php echo json_encode($pageNameArr); ?>');
        if(tempArray['Manage Orders']==1){          
            $("#containerPermissionAdd").css("display","block");
        }
   });

    function getPageValue(value, pagename){

        if(pagename=='Manage Orders'){ 
            if(value==1){
               $(".OrderStatus").val("1");
               $("#containerPermissionAdd").css("display","block");
            }else{
               $(".OrderStatus").val("0");
               $("#containerPermissionAdd").css("display","none");
            }
        }
    }
</script>


<script type="text/javascript">
    
     function getVisibility(fullId, value, inputID, type){
       
        if(type=='tab'){

           if(value==1){
                $("#"+fullId).prop('checked', false);
                $("#"+fullId).val("0");

                 $(".Visibility_"+inputID).prop('disabled', true);
                 $(".Visibility_"+inputID).prop('checked', false);
                 $(".Visibility_"+inputID).val("0");

                $(".View_"+inputID).prop('disabled', true);
                $(".View_"+inputID).prop('checked', false);
                $(".View_"+inputID).val("0");

                $(".View_And_Edit_"+inputID).prop('disabled', true);
            }

            if(value==0){ 
                $("#"+fullId).prop('checked', true);
                $("#"+fullId).val("1");

                $(".Visibility_"+inputID).prop('disabled', false);
                $(".Visibility_"+inputID).prop('checked', true);
                $(".Visibility_"+inputID).val("1");

                $(".View_"+inputID).prop('disabled', false);
                //$(".View_"+inputID).prop('checked', true);
                //$(".View_"+inputID).val("1");

                $(".View_And_Edit_"+inputID).prop('disabled', false);
            }    
        }


        if(type=='section'){
       
            if(value==1){
             
                $(".SectionVisible_"+inputID).prop('disabled', false);
                $(".SectionVisible_"+inputID).val("1");

                $(".SectionView_"+inputID).prop('disabled', false);
                $(".SectionView_"+inputID).prop('checked', true);
                $(".SectionView_"+inputID).val("0");

                $(".SectionEdit_"+inputID).prop('disabled', false);
            }

            if(value==0){
                $(".SectionVisible_"+inputID).prop('disabled', true);
                $(".SectionVisible_"+inputID).val("0");

                $(".SectionView_"+inputID).prop('disabled', true);
                $(".SectionView_"+inputID).prop('checked', false);
                $(".SectionView_"+inputID).val("0");

                $(".SectionEdit_"+inputID).prop('disabled', true);
            }

            if(value==''){
                $(".SectionVisible_"+inputID).prop('disabled', true);
                $(".SectionVisible_"+inputID).val("");

                $(".SectionView_"+inputID).prop('disabled', true);
                $(".SectionView_"+inputID).prop('checked', false);
                $(".SectionView_"+inputID).val("0");

                $(".SectionEdit_"+inputID).prop('disabled', true);
            }
        }
    }

    function getVisible(inputID, permissionID, opType){
          var radioButtonVal = $("#"+inputID).val();
          if(radioButtonVal==1){ 
            $("#"+inputID).prop('checked', false);
            $("#"+inputID).val("0");
          }else{
            $("#"+inputID).prop('checked', true);
            $("#"+inputID).val("1");

            $("#View_"+permissionID).prop('checked', false);
            $("#View_"+permissionID).val("0");

            $("#View_And_Edit_"+permissionID).prop('checked', false);
            $("#View_And_Edit_"+permissionID).val("0");
          }

          
    }

     function getView(inputID, permissionID, opType){
        var radioButtonVal = $("#"+inputID).val();

        if(radioButtonVal==1){            
            $("#"+inputID).prop('checked', false);
            $("#"+inputID).val("0");
        }else{
            $("#"+inputID).prop('checked', true);
            $("#"+inputID).val("1");

            $("#"+opType+"_"+permissionID).prop('checked', false);
            $("#"+opType+"_"+permissionID).val("0");

            $("#Visibility_"+permissionID).prop('checked', false);
            $("#Visibility_"+permissionID).val("0");
        }
    }

     function getEdit(inputID, permissionID, opType){
        var radioButtonVal = $("#"+inputID).val();

        if(radioButtonVal==1){            
            $("#"+inputID).prop('checked', false);
            $("#"+inputID).val("0");
        }else{

            $("#"+inputID).prop('checked', true);
            $("#"+inputID).val("1");

            $("#"+opType+"_"+permissionID).prop('checked', false);
            $("#"+opType+"_"+permissionID).val("0");

            $("#Visibility_"+permissionID).prop('checked', false);
            $("#Visibility_"+permissionID).val("0");
        }
    }

</script>