<div id="extraLargeModal_{{$role->id}}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="max-width: 99.5% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" style="color: #000;">Manage Permissions For - {{ ucfirst($role->user_group->Name) }}</h3>
                <button type="button" class="btn-close" data-dismiss="modal" style="color: #000;"></button>
            </div>
            <div class="modal-body">
                <div class="card mb-5 mb-xl-10">
                    

                    <div id="kt_account_profile_details" class="collapse show">
                        <div class="card-body border-top p-12">
                        <div id="UpdatePermission">
                        <form id="permissionFormEdit" method="POST" action="{{url('update-role-permissions')}}">
                        @csrf
                        <div class="row">
                    
                        <div class="card-body">
                       
                            <h5 >{{ __('Manage Page Permissions:') }}</h5>
                            <table class="table table-striped table-row-bordered gy-5 gs-7">
                                <thead>
                                    <tr>
                                        <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #eee !important;border-bottom: 1px solid #ddd !important;padding-left: 10px !important;">Pages</th>
                                        <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">Action</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($role->pagesArr)
                                    @foreach($role->pagesArr as $key => $page) 
                                        <?php if($page['Name']=='Manage Orders'){  ?>
                                            <input type="hidden" id="manageOrder_{{$role->id}}" value="{{ $page['Visibility'] }}">
                                        <?php } ?>
                                        
                                        <input type="hidden" name="Role_Id" value="{{$role->id}}">
                                        @if($page['Name']=='Manage Orders')
                                        <input type="hidden" name="pageExists" value="{{$page['Role_Permission_RowId']}}" />
                                        <input type="hidden" class="OrderStatus" name="OrderStatus" value="{{$page['Visibility']}}" />
                                        @endif
                                        <tr>
                                            <th scope="row" style="font-weight: 600 !important;background-color: #eee !important;padding-left: 10px !important;">
                                                {{ ucfirst($page['Name']) }} </th>

                                            <td>
                                                <?php
                                                    if(!empty($page['Visibility'])){
                                                        $type = "Edit";
                                                    }else{
                                                        $type = "Add";
                                                    }
                                                ?>
                                                <label>
                                                    <input type="radio" class="pagevisibility" name="PageVisibility[{{ $page['Role_Permission_RowId'] }}]" 
                                                            value="1" @if($page['Visibility']==1) {{"checked"}} @endif onchange="getPageValue(this.value, '<?php echo $page['Name']; ?>', {{ $role->id }});" /> Show </label>
                                                <label>
                                                    <input type="radio" class="pagevisibility" name="PageVisibility[{{ $page['Role_Permission_RowId'] }}]" 
                                                            value="0" @if($page['Visibility']=='0') {{"checked"}} @endif onchange="getPageValue(this.value, '<?php echo $page['Name']; ?>', {{ $role->id }});" @if($type=='Add') {{"checked" }} @endif /> Hide </label>
                                            </td>
                                        </tr>
                                            
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="row justify-content-center Editable" id="containerPermissionEdit_{{ $role->id }}" style="display:none;">

                        <div class="col-md-12">
                            <div class="card">
                                
                                    <div class="card-body">
                                    <h5>{{ __('Manage Container Permissions:') }}</h5>
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                        
                                        <table class="table table-striped table-row-bordered gy-5 gs-7">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #eee !important;border-bottom: 1px solid #ddd !important;padding-left:10px !important;">UI Element Name</th>
                                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">None</th>
                                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">View</th>
                                                    <th scope="col" style="font-size: 15px !important;font-weight: 600 !important;background-color: #ddd !important;">View&Edit</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if($role->container_permissions)
                                                @foreach($role->container_permissions as $key1 => $tab)
                                                <tr>
                                                    <th scope="row" style="font-size: 15px !important;font-weight: 600 !important;background-color: #eee !important;padding-left:10px !important;">{{ ucfirst($tab['Name']) }}</th>

                                                    <td>
                                                    <input type="radio" class="Edit VisibilityTab" id="Visibility_{{ $tab['Role_Permission_RowId'].$role->id }}" name="Visibility[{{ $tab['Role_Permission_RowId'] }}]" onclick="getVisibility(this.id, this.value, {{ $tab['Role_Permission_RowId'].$role->id }}, 'tab');" 
                                                            @if($tab['Visibility']=='1') {{"checked"}} value="1" @else value="0" @endif /> 
                                                    </td>                                    
                                                </tr>

                                                @if(isset($tab['childs']) && !empty($tab['childs']))
                                                    @foreach($tab['childs'] as $key2 => $section)
                                                        <?php //echo "<pre>";print_r($section); ?>
                                                        <tr class="sec">
                                                            <th scope="row" style="font-weight: 600 !important;background-color: #F5F5F5 !important;padding-left: 50px !important;">{{ ucfirst($section['Name']) }}</th>

                                                            <td>
                                                                <input type="radio" class="Visibility_{{$tab['Role_Permission_RowId'].$role->id }}" id="Visibility_{{ $section['Role_Permission_RowId'].$role->id }}" name="Visibility[{{ $section['Role_Permission_RowId'] }}]" onclick="getVisible(this.id, {{ $section['Role_Permission_RowId'].$role->id }}, 'Visibility');"
                                                                    @if($section['Visibility']=='1') {{"checked"}} value="1" @else value="0" @endif @if(($tab['Visibility']=='0') || is_null($tab['Visibility'])) disabled @endif />
                                                            </td>

                                                            
                                                            <td>
                                                                @if($section['Type'] !='Tab') 
                                                                @if( ($section['Name'] != 'Customization Image Upload') && ($section['Name'] != 'Preview Image Upload') && ($section['Name'] != 'Gallery Image Upload') )  
                                                                <input type="radio" class="View_{{ $tab['Role_Permission_RowId'].$role->id }} SectionView_{{$section['Role_Permission_RowId']}}" id="View_{{ $section['Role_Permission_RowId'].$role->id }}" name="View[{{ $section['Role_Permission_RowId'] }}]" 
                                                                            @if($section['View']=='1') {{"checked"}} value="1" @else value="0" @endif  onclick="getView(this.id, {{ $section['Role_Permission_RowId'].$role->id }}, 'View_And_Edit');" @if(($tab['Visibility']=='0') || is_null($tab['Visibility'])) disabled @endif /> 
                                                                @endif
                                                                @endif
                                                            </td>
                                                            
                                                            <td>
                                                                @if($section['Type'] !='Tab')
                                                                
                                                                <input type="radio" class="View_And_Edit_{{ $tab['Role_Permission_RowId'].$role->id }} SectionEdit_{{$section['Role_Permission_RowId']}}" id="View_And_Edit_{{ $section['Role_Permission_RowId'].$role->id }}" name="View_And_Edit[{{ $section['Role_Permission_RowId'] }}]" 
                                                                            @if($section['View_And_Edit']=='1') {{"checked"}} value="1" @else value="0" @endif onclick="getEdit(this.id, {{ $section['Role_Permission_RowId'].$role->id }}, 'View');"  @if(($tab['Visibility']=='0') || is_null($tab['Visibility'])) disabled @endif/>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach  
                                                @endif
                                                        
                                                @endforeach
                                                @endif
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                            
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="submit" class="btn-sm btn-info">
                                <span class="indicator-label">
                                    Update 
                                </span>
                            </button>
                        </div>
                        </div>
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" >Close</button> 
            </div>  
        </div>
    </div>
</div>
                                    

