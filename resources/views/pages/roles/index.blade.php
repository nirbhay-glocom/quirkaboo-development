@php
    $breadcrumb = bootstrap()->getBreadcrumb();
@endphp
<x-base-layout>

<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
        <div class="card-title m-5">
            <h3 class="fw-bolder m-0">{{ __('Role Management') }}</h3>
        </div>
        <div class="card-title m-5">
                @if ( theme()->getOption('layout', 'page-title/breadcrumb') === true && !empty($breadcrumb))
                    @if (theme()->getOption('layout', 'page-title/direction') === 'row')
                        <!--begin::Separator-->
                        <span class="h-20px border-black opacity-75 border-start mx-4"></span>
                        <!--end::Separator-->
                    @endif

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                        @foreach ($breadcrumb as $item)
                            <!--begin::Item-->
                            @if ( $item['active'] === true )
                                <li class="breadcrumb-item text-black opacity-75">
                                    {{ $item['title'] }}
                                </li>
                            @else
                                <li class="breadcrumb-item text-black opacity-75">
                                    @if ( ! empty($item['path']) )
                                        <a href="{{ theme()->getPageUrl($item['path']) }}" class="text-black text-hover-primary">
                                            {{ $item['title'] }}
                                        </a>
                                    @else
                                        {{ $item['title'] }}
                                    @endif
                                </li>
                            @endif
                            <!--end::Item-->
                            @if (next($breadcrumb))
                                <!--begin::Item-->
                                <li class="breadcrumb-item">
                                    <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                                </li>
                                <!--end::Item-->
                            @endif
                        @endforeach
                    </ul>
                    <!--end::Breadcrumb-->
                @endif
        </div>

      
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success" style="margin-left: 40px !important;margin-right: 40px!important;">
            <h4 style="text-align:center;">{{ $message }}</h4>
        </div>
    @endif

    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
          {{--  @can('role-create') --}}
           <a class="btn btn-sm btn-info" href="{{ route('roles.create') }}" style="float:right;margin:-38px 0px"> Create New Role</a>
           {{-- @endcan --}}
        <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                  <thead style="background: rgb(75, 69, 69);">
                      <tr class="fw-bold fs-6 text-muted">
                          <th class="txt">No</th>
                          <th class="txt">User Group</th>
                          <th class="txt">Role </th>
                          <th class="txt">Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                   
                      @if($roles)
                      @foreach ($roles as $key => $role)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>@if($role->user_group){{ $role->user_group->Name }} @endif</td>
                            <td>{{ $role->name }} </td>
                            <td>
                                <a class="badge badge-primary" href="{{ route('roles.edit',$role->id) }}">Manage Role</a>
                               
                                @if($role->pageRoute=='Add')    
                                    <a href="javascript:void(0);" class="badge badge-info"  onclick="changeURL({{$role->id}})" data-toggle="modal" data-target="#extraLargeModal_{{$role->id}}" data-backdrop="static" data-keyboard="false"> Manage Permissions </a>
                                
                                    @include('pages.roles.add_permissions')
                                @endif

                                @if($role->pageRoute=='Edit')    
                                    <a href="javascript:void(0);" class="badge badge-info"  onclick="changeURL({{$role->id}})" data-toggle="modal" data-target="#extraLargeModal_{{$role->id}}" data-backdrop="static" data-keyboard="false"> Manage Permissions </a>
                                
                                    @include('pages.roles.update_permissions')
                                @endif
                            </td>
                        </tr>
                      @endforeach
                      @endif
                  </tbody>
            </table>
        </div>
    </div>
</div>

{!! $roles->render() !!}
</x-base-layout>

<script src="{{asset('/')}}assets/js/bootstrap.min.js"></script>

<script>
    function changeURL(ID){
        const url = new URL(window.location.href);
        url.searchParams.set('Role_Id', ID);  
        window.history.replaceState(null, null, url);

        var baseUrl = (window.location).href; // You can also use document.URL
        var Role_Id = baseUrl.substring(baseUrl.lastIndexOf('=') + 1);
        if(Role_Id != ""){
            var manageOrder = $("#manageOrder_"+Role_Id).val();
            if(manageOrder==1){          
                $("#containerPermissionEdit_"+Role_Id).css("display","block");
            }
        }
    }
</script>

<script type="text/javascript">
   $(document).ready(function(){
        var baseUrl = (window.location).href; // You can also use document.URL
        var Role_Id = baseUrl.substring(baseUrl.lastIndexOf('=') + 1);
        
       if(Role_Id != ""){
            var manageOrder = $("#manageOrder_"+Role_Id).val();
            if(manageOrder==1){          
                $("#containerPermissionEdit_"+Role_Id).css("display","block");
            }
       }
   });

    function getPageValue(value, pagename, roleId){

        if(pagename=='Manage Orders'){ 
            if(value==1){
               $(".OrderStatus").val("1");
               $("#containerPermissionAdd_"+roleId).css("display","block");
            }else{
               $(".OrderStatus").val("0");
               $("#containerPermissionAdd_"+roleId).css("display","none");
            }
        }
    }
</script>


<script type="text/javascript">
    
     function getVisibility(fullId, value, inputID, type){
       
        if(type=='tab'){

            $("#View_"+inputID).prop('checked', false);
            $(".View_"+inputID).prop('checked', false);
            $(".View_"+inputID).val("0");
            $("#View_"+inputID).val("0");

           if(value==1){
                $("#"+fullId).prop('checked', false);
                $("#"+fullId).val("0");

                 $(".Visibility_"+inputID).prop('disabled', true);
                 $(".Visibility_"+inputID).prop('checked', false);
                 $(".Visibility_"+inputID).val("0");

                $(".View_"+inputID).prop('disabled', true);
                $(".View_"+inputID).prop('checked', false);
                $(".View_"+inputID).val("0");

                $(".View_And_Edit_"+inputID).prop('disabled', true);
                $(".View_And_Edit_"+inputID).prop('checked', false);
                $(".View_And_Edit_"+inputID).val("0");
            }

            if(value==0){
                $("#"+fullId).prop('checked', true);
                $("#"+fullId).val("1");

                $(".Visibility_"+inputID).prop('disabled', false);
                $(".Visibility_"+inputID).prop('checked', true);
                $(".Visibility_"+inputID).val("1");

                $(".View_"+inputID).prop('disabled', true);
                $(".View_"+inputID).prop('checked', false);
                $(".View_"+inputID).val("0");

                $(".View_And_Edit_"+inputID).prop('disabled', true);
                $(".View_And_Edit_"+inputID).prop('checked', false);
                $(".View_And_Edit_"+inputID).val("0");
            }    
        }
    }

    function getTabView(fullId, value, inputID, type){
        if(type=='tab'){
            
            $("#Visibility_"+inputID).prop('checked', false);
            $(".Visibility_"+inputID).prop('checked', false);
            $(".Visibility_"+inputID).val("0");
            $("#Visibility_"+inputID).val("0");

            if(value==1){ 
                $("#"+fullId).prop('checked', false);
                $("#"+fullId).val("0");

                $(".Visibility_"+inputID).prop('disabled', true);
                $(".Visibility_"+inputID).prop('checked', false);
                $(".Visibility_"+inputID).val("0");

                $(".View_"+inputID).prop('disabled', true);
                $(".View_"+inputID).prop('checked', false);
                $(".View_"+inputID).val("0");

                $(".View_And_Edit_"+inputID).prop('disabled', true);
                $(".View_And_Edit_"+inputID).prop('checked', false);
                $(".View_And_Edit_"+inputID).val("0");
            }

            if(value==0){
                $("#"+fullId).prop('checked', true);
                $("#"+fullId).val("1");

                $(".Visibility_"+inputID).prop('disabled', false);
                //$(".Visibility_"+inputID).prop('checked', true);
                //$(".Visibility_"+inputID).val("1");

                $(".View_"+inputID).prop('disabled', false);
                $(".View_"+inputID).prop('checked', true);
                $(".View_"+inputID).val("1");

                $(".View_And_Edit_"+inputID).prop('disabled', false);
            }
        }
    }

    function getVisible(inputID, permissionID, opType){
        var radioButtonVal = $("#"+inputID).val();
        if(radioButtonVal==1){ 
            $("#"+inputID).prop('checked', false);
            $("#"+inputID).val("0");
        }else{
            $("#"+inputID).prop('checked', true);
            $("#"+inputID).val("1");

            $("#View_"+permissionID).prop('checked', false);
            $("#View_"+permissionID).val("0");

            $("#View_And_Edit_"+permissionID).prop('checked', false);
            $("#View_And_Edit_"+permissionID).val("0");
        } 
    }

     function getView(inputID, permissionID, opType){
        var radioButtonVal = $("#"+inputID).val();

        if(radioButtonVal==1){            
            $("#"+inputID).prop('checked', false);
            $("#"+inputID).val("0");
        }else{
            $("#"+inputID).prop('checked', true);
            $("#"+inputID).val("1");

            $("#"+opType+"_"+permissionID).prop('checked', false);
            $("#"+opType+"_"+permissionID).val("0");

            $("#Visibility_"+permissionID).prop('checked', false);
            $("#Visibility_"+permissionID).val("0");
        }
    }

     function getEdit(inputID, permissionID, opType){
        var radioButtonVal = $("#"+inputID).val();

        if(radioButtonVal==1){            
            $("#"+inputID).prop('checked', false);
            $("#"+inputID).val("0");
        }else{

            $("#"+inputID).prop('checked', true);
            $("#"+inputID).val("1");

            $("#"+opType+"_"+permissionID).prop('checked', false);
            $("#"+opType+"_"+permissionID).val("0");

            $("#Visibility_"+permissionID).prop('checked', false);
            $("#Visibility_"+permissionID).val("0");
        }
    }

</script>