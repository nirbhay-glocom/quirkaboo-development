<link rel="stylesheet" type="text/css" href="{{asset('/')}}demo2/css/style.bundle.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}demo2/plugins/global/plugins.bundle.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/customization-image.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/lightbox.min.css" />
<link rel="stylesheet" type="text/css" href="{{asset('/')}}assets/css/custom-checkbox.css" />
<script src="{{asset('/')}}assets/js/lightbox-plus-jquery.min.js"></script>

<style>
    .boxShadow{
        box-shadow: 0px 0px 14px grey !important;
    }
    .downstyle2{
      float: right;
      /* margin: -960px 3px 0px 0px; */
      margin: 0px 0px 0px 5px;
      background-color: lightgray !important;
      position: relative;
    }
    .headTitle span{
        color:#000;font-weight: 700;font-size: 18px;
    }

    .headDate{
        padding: 15px 25px 0px 25px;
        text-align:center;
    }

    .divpic{
        margin-bottom: 15px;
    }

</style>
<br/>
    <div class="headTitle">
    <span>Customer Preview</span>           
    </div><br/>

<div class="outer-grid grid-container">
@if($order->Preview_Media != 'null')               
    @php
    $previewArr = json_decode($order->Preview_Media, true); 
    @endphp

    @if($previewArr)
    @foreach($previewArr as $key => $preview)

        @if($preview['Media_Type']=='Image')

        @if($preview['Customer_Preview']=='Yes')

                <div class="inner-grid img-wraps gPreviewBox_{{$order->ID.$key}} boxShadow" id="gPreviewBox_{{$order->ID.$key}}">
                

                <h6 class="headDate">Date: {{ $preview['Date'] }}</h6>
                

                <a class="example-image-link imgg" href="{{$preview['URL']}}" data-lightbox="example-1" style="width: 100% !important;display: block !important;">
                    <div class="divpic">
                        <img class="example-image" src="{{$preview['URL']}}" alt="image-1" />
                    </div>
                </a>
                <a href="{{$preview['URL']}}" download class="btn btn-icon btn-circle  w-25px h-25px bg-body shadow downstyle2">
                    <i class="bi bi-download"></i>
                </a>

                <div class="form-group">
                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label> 

                    <div class="col-sm-12" id="gpdivnote_{{$order->ID.$preview['ID']}}" style="margin-bottom: 10px;margin-top: -7px;">							
                        <span class="form-control form-control-sm readable" id="gpnoteUp_{{$order->ID.$preview['ID']}}" style="width: 122%;">{{$preview['Notes']}}</span>							
                    </div>
                </div><br><br>
            </div>
        @endif
        @endif

        @if($preview['Media_Type']=='Video')
        @if($preview['Customer_Preview']=='Yes')

            <div class="inner-grid img-wraps gPreviewBox_{{$order->ID.$key}} boxShadow" id="gPreviewBox_{{$order->ID.$key}}" >

               

                <h6 class="headDate">Date: {{ $preview['Date'] }}</h6>                           
                
                <div class="divpic" style="padding-bottom: 10px;">
                    <video width="100%" controls class="preview-vid" >
                        <source class="example-image" src="{{$preview['URL']}}" type="video/mp4">
                    </video>
                </div>

                <div class="form-group">
                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Notes</label>

                    <div class="col-sm-12" id="gpdivnote_{{$order->ID.$preview['ID']}}" style="margin-bottom: 10px;margin-top: -7px;">							
                        <span class="form-control form-control-sm readable" id="gpnoteUp_{{$order->ID.$preview['ID']}}" style="width: 122%;">{{$preview['Notes']}}</span>							
                    </div>
                </div><br><br>
                
            </div>
        @endif
        @endif

    @endforeach
    @endif

@else
<h5 style="text-align:center;">Customer Preview Not Found.</h5>
@endif
</div>

