<link rel = "stylesheet" href ="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>
<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
        <div class="card-title m-3">
            <h3 class="fw-bolder m-0">{{ __('Create New User') }}</h3>
        </div>
        <div class="card-title m-10">
            <a class="btn-sm btn-info align-self-center" href="{{ route('users.index') }}"> Back</a>
        </div>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
        {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
        <div class="row">
            <div class="row mb-6">
            <label class="col-lg-4 col-form-label required fw-bold fs-6">First Name:</label>
            <div class="col-lg-8 fv-row">               
                {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control form-control-lg form-control-solid', 'required')) !!}
                </div>
            </div>

           <div class="row mb-6">
               <label class="col-lg-4 col-form-label fw-bold fs-6">Last Name:</label>
                <div class="col-lg-8 fv-row">
                   {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control form-control-lg form-control-solid')) !!}
                </div>
            </div>
            <div class="row mb-6">
               <label class="col-lg-4 col-form-label required fw-bold fs-6">Email Address:</label>
                 <div class="col-lg-8 fv-row">
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control form-control-lg form-control-solid', 'required')) !!}
                </div>
            </div>
            <div class="row mb-6">
               <label class="col-lg-4 col-form-label required fw-bold fs-6">Password:</label>
                 <div class="col-lg-8 fv-row">
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control form-control-lg form-control-solid', 'required')) !!}
                </div>
            </div>
            <div class="row mb-6">
               <label class="col-lg-4 col-form-label required fw-bold fs-6">Confirm Password:</label>
                 <div class="col-lg-8 fv-row">
                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control form-control-lg form-control-solid', 'required')) !!}
                </div>
            </div>
            <div class="row mb-6">
               <label class="col-lg-4 col-form-label required fw-bold fs-6">Role:</label>
                 <div class="col-lg-8 fv-row">
                    {!! Form::select('roles[]', $roles,[], array('class' => 'form-control form-control-lg form-control-solid','multiple', 'required')) !!}
                </div>
            </div>

            <div class="row mb-6">
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Vendor:</label>
                <div class="col-lg-8 fv-row"> 
                    <div class="bs-multiselect">
                        <select id = "mltislct" name="Vendors[]" class="form-control" multiple = "multiple">                        
                            <option value = "DINA"> DINA </option>
                            <option value = "SELM"> SELM </option>
                            <option value = "PRST"> PRST </option>
                            <option value="QRKB"> QRKB </option>                        
                        </select>
                    </div>
                </div>
            </div>

            <div class="row mb-6">
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Display Price:</label>
                <div class="col-lg-8 fv-row">
                    <select class="form-control form-control-lg form-control-solid" name="Price">                        
                        <option value = "No"> No </option>
                        <option value = "Yes"> Yes </option>
                                                    
                    </select>
                </div>
            </div>

            <div class="row mb-6">
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Display Sales Channel:</label>
                <div class="col-lg-8 fv-row">
                    <select class="form-control form-control-lg form-control-solid" name="Sales_Channel">                        
                        <option value = "No"> No </option>
                        <option value = "Yes"> Yes </option>
                                                    
                    </select>
                </div>
            </div>
                
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <button type="submit" class="btn-sm btn-info">
                <span class="indicator-label">
                    Save 
                </span>
                </button>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>
</x-base-layout>

<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js">
</script>

<script>
$(document).ready(function() {
    $('#mltislct').multiselect({
        includeSelectAllOption: true,
    });
});
</script>

<style type="text/css">
    .btn-group {
        width: 100% !important;
    }

    .multiselect-container{
        width: 100% !important;
    }

    .bs-multiselect{
        background-color:#eef3f7;
    }
</style>

