<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
        <div class="card-header cursor-pointer">
            <div class="card-title m-0">
                <h3 class="fw-bolder m-0">Show User</h3>
            </div>
            <a class="btn-sm btn-info align-self-center" href="{{ route('users.index') }}"> Back</a>
        </div>
        
        <div class="card-body p-9">
            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">First Name</label>
                <div class="col-lg-8">
                    <span class="fw-bolder fs-6 text-dark"> {{ $user->first_name }}</span>
                </div>
            </div>

            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">Last Name</label>
                <div class="col-lg-8">
                    <span class="fw-bolder fs-6 text-dark"> {{ $user->last_name }}</span>
                </div>
            </div>
        
            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">Email Address</label>
                <div class="col-lg-8 fv-row">
                    <span class="fw-bold fs-6"> {{ $user->email }}</span>
                </div>
            </div>
            
            <div class="row mb-7">
                <label class="col-lg-4 fw-bold text-muted">
                    Role
                </label>
                <div class="col-lg-8 d-flex align-items-center">
                    @if(!empty($user->roles))
                        @foreach($user->roles as $v)
                            <label class="badge badge-success">{{ $v->name }}</label>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</x-base-layout>