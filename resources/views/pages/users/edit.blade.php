<link rel = "stylesheet" href ="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"/>
<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
            <div class="card-title m-3">
                <h3 class="fw-bolder m-0">{{ __('Edit New User') }}</h3>
            </div>
            <div class="card-title m-10">
                <a class="btn-sm btn-info align-self-center" href="{{ route('users.index') }}"> Back</a>
            </div>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
            {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
            <div class="row">
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">First Name:</label>
                    <div class="col-lg-8 fv-row">  
                        {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control form-control-lg form-control-solid', 'required')) !!}
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">Last Name:</label>
                    <div class="col-lg-8 fv-row">
                        {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control form-control-lg form-control-solid')) !!}
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Email Address:</label>
                    <div class="col-lg-8 fv-row">
                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control form-control-lg form-control-solid', 'required')) !!}
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Password:</label>
                    <div class="col-lg-8 fv-row">
                        {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control form-control-lg form-control-solid')) !!}
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Confirm Password:</label>
                    <div class="col-lg-8 fv-row">
                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control form-control-lg form-control-solid')) !!}
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Role:</label>
                    <div class="col-lg-8 fv-row">
                        {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control form-control-lg form-control-solid','multiple', 'required')) !!}
                    </div>
                </div>

                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Vendor:</label>
                    <div class="col-lg-8 fv-row"> 
                        <div class="bs-multiselect">
                            @php $vendorArr = explode(",",$user->Vendors); @endphp 
                            <select id = "mltislct" name="Vendors[]" class="custom-select form-control" multiple = "multiple">                        
                                <option <?php if(in_array("DINA", $vendorArr)){ echo "selected"; } ?> value = "DINA"> DINA </option>
                                <option <?php if(in_array("SELM", $vendorArr)){ echo "selected"; } ?> value = "SELM"> SELM </option>
                                <option <?php if(in_array("PRST", $vendorArr)){ echo "selected"; } ?> value = "PRST"> PRST </option>                                
                                <option <?php if(in_array("QRKB", $vendorArr)){ echo "selected"; } ?> value = "QRKB"> QRKB </option>   
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Display Price:</label>
                    <div class="col-lg-8 fv-row">
                        <select class="form-control form-control-lg form-control-solid" name="Price">                        
                            <option @if($user->Price=='No') {{"selected"}} @endif value = "No"> No </option>  
                            <option @if($user->Price=='Yes') {{"selected"}} @endif value = "Yes"> Yes </option>                                                      
                        </select>
                    </div>
                </div>

                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">Display Sales Channel:</label>
                    <div class="col-lg-8 fv-row">
                        <select class="form-control form-control-lg form-control-solid" name="Sales_Channel">                        
                            <option @if($user->Sales_Channel=='No') {{"selected"}} @endif value = "No"> No </option>
                            <option @if($user->Sales_Channel=='Yes') {{"selected"}} @endif value = "Yes"> Yes </option>
                                                        
                        </select>
                    </div>
                </div>
                
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn-sm btn-info">
                        <span class="indicator-label">
                            Update 
                        </span>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</x-base-layout>

<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js">
</script>

<script>
$(document).ready(function() {
    $('#mltislct').multiselect({
        includeSelectAllOption: true,
    });
});
</script>

<style type="text/css">
    .btn-group {
        width: 100% !important;
    }

    .multiselect-container{
        width: 100% !important;
    }

    .bs-multiselect{
        background-color:#eef3f7;
    }
</style>