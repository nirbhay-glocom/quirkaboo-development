@php
    $breadcrumb = bootstrap()->getBreadcrumb();
@endphp
<x-base-layout>
<div class="card mb-5 mb-xl-10">
    <div class="card-header cursor-pointer">
        <div class="card-title m-5">
            <h3 class="fw-bolder m-0">{{ __('Users Listing') }}</h3>
        </div>
        <div class="card-title m-5">
                @if ( theme()->getOption('layout', 'page-title/breadcrumb') === true && !empty($breadcrumb))
                    @if (theme()->getOption('layout', 'page-title/direction') === 'row')
                        <!--begin::Separator-->
                        <span class="h-20px border-black opacity-75 border-start mx-4"></span>
                        <!--end::Separator-->
                    @endif

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                        @foreach ($breadcrumb as $item)
                            <!--begin::Item-->
                            @if ( $item['active'] === true )
                                <li class="breadcrumb-item text-black opacity-75">
                                    {{ $item['title'] }}
                                </li>
                            @else
                                <li class="breadcrumb-item text-black opacity-75">
                                    @if ( ! empty($item['path']) )
                                        <a href="{{ theme()->getPageUrl($item['path']) }}" class="text-black text-hover-primary">
                                            {{ $item['title'] }}
                                        </a>
                                    @else
                                        {{ $item['title'] }}
                                    @endif
                                </li>
                            @endif
                            <!--end::Item-->
                            @if (next($breadcrumb))
                                <!--begin::Item-->
                                <li class="breadcrumb-item">
                                    <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                                </li>
                                <!--end::Item-->
                            @endif
                        @endforeach
                    </ul>
                    <!--end::Breadcrumb-->
                @endif
        </div>
        
    </div>

@if ($message = Session::get('success'))
<div class="alert alert-success" style="margin-left: 40px !important;margin-right: 40px!important;">
  <p>{{ $message }}</p>
</div>
@endif

<div id="kt_account_profile_details" class="collapse show">
        <div class="card-body border-top p-12">
      
        <a class="btn-sm btn-info" href="{{ route('users.create') }}" style="float:right;margin:-38px 0px"> Create New User</a>
        <table id="kt_datatable_example_1" class="table table-striped table-row-bordered gy-5 gs-7">
                  <thead style="background: rgb(75, 69, 69);">
                      <tr class="fw-bold fs-6 text-muted">
                          <th class="txt">No</th>
                          <th class="txt">Name</th>
                          <th class="txt">Email</th>
                          <th class="txt">Roles</th>
                          <th class="text-center">Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                      @if($data)
                      @foreach ($data as $key => $user)
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>
                          <td>
                            @if(!empty($user->roles))
                              @foreach($user->roles as $v)
                                <label class="badge badge-success">{{ $v->name }}</label>
                              @endforeach
                            @endif
                          </td>
                          <td>
                            <a class="badge badge-info" href="{{ route('users.show',$user->id) }}">Show</a>
                            <a class="badge badge-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                              
                            {{-- {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                  {!! Form::submit('Delete', ['class' => 'delete-button']) !!}
                              {!! Form::close() !!} --}}
                          </td>
                        </tr>
                        @endforeach
                      @endif
                  </tbody>
            </table>
        </div>
    </div>
</div>

{!! $data->appends(Request::all())->links() !!}

</x-base-layout>